# JModelica.org 2.1 

This is a fork of JModelica.org 2.1 open-source distribution.

JModelica.org is an extensible Modelica-based open source platform for optimization, simulation and analysis of complex dynamic systems. The main objective of the project is to create an industrially viable open source platform for simulation and optimization of Modelica models, while offering a flexible platform serving as a virtual lab for algorithm development and research. As such, JModelica.org provides a platform for technology transfer where industrially relevant problems can inspire new research and where state of the art algorithms can be propagated from academia into industrial use.

## Features

This fork provides an implementation of the following FMI 2.0 ME functionalities useful to save and restore internal FMU state on demand:

* fmi2GetFMUstate()
* fmi2SetFMUstate()
* fmi2FreeFMUstate()
* fmi2SerializeFMUstate()
* fmi2DeserializeFMUstate()
* fmi2SerializedFMUstateSize()

Resulting FMUs are fully compliant with FMI 2.0 standard.

## Build


```
git submodule update --init --recursive
mkdir build && cd build
../configure --prefix=<install prefix> --without-ipopt && make install
```

### Dependencies

JModelica.org requires the following dependencies:

* make
* g++
* gfortran
* python 
* swig
* ant
* default-jre-headless
* python-dev
* python-numpy
* python-scipy
* python-lxml
* python-nose
* python-jpype
* zlib1g-dev
* libboost-dev
* cython
* jcc
* pkg-config
* liblapack-dev
* libblas-dev
* libgsl-dev

## Docker

This fork provides a Dockerfile in order to readily use our extended version of JModelica.org 2.1.

```
docker build -t mclab/jmodelica .
docker run --rm -it mclab/jmodelica
```

## License

JModelica.org is distributed under the GPL v.3 license approved by the Open Source Initiative.

## Contributors

JModelica.org is a result of research at the Department of Automatic Control, Lund University, and is now maintained and developed by Modelon AB in collaboration with academia. 

This fork is maintained and developed by:

* [Stefano Sinisi](linkedin.com/in/stefanosinisi)
* [Vadim Alimguzhin](linkedin.com/in/vadim-alimguzhin/)
