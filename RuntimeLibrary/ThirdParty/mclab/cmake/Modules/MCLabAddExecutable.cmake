#    This file is part of MCLabCMakeModules
#    Copyright (C) 2020  Vadim Alimguzhin (MCLab, http://mclab.di.uniroma1.it)
#
#    MCLabCMakeModules is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation
#
#    MCLabCMakeModules is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabCMakeModules.
#    If not, see <https://www.gnu.org/licenses/>.

macro(mclab_add_executable exec_name )

add_executable(${exec_name} ${ARGN})

mclab_cflags(${exec_name})

target_include_directories(${exec_name} PUBLIC ${PROJECT_BINARY_DIR})
target_include_directories(${exec_name} PUBLIC ${PROJECT_SOURCE_DIR}/include)
target_include_directories(${exec_name} PUBLIC ${PROJECT_SOURCE_DIR}/src)

endmacro(mclab_add_executable)
