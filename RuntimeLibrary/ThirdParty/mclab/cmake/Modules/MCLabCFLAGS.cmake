#    This file is part of MCLabCMakeModules
#    Copyright (C) 2020  Vadim Alimguzhin (MCLab, http://mclab.di.uniroma1.it)
#
#    MCLabCMakeModules is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation
#
#    MCLabCMakeModules is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabCMakeModules.
#    If not, see <https://www.gnu.org/licenses/>.

# Setup CFLAGS to reasonable defaults
macro(mclab_cflags target)
  target_compile_options(${target} PUBLIC $<$<COMPILE_LANGUAGE:C>:--std=c99>)
  target_compile_definitions(${target} PUBLIC "_DEFAULT_SOURCE")

  if(NOT DEFINED MCLAB_WARNINGS)
    set(MCLAB_WARNINGS ON CACHE BOOL "Enable compiler warnings")
  endif(NOT DEFINED MCLAB_WARNINGS)
  set(MCLAB_CFLAGS_WARNINGS -fno-builtin --pedantic --pedantic-errors -Wall -Wextra -Wfloat-equal -Wconversion -Wno-unused)
  target_compile_options(${target} PUBLIC "$<$<BOOL:${MCLAB_WARNINGS}>:${MCLAB_CFLAGS_WARNINGS}>")

  set(MCLAB_SANITIZE OFF CACHE BOOL "Compiler sanitize")
  set(MCLAB_CFLAGS_SANITIZE "-fsanitize=address")
  target_compile_options(${target} PUBLIC "$<$<BOOL:${MCLAB_SANITIZE}>:${MCLAB_CFLAGS_SANITIZE}>")
  target_link_libraries(${target} "$<$<BOOL:${MCLAB_SANITIZE}>:${MCLAB_CFLAGS_SANITIZE}>")

  set(MCLAB_CFLAGS_DEBUG "-O0")
  target_compile_options(${target} PUBLIC "$<$<CONFIG:DEBUG>:${MCLAB_CFLAGS_DEBUG}>")
endmacro()
