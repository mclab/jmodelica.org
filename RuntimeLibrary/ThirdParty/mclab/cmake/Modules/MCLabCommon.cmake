#    This file is part of MCLabCMakeModules
#    Copyright (C) 2020  Vadim Alimguzhin (MCLab, http://mclab.di.uniroma1.it)
#
#    MCLabCMakeModules is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation
#
#    MCLabCMakeModules is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabCMakeModules.
#    If not, see <https://www.gnu.org/licenses/>.

include(MCLabBuildType)
include(MCLabCXXStandard)
include(MCLabCFLAGS)
include(MCLabDoxygen)
include(MCLabAddExecutable)
include(MCLabLinkLibrary)
include(MCLabFindAndLinkLibrary)
include(MCLabGenerateConfigHeader)
include(MCLabCheckDeps)
include(MCLabOpenMP)
include(MCLabMPI)
include(MCLabPthreads)
