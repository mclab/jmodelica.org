#    This file is part of MCLabCMakeModules
#    Copyright (C) 2020  Vadim Alimguzhin (MCLab, http://mclab.di.uniroma1.it)
#
#    MCLabCMakeModules is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation
#
#    MCLabCMakeModules is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabCMakeModules.
#    If not, see <https://www.gnu.org/licenses/>.

macro(mclab_tool tool_name tool_version)

set(CMAKE_MACOSX_RPATH 1)

project(${tool_name} VERSION ${tool_version})

include(MCLabCommon)

file(GLOB libdirs "lib/*")

foreach(libdir ${libdirs})
  if(IS_DIRECTORY ${libdir})
    get_filename_component(library ${libdir} NAME)
    list(APPEND libraries ${library})
  endif(IS_DIRECTORY ${libdir})
endforeach(libdir ${libdirs})

set(MCLAB_LIB_BUILD_TYPE "Release" CACHE STRING "Build type for libraries")
set(CMAKE_BUILD_TYPE_SAVE "${CMAKE_BUILD_TYPE}")
set(CMAKE_BUILD_TYPE "${MCLAB_LIB_BUILD_TYPE}")
set(MCLAB_LIB_WARNINGS ON CACHE STRING "Enable compiler warnings for lbiraries")
set(MCLAB_WARNINGS_SAVE "${MCLAB_WARNINGS}")
set(MCLAB_WARNINGS ${MCLAB_LIB_WARNINGS})
foreach(library ${libraries})
  add_subdirectory("lib/${library}" EXCLUDE_FROM_ALL)
endforeach(library ${libraries})
set(CMAKE_BUILD_TYPE "${CMAKE_BUILD_TYPE_SAVE}")
set(MCLAB_WARNINGS "${MCLAB_WARNINGS_SAVE}")

endmacro()
