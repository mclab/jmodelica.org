#    This file is part of MCLabCMakeModules
#    Copyright (C) 2020  Vadim Alimguzhin (MCLab, http://mclab.di.uniroma1.it)
#
#    MCLabCMakeModules is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation
#
#    MCLabCMakeModules is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabCMakeModules.
#    If not, see <https://www.gnu.org/licenses/>.

# Try to find the PPL librairies
# PPL_FOUND - system has PPL lib
# PPL_INCLUDE_DIR - the PPL include directory
# PPL_LIBRARIES - Libraries needed to use PPL

include(FindPackageHandleStandardArgs)

if (PPL_INCLUDE_DIR AND PPL_LIBRARIES)
  # Already in cache, be silent
  set(PPL_FIND_QUIETLY TRUE)
endif (PPL_INCLUDE_DIR AND PPL_LIBRARIES)

find_path(PPL_INCLUDE_DIR NAMES ppl_c.h)
find_library(PPL_LIBRARIES NAMES ppl_c libppl_c)
find_library(PPLXX_LIBRARIES NAMES ppl libppl)
MESSAGE(STATUS "PPL libs: " ${PPL_LIBRARIES} " " ${PPLXX_LIBRARIES} )

list(APPEND PPL_LIBRARIES ${PPLXX_LIBRARIES})
if(NOT GMP_FOUND)
  if(PPL_FIND_QUIETLY OR NOT PPL_FIND_REQUIRED)
    find_package(GMP)
  else()
    find_package(GMP REQUIRED)
  endif()
  list(APPEND PPL_LIBRARIES ${GMP_LIBRARIES})
  list(APPEND PPL_INCLUDE_DIR ${GMP_INCLUDE_DIR})
endif()

FIND_PACKAGE_HANDLE_STANDARD_ARGS(PPL DEFAULT_MSG PPL_INCLUDE_DIR PPL_LIBRARIES)

mark_as_advanced(PPL_INCLUDE_DIR PPL_LIBRARIES)
