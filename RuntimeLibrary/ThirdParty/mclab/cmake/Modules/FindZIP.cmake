#    This file is part of MCLabCMakeModules
#    Copyright (C) 2020  Vadim Alimguzhin (MCLab, http://mclab.di.uniroma1.it)
#
#    MCLabCMakeModules is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation
#
#    MCLabCMakeModules is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabCMakeModules.
#    If not, see <https://www.gnu.org/licenses/>.

# Searches for an installation of the zip library. On success, it sets the following variables:
#
#   ZIP_FOUND              Set to true to indicate the zip library was found
#   ZIP_INCLUDE_DIRS       The directory containing the header file zip/zip.h
#   ZIP_LIBRARIES          The libraries needed to use the zip library
#
# To specify an additional directory to search, set ZIP_ROOT.
#
# Author: Siddhartha Chaudhuri, 2009 ~ Vadim Alimguzhin, 2017
#

# Look for the header, first in the user-specified location and then in the system locations
SET(ZIP_INCLUDE_DOC "The directory containing the header file zip/zip.h")
FIND_PATH(ZIP_INCLUDE_DIRS NAMES zip/zip.h PATHS ${ZIP_ROOT} ${ZIP_ROOT}/include DOC ${ZIP_INCLUDE_DOC} NO_DEFAULT_PATH)
IF(NOT ZIP_INCLUDE_DIRS)  # now look in system locations
  FIND_PATH(ZIP_INCLUDE_DIRS NAMES zip.h DOC ${ZIP_INCLUDE_DOC})
ENDIF(NOT ZIP_INCLUDE_DIRS)

SET(ZIP_FOUND FALSE)

IF(ZIP_INCLUDE_DIRS)
  SET(ZIP_LIBRARY_DIRS ${ZIP_INCLUDE_DIRS})

  IF("${ZIP_LIBRARY_DIRS}" MATCHES "/include$")
    # Strip off the trailing "/include" in the path.
    GET_FILENAME_COMPONENT(ZIP_LIBRARY_DIRS ${ZIP_LIBRARY_DIRS} PATH)
  ENDIF("${ZIP_LIBRARY_DIRS}" MATCHES "/include$")

  IF(EXISTS "${ZIP_LIBRARY_DIRS}/lib")
    SET(ZIP_LIBRARY_DIRS ${ZIP_LIBRARY_DIRS}/lib)
  ENDIF(EXISTS "${ZIP_LIBRARY_DIRS}/lib")

  # Find ZIP libraries
  FIND_LIBRARY(ZIP_DEBUG_LIBRARY NAMES zipd zip_d libzipd libzip_d
               PATH_SUFFIXES Debug ${CMAKE_LIBRARY_ARCHITECTURE} ${CMAKE_LIBRARY_ARCHITECTURE}/Debug
               PATHS ${ZIP_LIBRARY_DIRS} NO_DEFAULT_PATH)
  FIND_LIBRARY(ZIP_RELEASE_LIBRARY NAMES zip libzip
               PATH_SUFFIXES Release ${CMAKE_LIBRARY_ARCHITECTURE} ${CMAKE_LIBRARY_ARCHITECTURE}/Release
               PATHS ${ZIP_LIBRARY_DIRS} NO_DEFAULT_PATH)


  SET(ZIP_LIBRARIES )
  IF(ZIP_DEBUG_LIBRARY AND ZIP_RELEASE_LIBRARY)
    SET(ZIP_LIBRARIES debug ${ZIP_DEBUG_LIBRARY} optimized ${ZIP_RELEASE_LIBRARY})
  ELSEIF(ZIP_DEBUG_LIBRARY)
    SET(ZIP_LIBRARIES ${ZIP_DEBUG_LIBRARY})
  ELSEIF(ZIP_RELEASE_LIBRARY)
    SET(ZIP_LIBRARIES ${ZIP_RELEASE_LIBRARY})
  ENDIF(ZIP_DEBUG_LIBRARY AND ZIP_RELEASE_LIBRARY)

  IF(ZIP_LIBRARIES)
    SET(ZIP_FOUND TRUE)
  ENDIF(ZIP_LIBRARIES)
ENDIF(ZIP_INCLUDE_DIRS)

IF(ZIP_FOUND)
  IF(APPLE)
    FIND_PATH(ZIP_CONFIG_INCLUDE_DIR NAMES zipconf.h PATHS /usr/local/lib/libzip/include)
    IF(ZIP_CONFIG_INCLUDE_DIR)
      LIST(APPEND ZIP_INCLUDE_DIRS ${ZIP_CONFIG_INCLUDE_DIR})
    ELSE(ZIP_CONFIG_INCLUDE_DIR)
      SET(ZIP_FOUND FALSE)
    ENDIF(ZIP_CONFIG_INCLUDE_DIR)
  ENDIF(APPLE)
ENDIF(ZIP_FOUND)

IF(ZIP_FOUND)
  IF(NOT ZIP_FIND_QUIETLY)
    MESSAGE(STATUS "Found ZIP: headers at ${ZIP_INCLUDE_DIRS}, libraries at ${ZIP_LIBRARY_DIRS}")
  ENDIF(NOT ZIP_FIND_QUIETLY)
ELSE(ZIP_FOUND)
  IF(ZIP_FIND_REQUIRED)
    MESSAGE(FATAL_ERROR "ZIP library not found")
  ENDIF(ZIP_FIND_REQUIRED)
ENDIF(ZIP_FOUND)
