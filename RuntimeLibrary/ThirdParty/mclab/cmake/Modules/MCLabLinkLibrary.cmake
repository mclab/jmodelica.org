#    This file is part of MCLabCMakeModules
#    Copyright (C) 2020  Vadim Alimguzhin (MCLab, http://mclab.di.uniroma1.it)
#
#    MCLabCMakeModules is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation
#
#    MCLabCMakeModules is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabCMakeModules.
#    If not, see <https://www.gnu.org/licenses/>.

macro(mclab_link_library target library)
  if(DEFINED "${mclab_deps}")
    list(FIND ${mclab_deps} ${library} ${already})
  else(DEFINED ${mclab_deps})
    set(already -1)
  endif(DEFINED "${mclab_deps}")
  if(${already} EQUAL -1)
    list(APPEND mclab_deps ${library})
    get_directory_property(HAS_PARENT PARENT_DIRECTORY)
    if(HAS_PARENT)
      set(mclab_deps ${mclab_deps} PARENT_SCOPE)
    endif(HAS_PARENT)
  endif(${already} EQUAL -1)
  target_link_libraries(${target} ${library})
endmacro()
