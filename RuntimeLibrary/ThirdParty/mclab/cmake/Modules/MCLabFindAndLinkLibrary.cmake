#    This file is part of MCLabCMakeModules
#    Copyright (C) 2020  Vadim Alimguzhin (MCLab, http://mclab.di.uniroma1.it)
#
#    MCLabCMakeModules is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation
#
#    MCLabCMakeModules is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabCMakeModules.
#    If not, see <https://www.gnu.org/licenses/>.

macro(mclab_find_and_link_library target library)
  find_package(${library} ${ARGN})
  string(TOUPPER ${library} library_upcase)
  if(${${library_upcase}_FOUND})
    if(${library_upcase}_INCLUDE_DIR)
      include_directories(${${library_upcase}_INCLUDE_DIR})
    elseif(${library_upcase}_INCLUDE_DIRS)
      include_directories(${${library_upcase}_INCLUDE_DIRS})
    endif(${library_upcase}_INCLUDE_DIR)
    target_link_libraries(${target} ${${library_upcase}_LIBRARIES})
  endif(${${library_upcase}_FOUND})
endmacro()
