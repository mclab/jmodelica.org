#    This file is part of MCLabCMakeModules
#    Copyright (C) 2020  Vadim Alimguzhin (MCLab, http://mclab.di.uniroma1.it)
#
#    MCLabCMakeModules is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation
#
#    MCLabCMakeModules is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabCMakeModules.
#    If not, see <https://www.gnu.org/licenses/>.

#.rst:
# FindCPLEX
# -------
#

# Finds the CPLEX library
#
# This will define the following variables::
#
#   CPLEX_FOUND    - True if the system has the CPLEX library
#   CPLEX_VERSION  - The version of the CPLEX library which was found
#
# and the following imported targets::
#
#   CPLEX::CPLEX   - The CPLEX library

file(GLOB CPLEX_DEFAULT_BASE /opt/ibm/ILOG/CPLEX_Studio*/cplex)

find_path(CPLEX_INCLUDE_DIR
  NAMES ilcplex/cplex.h
  PATHS "${CPLEX_DEFAULT_BASE}/include"
)

file(GLOB CPLEX_DEFAULT_LIB "${CPLEX_DEFAULT_BASE}/lib/x86-64*/static_pic")
find_library(CPLEX_LIBRARY
  NAMES cplex
  PATHS ${CPLEX_DEFAULT_LIB}
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(CPLEX
  FOUND_VAR CPLEX_FOUND
  REQUIRED_VARS
    CPLEX_LIBRARY
    CPLEX_INCLUDE_DIR
  VERSION_VAR CPLEX_VERSION
)

if(CPLEX_FOUND)
  set(CPLEX_LIBRARIES ${CPLEX_LIBRARY})
  set(CPLEX_INCLUDE_DIRS ${CPLEX_INCLUDE_DIR})
  set(CPLEX_DEFINITIONS ${PC_CPLEX_CFLAGS_OTHER})
endif()

if(CPLEX_FOUND AND NOT TARGET CPLEX::CPLEX)
  add_library(CPLEX::CPLEX UNKNOWN IMPORTED)
  set_target_properties(CPLEX::CPLEX PROPERTIES
    IMPORTED_LOCATION "${CPLEX_LIBRARY}"
    INTERFACE_COMPILE_OPTIONS "${PC_CPLEX_CFLAGS_OTHER}"
    INTERFACE_INCLUDE_DIRECTORIES "${CPLEX_INCLUDE_DIR}"
  )
endif()

mark_as_advanced(
  CPLEX_INCLUDE_DIR
  CPLEX_LIBRARY
)
