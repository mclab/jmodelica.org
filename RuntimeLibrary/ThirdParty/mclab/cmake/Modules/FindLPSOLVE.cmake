#    This file is part of MCLabCMakeModules
#    Copyright (C) 2020  Vadim Alimguzhin (MCLab, http://mclab.di.uniroma1.it)
#
#    MCLabCMakeModules is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation
#
#    MCLabCMakeModules is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabCMakeModules.
#    If not, see <https://www.gnu.org/licenses/>.

# - Try to find lp_solve 5.5
# Once done this will define
#  LPSOLVE_FOUND - System has lp_solve
#  LPSOLVE_INCLUDE_DIRS - The lp_solve include directories
#  LPSOLVE_LIBRARIES - The libraries needed to use lp_solve
#  LPSOLVE_DEFINITIONS - Compiler switches required for using lp_solve

find_path(LPSOLVE_INCLUDE_DIR lpsolve.h PATH_SUFFIXES lpsolve)

find_library(LPSOLVE_LIBRARY NAMES lpsolve55)

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set LIBXML2_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(LPSOLVE DEFAULT_MSG LPSOLVE_LIBRARY LPSOLVE_INCLUDE_DIR)

if (LPSOLVE_FOUND)
  set(LPSOLVE_INCLUDE_DIRS ${LPSOLVE_INCLUDE_DIR})
  set(LPSOLVE_LIBRARIES ${LPSOLVE_LIBRARY})
endif(LPSOLVE_FOUND)

mark_as_advanced(LPSOLVE_INCLUDE_DIR LPSOLVE_LIBRARY)
