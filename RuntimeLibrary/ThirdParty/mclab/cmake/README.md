This repo contains our modules for CMake.
They can be splitted into two groups:

1. Modules that enable easy development of libraries and tools integrated into our CMake infrastructure.
The primary modules that are intended to be included in your `CMakeLists.txt` files are:
    * `MCLabLibrary` that defines macro `mclab_library()`
    * `MCLabTool` that defines macro `mclab_tool()`
2. Find modules for libraries that are not supported by CMake distribution.
   List of libraries:
    * [GMP](http://gmplib.org/)
    * [GlPK](https://www.gnu.org/software/glpk/)
    * [lp_solve](http://lpsolve.sourceforge.net/)
    * [CPLEX](https://www-01.ibm.com/software/commerce/optimization/cplex-optimizer/)
    * [PPL](http://bugseng.com/products/ppl/)