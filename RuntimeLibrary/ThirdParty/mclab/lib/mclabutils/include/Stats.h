/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef __STATS_H__
#define __STATS_H__

#include <time.h>

/*! @file
        @brief
                The Stats package allows to compute statistics over large arrays
   of values.

                The functions use iterative algorithms to avoid memory
   explosion.

        @date 2014-11-14
*/

/*!	@brief
                Computes the average of an array of double values
        @param data
                The array of values
        @param n
                The length of 'data'
*/
double Stats_avg_d(const double* const data, unsigned long n);

/*!	@brief
                Computes the standard deviation of an array of double
   values
        @param data
                The array of values
        @param n
                The length of 'data'
*/
double Stats_stddev_d(const double* const data, unsigned long n);

/*!	@brief
                Computes the standard deviation of an array of double
   values, given its average.

        @details
                _this function can be used to compute standard deviation more
   efficiently than with Stats_stddev_ld() , when the average value of 'data'
   has already been computed.
        @param data
                The array of values
        @param n
                The length of 'data'
        @param avg
                The average of array 'data'
*/
double Stats_stddevGivenAvg_d(const double* const data,
                                    unsigned long n, double avg);


/*!	@brief
                Computes the average of an array of long double values
        @param data
                The array of values
        @param n
                The length of 'data'
*/
long double Stats_avg_ld(const long double* const data, unsigned long n);

/*!	@brief
                Computes the standard deviation of an array of long double
   values
        @param data
                The array of values
        @param n
                The length of 'data'
*/
long double Stats_stddev_ld(const long double* const data, unsigned long n);

/*!	@brief
                Computes the standard deviation of an array of long double
   values, given its average.

        @details
                _this function can be used to compute standard deviation more
   efficiently than with Stats_stddev_ld() , when the average value of 'data'
   has already been computed.
        @param data
                The array of values
        @param n
                The length of 'data'
        @param avg
                The average of array 'data'
*/
long double Stats_stddevGivenAvg_ld(const long double* const data,
                                    unsigned long n, long double avg);

/*!	@brief
                Computes the average of an array of long values
        @param data
                The array of values
        @param n
                The length of 'data'
*/
long double Stats_avg_l(const long* const data, unsigned long n);

/*!	@brief
                Computes the standard deviation of an array of long values
        @param data
                The array of values
        @param n
                The length of 'data'
*/
long double Stats_stddev_l(const long* const data, unsigned long n);

/*!	@brief
                Computes the standard deviation of an array of long values,
   given its average.

        @details
                _this function can be used to compute standard deviation more
   efficiently than with Stats_stddev_l() , when the average value of 'data' has
   already been computed.
        @param data
                The array of values
        @param n
                The length of 'data'
        @param avg
                The average of array 'data'
*/
long double Stats_stddevGivenAvg_l(const long* const data, unsigned long n,
                                   long double avg);

/*!	@brief
                Computes the average of an array of unsigned long values
        @param data
                The array of values
        @param n
                The length of 'data'
*/
long double Stats_avg_ul(const unsigned long* const data, unsigned long n);
/*!	@brief
                Computes the standard deviation of an array of unsigned long
   values
        @param data
                The array of values
        @param n
                The length of 'data'
*/
long double Stats_stddev_ul(const unsigned long* const data, unsigned long n);
/*!	@brief
                Computes the standard deviation of an array of unsigned long
   values, given its average.

        @details
                _this function can be used to compute standard deviation more
   efficiently than with Stats_stddev_ul() , when the average value of 'data'
   has already been computed.
        @param data
                The array of values
        @param n
                The length of 'data'
        @param avg
                The average of array 'data'
*/
long double Stats_stddevGivenAvg_ul(const unsigned long* const data,
                                    unsigned long n, long double avg);

/*!	@brief
                Computes the average of an array of time_t values
        @param data
                The array of values
        @param n
                The length of 'data'
*/
long double Stats_avg_time_t(const time_t* const data, unsigned long n);

/*!	@brief
                Computes the standard deviation of an array of time_t values
        @param data
                The array of values
        @param n
                The length of 'data'
*/
long double Stats_stddev_time_t(const time_t* const data, unsigned long n);
/*!	@brief
                Computes the standard deviation of an array of time_t values,
   given its average.

        @details
                _this function can be used to compute standard deviation more
   efficiently than with Stats_stddev_time_t() , when the average value of
   'data' has already been computed.
        @param data
                The array of values
        @param n
                The length of 'data'
        @param avg
                The average of array 'data'
*/
long double Stats_stddevGivenAvg_time_t(const time_t* const data,
                                        unsigned long n, long double avg);

#endif
