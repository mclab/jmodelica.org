/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef __ARRAY__
#define __ARRAY__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <Debug.h>

/*! @file
                @brief
                        An instance of the Array data structure defines a
   generic array.
*/

/*! @brief
                        An instance of the Array data structure defines a
   generic array.
                @details
                        Arrays automatically grow in size when needed.
                        Also, attempts to access an non-existing element result
   into assertion violations, thus easying debug.
*/
typedef struct Array Array;

/*!	@brief
                Creates a new Array

        @param capacity
                The array initial capacity.
        @param elemSize
                The size in bytes of the elements that can be stored in the
   array (typically, _this value is obtained using sizeof(elem type))

        @param capIncrement
                The number of additional elements that will be allocated before
   adding a new element in a full array

        @param fprintElem
                A call-back function which prints to file stream *f a textual
   representation of an element of the array. It receives as input a _pointer_
   to an element of the array.

        @param fEqualsElem
                A call-back function which checks whether two elements are
   equal. It receives as input _pointers_ to two elements of the array.
*/
Array* Array_new(unsigned int capacity, size_t elemSize,
                 unsigned int capIncrement,
                 void (*fprintElem)(const void* const elem_p, FILE*),
                 int (*fEqualsElem)(const void* const e1_p,
                                    const void* const e2_p));

/*!	@brief
                Simple constructor to create an array whose elements are int.
                fPrintElem and fEqualsElem function pointers are automatically
   set
*/
Array* Array_newInt(unsigned int capacity, unsigned int capIncrement);

/*!	@brief
                Simple constructor to create an array whose elements are float.
                fPrintElem and fEqualsElem function pointers are automatically
   set
*/
Array* Array_newFloat(unsigned int capacity, unsigned int capIncrement);

/*!	@brief
                Simple constructor to create an array whose elements are double.
                fPrintElem and fEqualsElem function pointers are automatically
   set
*/
Array* Array_newDouble(unsigned int capacity, unsigned int capIncrement);

/*!	@brief
                Simple constructor to create an array whose elements are char*
   (i.e., strings). fPrintElem and fEqualsElem function pointers are
   automatically set
*/
Array* Array_newString(unsigned int capacity, unsigned int capIncrement);

/*!	@brief
                Simple constructor to create an array whose elements are char.
                fPrintElem and fEqualsElem function pointers are automatically
   set
*/
Array* Array_newChar(unsigned int capacity, unsigned int capIncrement);

/*!	@brief
                Simple constructor to create an array whose elements are long.
                fPrintElem and fEqualsElem function pointers are automatically
   set
*/
Array* Array_newLong(unsigned int capacity, unsigned int capIncrement);

/*!	@brief
                Simple constructor to create an array whose elements are
   unsigned long. fPrintElem and fEqualsElem function pointers are automatically
   set
*/
Array* Array_newULong(unsigned int capacity, unsigned int capIncrement);

/*! @brief
                Creates an Array instance by wrapping an already existing
   (malloc-/calloc-ed) C array 'buffer', whose 'length' elements are assumed to
   have size 'elemSize'.

                Upon  Array_free() , also 'buffer' will be freed.

        @param length
                The length of buffer
        @param elemSize
                The size (in bytes) of each element in buffer
        @param buffer
                The already allocated C array
        @param capIncrement
                The capacity increment, should an element be added to _this
   Array
        @param fprintElem
                A call-back function which prints to file stream *f a textual
   representation of an element of the array. It receives as input a _pointer_
   to an element of the array.

        @param fEqualsElem
                A call-back function which checks whether two elements are
   equal. It receives as input _pointers_ to two elements of the array.
*/
Array* Array_new_wrap(unsigned int length, size_t elemSize, void* buffer,
                      unsigned int capIncrement,
                      void (*fprintElem)(const void* const elem_p, FILE*),
                      int (*fEqualsElem)(const void* const e1_p,
                                         const void* const e2_p));

/*!	@brief
                Clones _this Array
*/
Array* Array_clone(const Array* const _this);

/*! @brief
                Returns the size in bytes of each element in _this Array
*/
size_t Array_elemSize(const Array* const _this);

/*!	@brief
                Retrieves an element from _this array
        @param i
                The index of the element to be retrieved.
                If i < 0 or i >= Array_length(_this), an assertion failure is
   triggered.
        @param value
                *value is filled with the value of the i-th element of _this
   Array
        @returns
                'value'
*/
void* Array_get(const Array* const _this, unsigned int i, void* const value);

/*!	@brief
                Replaces the i-th element of _this Array with value '*value'.
        @param i
                The index of the element to be replaced.
                If i < 0 or i >= Array_length(_this), an assertion failure is
   triggered.
        @param value
                A pointer to the value that will be copied into the i-th slot of
   _this Array
*/
void Array_set(Array* const _this, unsigned int i, const void* const value);

/*!	@brief
                Equivalent to Array_get(_this, 0, value)
*/
void* Array_getFirst(const Array* const _this, void* const value);

/*!	@brief
                Equivalent to Array_get(_this, Array_length(_this)-1, value)
*/
void* Array_getLast(const Array* const _this, void* const value);

/*!	@brief
                Returns a read-only pointer to the i-th element of _this array.
                _this pointer is guaranteed to be valid only until the next
   invocation of Array_add(), Array_push(), or any method which writes into
   _this Array (as they might trigger the capacity adjustment algorithm which
   could in turn trigger a realloc()).

                Contract: Array_elemPtr(array, i) == &
   (Array_as_array(array)[i])

                Use _this method only when _stricly_ needed!
*/
const void* Array_elemPtr(const Array* const _this, unsigned int i);

/*!	@brief
                Returns a read-only view of _this Array as a standard C array
*/
const void* Array_as_C_array(const Array* const _this);

// Methods to handle _this array as a stack
/*!	@brief
                Removes the top element of _this Array (viewed as a stack) and
   copies it into *value
        @param value
                A pointer to a location where the top element of _this stack
   must be copied
        @return
                value
*/
void* Array_pop(Array* const _this, void* const value);

/*!	@brief
                Adds a new element to the top _this Array (viewed as a stack)
        @param valueP
                A pointer to a location where the element to be pushed is
   stored. The Array will contain a copy of *valueP
*/
void Array_push(Array* const _this, const void* const valueP);

/*!	@brief
                Gets (but does not remove) the top element of _this Array
   (viewed as a stack) and copies it into *value
        @param value
                A pointer to a location where the top element of _this stack
   must be copied
        @return
                value
*/
void* Array_top(Array* const _this, void* const value);

// Methods to handle _this array as a queue

/*!	@brief
                Enqueues a new element of _this Array (viewed as a queue)
        @param valueP
                A pointer to a location where the element to be enqueued is
   stored. The Array will contain a copy of *valueP
*/
void Array_enqueue(Array* const _this, const void* const valueP);

/*!	@brief
                Dequeues the first element of _this Array (viewed as a queue)
   and copies it into *value
        @param value
                A pointer to a location where the first element of _this queue
   must be copied
        @return
                value
*/
void* Array_dequeue(Array* const _this, void* const value);

/*!	@brief
                Adds a new element at the end of _this Array
        @param valueP
                A pointer to a location where the element to be added is stored.
                The Array will contain a copy of *valueP
*/
void Array_add(Array* const _this, const void* const valueP);

/*!	@brief
                Adds the given element n times at the end of _this Array
        @param valueP
                A pointer to a location where the element to be added is stored.
                The Array will contain n copies of *valueP
*/
void Array_add_n_times(Array* _this, const void* const valueP, int n);

/*!	@brief
                Swaps the i-th and the j-th elements of _this Array
        @details
                In case i or j is not a valid index, an assertion failure is
   raised
*/
void Array_swap(Array* _this, unsigned int i, unsigned int j);

/*!	@brief
                Truncates _this Array to length len, by removing all elements
   having index >= len
*/
void Array_trunc(Array* const _this, unsigned int len);

/*! @brief
                Wipes out _this Array
*/
void Array_clear(Array* const _this);

/*!	@brief
                Returns the number of elements in _this Array
*/
unsigned int Array_length(const Array* const _this);

/*!	@brief
                Copies _this Array into existing array dest.
        @details
                _this method mimics  Array_clone() , with the difference that
                Array dest already exists.
                After invocation of _this function, dest will be equal to _this
*/
void Array_copy(const Array* const _this, Array* dest);

/*! @brief
                Returns a pointer to the callback function which prints a single
   element of _this Array.
        @details
                The returned function takes a pointer to the Array element and a
   file handler as arguments.
*/
void (*Array_fprintElem(const Array* const _this))(const void* const, FILE*);

/*!	@brief
                Checks whether _this Array is equal to Array 'other'
        @return
                1 if _this is equal to other, 0 otherwise
*/
int Array_equals(const Array* const _this, const Array* const other);

/*!	@brief
                Frees memory allocated to *thisP Array and sets *thisP to NULL
*/
void Array_free(Array** thisP);

/*!	@brief
                Prints _this Array to file stream *f
*/
void Array_fprint(const void* const _this, FILE* f);

/*!	@brief
                Sorts array _this according to the given comparator.
                Function comparator(ptr_a,ptr_b) defines how possible array
   values should appear in the ordering. It must take as input two pointers to
   two array elements and must return: <ul> <li>a negative int iff *ptr_a must
   come before *ptr_b in the ordering</li> <li>int zero iff *ptr_a and *ptr_b
   are equivalent wrt. the ordering</li> <li>a positive int iff *ptr_a must come
   after *ptr_b in the ordering.</li>
*/
void Array_sort(Array* _this, int (*comparator)(const void*, const void*));

int Array_double_comparator_asc(const void* a, const void* b);
int Array_double_comparator_desc(const void* a, const void* b);

#endif
