/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef __RND_GEN_DISTRIBUTION__
#define __RND_GEN_DISTRIBUTION__

#include <RndGen.h>

/*! @file
                @brief
                        An instance of the RndGenDistribution data structure
   defines a random discrete distribution.
                @date 2014-11-14
*/

/*! @brief
                An instance of the RndGenDistribution data structure defines a
   random discrete distribution.
*/
typedef struct RndGenDistribution RndGenDistribution;

/*! @brief
    Creates a new RndGenDistribution instance, representing a random discrete
  distribution function P() defined over the discrete domain
    @verbatim X = { x | x is an integer & x_min <= x <= x_max } @endverbatim

  @details
    The values of P(x) are set to 0 for all x in X.
    Probability values can be set by using function  RndGenDistribution_set()
  and passed values are truncated at 'decimal_places' decimal digits.

    Hence, the higher value for 'decimal_places', the higher precision.
*/
RndGenDistribution* RndGenDistribution_new(RndGen* rnd, unsigned long x_min,
                                           unsigned long x_max,
                                           int decimal_places);

/*! @brief
    Creates a new RndGenDistribution instance, representing a random discrete
  distribution function P() defined over the discrete domain:
    @verbatim X = { x | x is an integer & x_min <= x <= x_max } @endverbatim

  @details
    For each x in X, the value of P(x) is set to probability(x).
    Each value of probability() is approximated as a multiple of
  1/(10^decimal_places).

    Example: assume that x_min = 0, x_max = 3 and probability() = {1-->0.1,
  2-->0.73, 3-->0.26} If decimal_places = 1, we are scaling the probability
  distribution as follows:

    @verbatim
    x=1 --> (int) 0.1  * 10^1 = 1
    x=2 --> (int) 0.73 * 10^1 = 7
    x=3 --> (int) 0.26 * 10^1 = 2
    @endverbatim

    With a better precision, e.g., decimal_places = 2, we are scaling the
    probability distribution as follows:
    @verbatim
    x=1 --> (int) 0.1  * 10^2 = 10
    x=2 --> (int) 0.73 * 10^2 = 73
    x=3 --> (int) 0.26 * 10^2 = 26
    @endverbatim

    After scaling, the distribution is normalised as to sum up to 1.
*/
RndGenDistribution* RndGenDistribution_new_fromFunction(
    RndGen* rnd, double (*probability)(unsigned long), unsigned long x_min,
    unsigned long x_max, int decimal_places);

/*!	@brief
                Sets the value of the probability of integer x to p in _this
   RndGenDistribution. Value p is approximated to _this->decimal_places decimal
   places (see  RndGenDistribution_new()  and
   RndGenDistribution_new_fromFunction() )
*/
void RndGenDistribution_set(RndGenDistribution* _this, unsigned long x,
                            double p);

/*! @brief
                Gets the probability value of x
*/
double RndGenDistribution_get(RndGenDistribution* _this, unsigned long x);

/*!	@brief
                Gets the minimum value of the domain on which _this
   RndGenDistribution is defined
*/
unsigned long RndGenDistribution_domain_min(
    const RndGenDistribution* const _this);

/*!	@brief
                Gets the maximum value of the domain on which _this
   RndGenDistribution is defined
*/
unsigned long RndGenDistribution_domain_max(
    const RndGenDistribution* const _this);

/*!	@brief
                Gets the RndGen used by _this RndGenDistribution
*/
RndGen* RndGenDistribution_getRndGen(RndGenDistribution* _this);

/*!	@brief
                Gets a domain value of _this RndGenDistribution according to the
   defined probabilities
*/
unsigned long RndGenDistribution_next(RndGenDistribution* _this);

/*! @brief
                Frees memory allocated for *thisP and sets *thisP to NULL
*/
void RndGenDistribution_free(RndGenDistribution** thisP);

#endif
