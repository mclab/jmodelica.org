/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef __RANGE_DOUBLE_H__
#define __RANGE_DOUBLE_H__

/*! @file
                @brief
                        An instance of the Range_Double data structure defines a
   finite set of doubles.
                @date 2014-11-15
*/

/*! @brief
                An instance of the Range_Double data structure defines a finite
   set of doubles.
                @details
                A Range_Double is specified by three values:

                - minimum value 'min'

                - maximum value 'max'

                - step value 'step'

                A Range_Double defines the following finite set of doubles:
                        <pre>{ x  | min <= x <= max & (x-min) = k*step, for some
   integer k }</pre>

                For example, the following Range_Double:  (min=0.0, max=10.0,
   step=2.8) defines the following set of doubles: <pre>{ 0, 0+2.8=2.8,
   2.8+2.8=5.6, 5.6+2.8=8.4 }</pre>

                Using  Range_Double_Iterator_new()  it is possible to iterate
   over these values forward and backward.

                Using  Range_Double_Iterator_new_flipping()  it is possible to
   iterate over these values starting from the median value, in a flipping
   fashion. For the Range_Double example above, a flipping iterator would
   produce the following sequence: <pre>[5.6, 2.8, 8.4, 0]</pre>
*/
typedef struct Range_Double Range_Double;

/*! @brief
                An instance of the Range_Double_Iterator data structure defines
   an iterator over a Range_Double.
*/
typedef struct Range_Double_Iterator Range_Double_Iterator;

/*!	@brief
                Creates a new Range_Double
*/
Range_Double* Range_Double_new(double min, double max, double step);

/*!	@brief
                Creates a new Range_Double from a string following format:
                  \verbatim[<min>{{, <max>}]{/<step>}}\endverbatim
                  or
                  min

                where: "[" and "]" are terminal characters, and {...} denote an
   optional part.

                Examples:

                        [3.5, 5.4]/2.3  --> min=3.5, max=5.4, step=2.3
                        [3.5, 5.4]  	--> min=3.5, max=5.4, step=1
                        [3.5]	  		--> min=3.5, max=3.5, step=1
                        3.5				--> min=3.5, max=3.5,
   step=1
*/
Range_Double* Range_Double_new_fromString(const char* string);

/*!	@brief
                Gets the minimum value of _this Range_Double
*/
double Range_Double_min(const Range_Double* const _this);

/*!	@brief
                Prints-out a textual representation of _this Range_Double to
   file f
*/
void Range_Double_fprint(const Range_Double* const _this, FILE* f);

void Range_Double_toString(const Range_Double* _this, char* result,
                           size_t maxlen);

/*! @brief
                Checks whether _this and other are equal
*/
int Range_Double_equals(const Range_Double* _this, const Range_Double* other);

/*!	@brief
                Gets the maximum value of _this Range_Double
*/
double Range_Double_max(const Range_Double* const _this);
/*!	@brief
                Gets the 'step' value of _this Range_Double
*/
double Range_Double_step(const Range_Double* const _this);

/*!	@brief
                Gets the element of _this Range_Double corresponding to the
   given index.

        @details
                If index < 0, the function returns the min value of _this
   Range_Double. If index >= Range_Double_size(_this)-1, the function returns
   the max value of _this Range_Double.
*/

double Range_Double_value(Range_Double* const _this, int index);

/*!	@brief
                Gets the element of _this Range_Double closest to the given
   value.

        @details
                In case 'value' is exactly (v + v')/2  for two successive values
   v and v' belonging to _this Range_Double, the function returns v'.
        @return
                The closest value to 'value' belonging to _this Range_Double.
*/
double Range_Double_closest_value(Range_Double* const _this, double value);

/*!	@brief
                Gets the index of the element in _this Range_Double closest to
   the given value.

        @details
                In case 'value' is exactly (v + v')/2  for two successive values
   v and v' belonging to _this Range_Double, the function returns the index of
   v'.
        @return
                The index of the closest value to 'value' belonging to _this
   Range_Double. If value < min, _this function returns 0. If value > max, _this
   function returns the last legal index of _this range.

*/
int Range_Double_closest_index(Range_Double* const _this, double value);

/*!	@brief
                Gets the number of elements in _this Range_Double (under basic
   semantics)
*/
int Range_Double_size(Range_Double* const _this);

/*!	@brief
                Frees memory used by *thisP and sets *thisP to NULL
*/
void Range_Double_free(Range_Double** thisP);

/*! @brief
                Creates a new iterator for _this Range_Double
        @details
                The created iterator will scan all values in _this Range_Double
   in ascending order.
*/
Range_Double_Iterator* Range_Double_Iterator_new(Range_Double* const range);

/*! @brief
                Creates a new flipping iterator for _this Range_Double
        @details
                The created iterator will scan all values in _this Range_Double,
   starting from the middle value and oscillating.

                For example, for the following Range_Double:  (min=0.0,
   max=10.0, step=2.8), which defines the following set of doubles: <pre>{ 0,
   0+2.8=2.8, 2.8+2.8=5.6, 5.6+2.8=8.4 }</pre>

                a flipping iterator would produce the following sequence:
                        <pre>[5.6, 2.8, 8.4, 0]</pre>
*/
Range_Double_Iterator* Range_Double_Iterator_new_flipping(
    Range_Double* const range);

/*! @brief
                Checks whether a call to function  Range_Double_Iterator_next()
   on _this iterator will produce a value. _this function returns always 1 if
   _this is a looping iterator.
        @return
                1 if true, 0 if false
*/
int Range_Double_Iterator_hasNext(Range_Double_Iterator* const _this);

/*! @brief
                Writes in *result the next value of _this iterator.
        @return
                1 if the next value exists (i.e., a call to
   Range_Double_Iterator_hasNext() would return 1), 0 otherwise
*/
int Range_Double_Iterator_next(Range_Double_Iterator* const _this,
                               double* result);

/*! @brief
                Rewinds _this iterator, as it were just created
*/
void Range_Double_Iterator_rewind(Range_Double_Iterator* const _this);

/*! @brief
                Frees memory allocated to *thisP iterator and sets *thisP to
   NULL
*/
void Range_Double_Iterator_free(Range_Double_Iterator** thisP);

#endif
