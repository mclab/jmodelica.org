/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef __MCLABUTILS__
#define __MCLABUTILS__

/*! @file
		@brief Includes all individual header files.
*/

#include <Array.h>
#include <BasicTypeValue.h>
#include <CommandLineInterface.h>
#include <CSV.h>
#include <Debug.h>
#include <FileUtils.h>
#include <HashSet.h>
#include <HashTable.h>
#include <Numbers.h>
#include <ProcStats.h>
#include <Properties.h>
#include <Range_Double.h>
#include <Range_Int.h>
#include <RndGen.h>
#include <RndGenDistribution.h>
#include <Stats.h>
#include <StringUtils.h>
#include <Timer.h>
#include <TupleGenerator.h>
#include <Serializable.h>

#endif
