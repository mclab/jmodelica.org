/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef __DEBUG_H__
#define __DEBUG_H__

/*! @file
    @brief
      The Debug package allows developers to easily program with assertions.
    @details
      _this programming style:
      <ul>
      <li> reduces the risk of introducing bugs, and
      <li> allows the programmer to find a bug much easier
      </ul>
    For each program unit (e.g., a file defining a data-structure well as its
   related functions), do the following: <ul> <li><pre>#define DEBUG
   "DataStructure_Name"</pre> where DataStructure_Name is a string identifying
        the file/data-structure, e.g., its name. _this is called a "Debug
   category" (the macro name, e.g., DEBUG, can be whatever.) <li> For each
   function do the following:

    @verbatim
    int myFunction(MyDataStructure* _this, int a, int b) {
      // Check pre-conditions: _this must be != NULL
      Debug_assert(DEBUG, _this != NULL, "_this == NULL\n");

      // Check pre-conditions: a must be less than b (note the extra arguments,
   a-la printf) Debug_assert(DEBUG, a < b, "a = %d >= b = %d\n", a, b);
      ...
      a += b;
      // Debug messages
      Debug_out(DEBUG, "Now a = %d\n", a);
      ...
      Debug_assert(DEBUG, a == myOtherWayOfComputingA(), "Computation of a is
   wrong\n");

      // Executes a function/instruction
      Debug_perform(DEBUG, printMyDataStructure() );

      int myarray[10] = {0}
      // populate myarray
      ...

      // Print-out a debug message showing the content of array 'myarray'
      Debug_outArray(DEBUG,
        "int myarray holding my numbers", // header to be printed
        "%d", // printf format to print out each array entry
        myarray, // array var
        10 // array length
      );
    }
    @endverbatim
    </ul>

    Assertions and debug messages are enabled by default for all categories,
    but can be disabled in the main() function on a category basis:
    <ul>
    <li><pre>Debug_setDefaultAssertions(boolean value);</pre>
      enables/disables default value for assertion checking

    <li><pre>Debug_setDefaultOut(boolean value);</pre>
      enables/disables default debug output (Debug_out)
      and function execution (Debug_perform)

    <li><pre>Debug_enableOut(category);</pre>
    explicitly enables debug output (Debug_out) and function execution
    (Debug_perform) for category 'category'

    <li><pre>Debug_disableOut(category);</pre>
      explicitly disables debug output and function execution
      for category 'category'

    <li><pre>Debug_enableAssertions(category);</pre>
      explicitly enables assertion checking (Debug_assert)
      for category 'category'

    <li><pre>Debug_disableAssertions(category);</pre>
      explicitly disables assertion checking
      for category 'category'
    </ul>

    Debug output and assertion checking for the special category "ALWAYS"
    (in <tt>#define</tt>-ed constants DEBUG_ALWAYS and DEBUG_ALL (DEBUG_ALL is
   deprecated)) are ALWAYS enabled.

    To disable all assertion checking at once (for, e.g., production code),
   simply compile with -DNDEBUG.

    Assertion checking and debug output are encoded as macros, so if -DNDEBUG is
   enabled during compilation, there will be NO OVERHEAD in computation. All
   Debug_out, Debug_perform and Debug_assert are simply IGNORED by the compiler.

    Before terminating function main(), you should call Debug_freeAll() to
   deallocate the memory used by the Debug package.

    @warning Disabling assertion checking is strongly discouraged.
    Typically there is only very little performance gain.
*/

#include <assert.h>
#include <stdio.h>
#include <time.h>

#include <Properties.h>

#include <Timer.h>

#define DEBUG_ALWAYS "ALWAYS"
#define DEBUG_ALL "ALWAYS" // for back-ward compatibility, to be removed
#define CATEGORY_MAX_LENGTH 1024

/*! @brief
                Tells the package whether to execute  Debug_out()  and
   Debug_perform() commands for Debug categories (default behaviour)
        @param d
                1 = true, 0 = false
*/
void Debug_setDefaultOut(int d);

/*! @brief
                Tells the package whether to execute  Debug_assert()
                commands for Debug categories
                (default behaviour)
        @param d
                1 = true, 0 = false
*/
void Debug_setDefaultAssertions(int d);

/*! @brief
                Enables the execution of  Debug_out()  and  Debug_perform()
                commands for Debug category 'id'
*/
void Debug_enableOut(const char *const id);

/*! @brief
                Enables the execution of  Debug_assert()
                commands for Debug category 'id'
*/
void Debug_enableAssertions(const char *const id);

/*! @brief
                Enables the execution of all Debug commands (assertion checking,
   debug output, function executions) for Debug category 'id'
*/
void Debug_enable(const char *const id);

/*! @brief
                Disables the execution of  Debug_out()  and  Debug_perform()
                commands for Debug category 'id'
*/
void Debug_disableOut(const char *const id);

/*! @brief
                Disables the execution of  Debug_assert()
                commands for Debug category 'id'
*/
void Debug_disableAssertions(const char *const id);

/*! @brief
                Disables the execution of all Debug commands (assertion
   checking, debug output, function executions) for Debug category 'id'
*/
void Debug_disable(const char *const id);

/*! @brief
                Returns the current time in milliseconds (from the Unix epoch)
*/

/*! @brief
                Returns the current indent string to be prepended to each debug
   output message.
*/
const char *Debug_indent_string();

void Debug_setIndentAmount(unsigned n);
void Debug_setMaxIndent(unsigned n);
void Debug_setIndentChar(char c);
void Debug_outNest_real_function();
void Debug_outUnnest_real_function();

unsigned long long Debug_timestamp_millisec();
#define TIMESTAMP (Debug_timestamp_millisec())

#define _OUT 1
#define _ASSERTIONS 2

/*! @brief
                Checks whether assertion checking or debug output/function
   execution is enabled for category 'id'
        @param id
                Debug category
        @param WHAT
                1 = debug output/function execution; 2 = assertion checking
*/
int Debug_isEnabled(const char *const id, int WHAT);

/*!	@brief
                Frees the memory allocated by the Debug package.

        @details
                _this function should be invoked before terminating the main
   program (to avoid false positives with Valgrind)
*/
void Debug_freeAll();

#ifndef NDEBUG
#ifndef NDEBUG_OUT
/*! @brief
                If debug output for Debug category 'id' is enabled,
                then outputs the current timestamp (in milliseconds from the
   Unix epoch) and a message
        @param id
                Debug category
        @param ...
                A string (with printf placeholders) followed by additional
   arguments (as in printf)
*/
#define Debug_out(id, ...)                                                     \
	if (Debug_isEnabled(id, _OUT)) do {                                        \
			fprintf(stderr, "[timestamp=%llu] %s:%d: %s", TIMESTAMP, __FILE__, \
			        __LINE__, Debug_indent_string());                          \
			fprintf(stderr, __VA_ARGS__);                                      \
	} while (0)

#define Debug_outNoPredix(id, ...)                        \
	if (Debug_isEnabled(id, _OUT)) do {                   \
			fprintf(stderr, "%s", Debug_indent_string()); \
			fprintf(stderr, __VA_ARGS__);                 \
	} while (0)
/*! @brief
                If debug output for Debug category 'id' is enabled,
                then increases the nesting level of output messages.
*/
#define Debug_outNest(id)                  \
	if (Debug_isEnabled(id, _OUT)) do {    \
			Debug_outNest_real_function(); \
	} while (0)

/*! @brief
                If debug output for Debug category 'id' is enabled,
                then decreases the nesting level of output messages.
*/
#define Debug_outUnnest(id)                  \
	if (Debug_isEnabled(id, _OUT)) do {      \
			Debug_outUnnest_real_function(); \
	} while (0)

/*! @brief Executes function f if debug output for Debug category 'id' is
   enabled
        @param id
                Debug category
        @param f
                A function name with arguments
*/
#define Debug_perform(id, f)            \
	if (Debug_isEnabled(id, _OUT)) do { \
			f;                          \
	} while (0)

/*! @brief Outputs the content of a C array if debug output for Debug category
   'id' is enabled
        @param id
                Debug category
        @param name
                The name of the array (a string, to be used in the first output
   line)
        @param format
                A string containing the proper printf placeholder for the
   datatype of array elements
        @param array
                The array to print
        @param length
                The length of the array
*/
#define Debug_outArray(id, name, format, array, length)             \
	if (Debug_isEnabled(id, _OUT)) do {                             \
			Debug_out(id, "Array %s (length %d):\n", name, length); \
			for (int i = 0; i < length; i++) {                      \
				Debug_out(id, "  [%d] --> ", i);                    \
				fprintf(stderr, format, array[i]);                  \
				fprintf(stderr, "\n");                              \
			}                                                       \
			Debug_out(id, "end\n");                                 \
	} while (0)

#else // case NDEBUG_OUT defined
#define Debug_out(...) ((void) 0)
#define Debug_outNest(...) ((void) 0)
#define Debug_outUnnest(...) ((void) 0)
#define Debug_perform(id, f) ((void) 0)
#define Debug_outArray(id, name, format, array, length) ((void) 0)

#endif

/*! @brief
                If debug output for Debug category 'id' is enabled, raises an
   assertion failure if condition 'cond' is false
        @param id
                Debug category
        @param cond
                A boolean expression that to check (assertion)
        @param ...
                A error message string (with printf placeholders) followed by
   additional arguments (as in printf)
*/
#define Debug_assert(id, cond, ...)                                    \
	if (Debug_isEnabled(id, _ASSERTIONS)) do {                         \
			int condRes = (cond);                                      \
			if (!(condRes)) {                                          \
				fprintf(stderr, "[timestamp=%llu] %s:%d: ", TIMESTAMP, \
				        __FILE__, __LINE__);                           \
				fprintf(stderr, __VA_ARGS__);                          \
			}                                                          \
			assert(condRes);                                           \
	} while (0)

#else // case NDEBUG defined

#define Debug_assert(id, cond, ...) ((void) 0)

#define Debug_out(...) ((void) 0)
#define Debug_outNest(...) ((void) 0)
#define Debug_outUnnest(...) ((void) 0)
#define Debug_outArray(id, name, format, array, length) ((void) 0)
#define Debug_perform(id, f) ((void) 0)

#endif

#endif
