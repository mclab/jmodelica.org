/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef __TIMER_H__
#define __TIMER_H__

#define TIMER_NAME_MAXLEN 1024

#define TIMER_TO_SEC(x) ((double) ((x)/1000.0))
#define TIMER_TO_MSEC(x) ((unsigned long long) ((x)*1000.0))

/*! @file
		@brief
			Timers allow the programmer to easily measure the time elapsed beetween two points in the program.
*/

/*!	@brief
		Creates and starts a new timer, associated to (unique) name 'name'

*/
void Timer_start(const char* const name);

/*!	@brief
		Creates and starts a new timer from a given value, associated to (unique) name 'name'

*/
void Timer_startFromValue(const char* const name, unsigned long long value);

/*!	@brief
		Get timer value associated to name 'name'
*/
unsigned long long Timer_getCurrentValue(const char* const name);

/*!	@brief
		Terminates (and destroys) the (previously started) timer 'name' and returns the elapsed time in milliseconds
*/
unsigned long long Timer_end(const char* const name);


#endif

