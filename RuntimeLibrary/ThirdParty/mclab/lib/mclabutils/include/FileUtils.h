/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef __FILE_UTILS__
#define __FILE_UTILS__


#include <Array.h>
/*!	@file
		@brief
			The FileUtils package provides additional functions to work with files in C
	@date 2014-11-15
*/

/*! @brief
	Fill 'lines' with the lines of file 'fileName'
*/
void FileUtils_lineByline(const char *fileName, Array* lines);


/*! @brief
		Fill 'fileNames' with all files into a directory 'dirName' with extension 'fileExtension' with a depth of 'depth'
*/
void FileUtils_listDir(const char * dirName, char *fileExtension, int depth, Array *fileNames);


/*! @brief
		Copies file 'from' to 'to', using a fast algorithm.
   	@return
 		0 on success and -1 on error
*/
int FileUtils_copyFile(const char *from, const char *to);

/*! @brief
		Checks whether 'path' denotes a regular file
   	@return
 		1 if yes, 0 if no
*/
int FileUtils_isFile(const char* const path);

/*! @brief
		Checks whether 'path' denotes a directory
   	@return
 		1 if yes, 0 if no
*/
int FileUtils_isDir(const char* const path);

/*! @brief
		Writes into *out_dir the portion of 'path' identifying the deepest directory
		in which 'path' resides
   	@return
 		out_dir
*/
const char* FileUtils_dirName(const char* const path, char* const out_dir);

/*! @brief
		Writes into *out_fname the portion of 'path' identifying the file name of 'path'
	@details
		If path identifies a file, out_fname will contain its file name.
		If path identifies a directory, out_fname will contain the directory name.
   	@return
 		out_fname
*/
const char* FileUtils_baseName(const char* const path, char* const out_fname);

#endif

