/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef __HASH_SET__
#define __HASH_SET__

#include <stdio.h>
#include <stdlib.h>
#include "HashTable.h"

/*! @file
                @brief
                        An instance of the HashSet data structure defines a
   generic hash-set.
                @date 2014-11-12
*/

/*! @brief
                An instance of the HashSet data structure defines a generic
   hash-set.

        @details
                Elements in a set are generic pointers (void*), thus
                they can point to instances of arbitrary structures.
*/
typedef struct HashSet HashSet;

/*!
        @brief Creates a new empty hashset, whose key elements are keySize bytes
   long.

        @param keySize
                The size in byte of each key

        @return
                A new HashSet instance whose elements must have size keySize
   bytes
*/
HashSet* HashSet_new(size_t keySize);

/*!	@brief
                Returns true if and only if _this hashset contains element elem

        @return
                1 if _this contains elem, 0 otherwise

*/
int HashSet_contains(const HashSet* const _this, const void* const elem);

/*! @brief
                Adds a copy of the data structure instance pointed by elem to
   _this hashset

        @return
                1 if and only if elem has been successfully added to _this, 0
   otherwise (i.e., _this already contains elem)
*/
int HashSet_add(HashSet* const _this, const void* const elem);

/*! @brief
                Removes elem to _this hashset

        @return
                1 if and only if elem has been successfully removed _this, 0
   otherwise (i.e., _this does not contain elem)
*/
void HashSet_remove(HashSet* const _this, const void* const elem);

/*! @brief
                Returns the cardinality (or size) of _this hashset

        @return
                the number of elements in _this hashset
*/
unsigned long HashSet_size(const HashSet* const _this);

/*! @brief
                Frees memory allocated for the hashset pointed to by *_this and
   sets *_this to NULL
*/
void HashSet_free(HashSet** const _this);

/*! @brief
                Checks whether _this contains all elements of other, i.e.,
   whether each element of other is identical (bit by bit) to an element of
   _this. If other == NULL, result = 1.
*/
int HashSet_containsAll(const HashSet* _this, const HashSet* other);

/*! @brief
                Checks whether _this equals other. Two HashSets are equal iff
   they are both NULL or: - they have the same size and - each element of _this
   is identical (i.e., bit by bit) to an element of other.

                Note: _this method does not implement deep-equals (which has
   still to be imlemented by taking an additional user-defined elem_equals()
   function pointer).
*/
int HashSet_equals(const HashSet* _this, const HashSet* other);

/*! @brief An instance of _this data structure defines an iterator over a
 * HashSet
 */
typedef struct HashSetIterator HashSetIterator;

/*! @brief
                Creates a new iterator for _this HashSet
*/
HashSetIterator* HashSetIterator_new(const HashSet* const _this);

/*! @brief
                Checks whether _this iterator has more elements to return

        @return
                1 if a call to HashSetIterator_next(_this) will return a
   non-NULL entry, 0 otherwise
*/
int HashSetIterator_hasNext(const HashSetIterator* const _this);

/*! @brief
                Returns a pointer to the next element of the HashSet which _this
   iterator is iterating on.

                So, for example:
                  - if the set elements are basic types or structs, it returns a
   pointer to a basic type or struct value; - if the set elements are pointers,
   it returns a double-pointer.
*/
const void* HashSetIterator_next(HashSetIterator* const _this);

/*! @brief
                Frees memory used by _this iterator. Also sets *_this to NULL.
*/
void HashSetIterator_free(HashSetIterator** const _this);

#endif
