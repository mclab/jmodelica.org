/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef __HASHTABLE__
#define __HASHTABLE__

#include <stddef.h>
#include <stdio.h>

/*! @file
                @brief
                        An instance of the HashTable data structure defines a
   generic hash-table.
                @date 2014-11-21
*/

/*! @brief
                An instance of the HashTable data structure defines a generic
   hash-table.

        @details
                Both keys and values are generic pointers (void*), thus
                they can point to instances of arbitrary structures.

                Values stored in a HashTable can be NULL.
*/
typedef struct HashTable HashTable;

/*! @brief
                Return the size in bytes of an instace of HashTable
*/
size_t HashTable_sizeof();

/*!
        @brief
                An instance of _this data structure defines an entry of a
   generic hash-table.
*/
typedef struct HashTable_Entry HashTable_Entry;

/*!	@brief
                Returns a pointer to the key of _this hashtable entry
*/
const void* HashTable_Entry_key(const HashTable_Entry* _this);

size_t HashTable_Entry_keySize(HashTable_Entry const *_this);

/*!	@brief
                Returns a pointer to the value of _this hashtable entry.
*/
void* HashTable_Entry_value(const HashTable_Entry* _this);

/*! @brief
                Returns the size of an entry in _this hashtable.
                The size includes:
                        (i) the size to store a key value, and (ii) the size of
   a HashTable_Entry instance (which include pointers to both key and value).
*/
size_t HashTable_Entry_sizeof(const HashTable* _this);

/*!
        @brief Creates a new empty hashtable, whose key elements are keySize
   bytes long and values are pointers.

        @param keySize
                The size in byte of each key

        @return
                A new HashTable instance.
 */
HashTable* HashTable_new();

/*!
        @brief
                Clones _this
*/
HashTable* HashTable_clone(const HashTable* _this);

/*!
        @brief
                Returns the HashTable_Entry associated to the key value pointed
   by 'key' in _this HashTable, or NULL if no such entry exists.

        @param keyP
                a pointer to the key. The function will search for
                an entry in the HashTable whose key is an identical copy of
   value pointed by key as key (i.e., not the value of the pointer itself).
 */
HashTable_Entry* HashTable_entry(const HashTable* _this, const void* keyP, size_t key);

/*!
        @brief
                Returns the value associated to the key value pointed by 'key'
   in _this HashTable, or NULL if no such value exists.

        @param key
                a pointer to the key. The function will search for
                an entry in the HashTable whose key is an identical copy of
   value pointed by key as key (i.e., not the value of the pointer itself).

        @param valueP
                the address of a pointer to the value. The function will store
                the returned value into *valueP (NULL if no pair exists).

        @warning
                _this function returns NULL in two cases: (i) if key does not
   occur in _this HashTable and (ii) if key is associated to value NULL.

                To distinguish between these two cases, use
                 HashTable_entry()  instead.

        @return *valueP
 */
void* HashTable_get(const HashTable* _this, const void* key, size_t keySize, void** valueP);

/*!	@brief
                Gets the size in bytes of the key column of _this HashTable
*/
size_t HashTable_keySize(const HashTable* _this);

/*!
        @brief Adds pair (*key, value) to _this HashTable, removing any other
        (*key, value') pair.

        @param key
                a pointer to the key. The HastTable will store
                a copy of the value pointed by key as key (i.e., not the
   pointer). _this means that a call to free(key) will have no destroying
   effects on the HashTable content.

        @param value
                a pointer to the value. The HashTable will store
                the pointer itself as value (i.e., it will not duplicate the
   value in another memory location).

        @warning
                It must be value != NULL. In other words, it is not possible to
   store NULL values in a HashTable.

        @return
                a pointer to the old value if exists, otherwise NULL
 */
void* HashTable_put(HashTable* _this, const void* key, size_t keySize, void* value);

/*!
        @brief
                Deletes the pair having key *key from HashTable _this, if one
   exists.

        @details
                The function looks for a key value which is an identical copy
                of the value pointed by key (i.e., *key, not the pointer
   itself). If one such entry is found in the hashtable, it is deleted (freeing
   up memory to hold the hashtable entry, i.e., the key value and the pointer to
                the value.
                The value itself is not freed, as the hashtable keeps only a
   pointer to it).

        @param _this
                a pointer to the invocation object

        @param key
                a pointer to the value to the key to search

        @return
                a pointer to the old value of 'key' if exists (otherwise NULL)
 */
void* HashTable_remove(HashTable* _this, const void* key, size_t keySize);

/*! @brief
                Returns the number of entries in _this HashTable
*/
unsigned long HashTable_size(const HashTable* _this);

/*!
        @brief Frees the memory allocated for the HashTable _this and sets
   *_this to NULL.

        In particular:

         - the memory storing the key values is freed (as key values are copied)

         - the memory storing pointers to values is freed

         - the memory storing value is _not_ freed (as the HashTable stores
   pointers to values, not the values themselves).
*/
void HashTable_free(HashTable** const _this);

/* REMOVED, as these have been replaced by HashTable_fprint()
void HashTable_print(FILE* stream, const HashTable* const _this, char*
(*keyToString)(const void* keyP, char* str), char* (*valueToString)(const void*
valueP, char* str), unsigned int keyMaxStrLen, unsigned int valueMaxStrLen);

char* HashTable_dumpToString(char* string, const HashTable* const _this, char*
(*keyToString)(const void* keyP, char* str), char* (*valueToString)(const void*
valueP, char* str));
*/

/*! @brief
                Sets call-back function to fprint key values of _this hashtable
   to file handler f

        @param fprint_key
                The function to call back when fprinting the key values of _this
   hashtable. _this fprint_key function takes as arguments a pointer to the key
   (as a void* pointer) and a pointer to the output file stream.
*/
void HashTable_set_fprint_key(HashTable* _this,
                              void (*fprint_key)(const void* key, FILE* f));

/*! @brief
                Sets call-back function to fprint values of _this hashtable to
   file handler f

        @param fprint_value
                The function to call back when fprinting the values of _this
   hashtable. _this fprint_value function takes as arguments a pointer to the
   value (as a void* pointer) and a pointer to the output file stream.
*/
void HashTable_set_fprint_value(HashTable* _this,
                                void (*fprint_value)(const void* key, FILE* f));

/*! @brief
                Prints _this hashtable to output file stream f.

        @details
                To use _this function, you first must set proper call-back
   functions to fprint a key pointer (see  HashTable_set_fprint_key ) and a
   value pointer (see  HashTable_set_fprint_value )
*/
void HashTable_fprint(const HashTable* _this, FILE* f);

/*! @brief An instance of _this data structure defines an iterator over a
 * HashTable
 */
typedef struct HashTableIterator HashTableIterator;

/*! @brief
                Return the size in bytes of an instance of HashTableIterator
*/
size_t HashTableIterator_sizeof();

/*! @brief
                Creates a new iterator for _this HashTable
*/
HashTableIterator* HashTableIterator_new(const HashTable* _this);

/*! @brief
                Checks whether _this iterator has more elements to return

        @return
                1 if a call to HashTableIterator_next(_this) will return a
   non-NULL entry, 0 otherwise
*/
int HashTableIterator_hasNext(const HashTableIterator* _this);

/*! @brief Returns the next entry of the HashTable which _this iterator is
   iterating on.

        @return
                a HashTable_Entry instance, or NULL if no more entries exist
*/
HashTable_Entry* HashTableIterator_next(HashTableIterator* _this);

/*!	@brief Rewinds _this iterator as the next call to function
   HashTableIterator_next will return the first entry
*/
void HashTableIterator_rewind(HashTableIterator* _this);

/*! @brief
                Frees memory used by _this iterator. Also sets *_this to NULL.
*/
void HashTableIterator_free(HashTableIterator** _this);

#endif
