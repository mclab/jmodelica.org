/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef __TUPLE_GENERATOR__
#define __TUPLE_GENERATOR__

/*! @file
 */

/*! @brief TupleGenerator
 */
typedef struct TupleGenerator TupleGenerator;

/*! @brief
                Creates a new TupleGenerator
*/
TupleGenerator* TupleGenerator_new();

/*! @brief
                Frees a TupleGenerator.
                If exceptions are enabled, it raises an exception if there
   exists iterators associated to _this.
*/
void TupleGenerator_free(TupleGenerator** thisP);

/*! @brief
                Adds a new element to the tuple-space of _this.
                Tuples generated from '_this' will have values for _this new
  element in set [min, max] intersect { x | exists k integer >= 0, x = min +
  k*step}.

        @param
                min
  @param
                max >= min
  @param
                step > 0
        @returns
                0 in case of success and -1 in case there exist iterators
  associated to _this (hence, _this has become immutable).
*/
int TupleGenerator_addElem(TupleGenerator* _this, double min, double max,
                           double step);

/*! @brief
                Adds n identical elements to the tuple-space of _this.
        @returns
                0 in case of success and -1 in case there exist iterators
   associated to _this (hence, _this has become immutable).
*/
int TupleGenerator_addElem_n_times(TupleGenerator* _this, unsigned n,
                                   double min, double max, double step);

typedef struct TupleGenerator_Iterator TupleGenerator_Iterator;

typedef enum {
  TUPLEGENERATOR_ITERATOR_ALL,
  TUPLEGENERATOR_ITERATOR_ALLDIFF_ASC
} TupleGenerator_Iterator_Type;

/*! @brief
                Creates a new iterator for TupleGenerator gen.

        @param type
                The type of _this iterator. Two types are defined:
                - TUPLEGENERATOR_ITERATOR_ALL: enumerates all tuples in the
   tuple-space of gen - TUPLEGENERATOR_ITERATOR_ALLDIFF_ASC: enumerates all
   tuples in the tuple-space of gen which satisfy the following constraints: a)
   all elements of the tuple have different values b) elements of the tuple are
   in ascending order (reading the tuple from the first to the last element).
*/
TupleGenerator_Iterator* TupleGenerator_Iterator_new(
    TupleGenerator* gen, TupleGenerator_Iterator_Type type);

/*! @brief
                Frees iterator *thisP (and unlinks it from its TupleGenerator)
*/
void TupleGenerator_Iterator_free(TupleGenerator_Iterator** thisP);

/*! @brief
                Rewinds _this (next call to next() will return the same tuple as
   it were the first invocation).
*/
void TupleGenerator_Iterator_rewind(TupleGenerator_Iterator* _this);

/*! @brief
                Returns 1 if _this has a next tuple, 0 otherwise
*/
int TupleGenerator_Iterator_hasNext(TupleGenerator_Iterator* _this);

/*! @brief
                If _this->hasNext() is true, then it fills result with the next
   tuple enumerated by _this.
        @returns
                1 if _this call has produced a meaningful 'result' Array
                0 otherwise (in _this case, value in result is meaningless).
*/
int TupleGenerator_Iterator_next(TupleGenerator_Iterator* _this, Array* result);
#endif
