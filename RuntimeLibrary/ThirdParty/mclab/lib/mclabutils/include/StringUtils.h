/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef __STRING_UTILS__
#define __STRING_UTILS__

/*! @file
                @brief
                        The StringUtils package provides several additional
   functions to work with strings in C
                @date 2014-11-15
*/

#include <Array.h>

/*!	@brief
                Returns the last index of char 'c' in string 'str'
                (-1 if 'c' does not occur in 'str')
*/
int StringUtils_lastIndexOf(const char* str, char c);

/*!	@brief
                Returns the last index of char 'c' in string 'str' which is at
   most 'maxIndex'
                (-1 if 'c' does not occur in 'str' before position 'maxIndex')
*/
int StringUtils_lastIndexOf_atMost(const char* str, char c, int maxIndex);

/*!	@brief
                Clones string 'str' and returns a pointer to the duplicate
   string
*/
char* StringUtils_clone(const char* str);

/*! @brief
                Creates a new string consisting of the substring of 'str' from
   index 'from' to index 'to' (inclusive). Both 'from' and 'to' can be negative:
   in _this case, they count from the end of the string
*/
char* StringUtils_substring(const char* str, int from, int to);

/*! @brief Modifies 'str' by removing white spaces at both ends. Returns a
 * pointer to 'str'.
 */
char* StringUtils_trim(char* str);

/*! @brief
                Returns the first index of char 'c' in string s (-1 if 'c' does
   not occur in 'str')
*/
int StringUtils_strpos(const char* s, char c);

/*!	@brief
                Modifies string 's' by replacing each char in array 'findChar'
   with the respective char in array 'replaceChar'.

        @details
                _this function replaces the i-th char in array 'findChar' with
   the i-th char in 'replaceChar' (for all i ranging from 0 to
   strlen(findChar)-1 = strlen(replaceChar)-1)

                Note that findChar and replaceChar must be of the same length
*/
int StringUtils_replaceAll(char* s, char* findChars, char* replaceChars);

/*!	@brief
                Adds to the end of Array 'tokens' the strings obtained by
   splitting 's' in the points where a character in array 'sep' occurs.

                Example: <tt>StringUtils_tokenize("hello hola:bye", " :",
   tokens)</tt> will add to the end of Array 'tokens' the strings "hello",
   "hola", "bye" (in _this order)
*/
void StringUtils_tokenize(char* s, char* sep, Array* tokens);

/*!	@brief
                Changes string 's' by adding a number of 'pad' characters to the
   right, until the final length of 's' is 'length'
*/
char* StringUtils_rightPad(char* s, size_t length, char pad);

/*!	@brief
                Changes string 's' by adding a number of 'pad' characters to the
   right, until the final length of 's' is 'length'
*/
char* StringUtils_leftPad(char* s, size_t length, char pad);

/*! @brief
                Checks whether 's' starts with string 'needle'
        @return
                1 if true, 0 if false
*/
int StringUtils_startsWith(const char* const s, const char* const needle);

int StringUtils_startsWithIgnoreCase(const char* const s,
                                     const char* const needle);

/*! @brief
                Converts s into lowercase.

        @return
                A pointer to s
*/
char* StringUtils_toLowercase(char* s);

/*! @brief
                Converts s into uppercase
        @return
                A pointer to s
*/
char* StringUtils_toUppercase(char* s);

/*! @brief
                Checks whether 's' ends with string 'needle'
        @return
                1 if true, 0 if false
*/
int StringUtils_endsWith(const char* const s, const char* const needle);

int StringUtils_endsWithIgnoreCase(const char* const s,
                                   const char* const needle);

/*! @brief
                Fills *result with 's' converted into int.
        @return
                0 on success, and an error code (a negative integer) otherwise
*/
int StringUtils_toInt(const char* const s, int* result);

/*! @brief
                Fills *result with 's' converted into long.
        @return
                0 on success, and an error code (a negative integer) otherwise
*/
int StringUtils_toLong(const char* const s, long* result);

/*! @brief
                Fills *result with 's' converted into unsigned int.
        @return
                0 on success, and an error code (a negative integer) otherwise
*/
int StringUtils_toUInt(const char* const s, unsigned int* result);

/*! @brief
                Fills *result with 's' converted into unsigned long.
        @return
                0 on success, and an error code (a negative integer) otherwise
*/
int StringUtils_toULong(const char* const s, unsigned long* result);

/*! @brief
                Fills *result with 's' converted into double.
        @return
                0 on success, and an error code (a negative integer) otherwise
*/
int StringUtils_toDouble(const char* const s, double* result);

/*! @brief
                Fills *result with 's' converted into int (1 for true, 0 for
   false).
        @details
                _this function interprets 's' as 'true' iff s equals the string
   'true' ignoring case.
        @return
                0 on success, and an error code (a negative integer) otherwise
*/
int StringUtils_toBoolean(const char* const s, char* result);

/*!	@brief
                Checks whether 's' equals 's2'
        @return
                1 is 's' equals 's2', 0 otherwise
*/
int StringUtils_equals(const char* const s, const char* const s2);

/*!	@brief
                Checks whether 's' equals 's2' ignoring case
        @return
                1 is 's' equals 's2', 0 otherwise
*/
int StringUtils_equalsIgnoreCase(const char* const s, const char* const s2);

/*!	@brief
                Extends the length of string 's' and initializes new-allocated
   memory with '\0'
*/
char* StringUtils_extend(char* s, size_t length);

#endif
