/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef __SERIALIZABLE__
#define __SERIALIZABLE__
#include <Properties.h>
#include <stdbool.h>

/*! @file
    @brief
        An instance of the Serializable data structure defines a generic
   serializable object.
*/

/*! @brief
        An instance of the Serializable data structure defines a generic
   serializable object.

    @details
        In other words, each Serializable instance is a "wrapper" to a specific
   data structure
        that allows easy save and restore.
*/
typedef struct Serializable Serializable;

typedef Serializable *(*toSerializable)(void *obj);
typedef void *(*reallocArray)(void *arrayPtr, size_t size);

/*! @brief
        An instance of the SerializableMethods data structure defines a set of
   function pointers
        that are essential for specific-object serialization (and
   deserialization).
*/
struct SerializableMethods {
  const char *(*className)(void *subInstance);
  const void *passport;
  void (*serializeFields)(void *subInstance);
  void (*deserializeFields)(void *subInstance);
  reallocArray reallocArrayField;
};

/*! @brief
        An instance of the SerializableMethods data structure defines a set of
   function pointers
        that are essential for specific-object serialization (and
   deserialization).
*/
typedef struct SerializableMethods SerializableMethods;

/*! @brief
        Creates a new instance of the Serializable data structure

        @param subInstance
                The pointer to the object to serialize

        @param methods
                Functions for assembling and disassembling the specific object
*/
Serializable *Serializable_new(void *subInstance, SerializableMethods methods);
/*! @brief
    Change the subInstance of this_ with the subInstance passed in input. The
   invocation
    of such function is allowed only by the subInstance itself (through the
   passport mechanism).
        @param _this
                The Serializable to change

        @param subInstance
                The new subInstance

        @param passport
                It's used to check if method invocation is authorized
*/
void Serializable_changeSubInstance(Serializable *this_, void *subInstance,
                                    const void *passport);

/*! @brief
        Adds the value of data structure's attr field into an istance of the
   Properties data structure (that is in turn
        contained in the Serializable this_)

        @param _this
                The Serializable to update with the new field

        @param attr
                The field name

        @param type
                The field type

        @param valueP
                The field value

        @param passport
                It's used to check if method invocation is authorized
*/
void Serializable_setField(Serializable *this_, const char *name,
                           Properties_Content_Type type, void *valueP,
                           const void *passport);

/*! @brief
        Gets the value of data structure's attr field from an istance of the
   Properties data structure (that is in turn
        contained in the Serializable this_)

        @param _this
                The Serializable containing the up-to-date fields' values

        @param attr
                The field name

        @param passport
                It's used to check if method invocation is authorized

        @return
                The pointer to the value
*/
const void *Serializable_getField(Serializable *_this, char *attr,
                                  const void *passport);

/*! @brief
      Adds array to fields of _this Serializable.

        @param _this
                The Serializable to update

        @param name
                The field name

        @param type
                The field type

        @param array
                The array to add

        @param elem_size
                The size of elements in the array

        @param sizes
                An array containing length of each dimension

        @param f
                A function which returns a Serializable from an array element
   value, it can be NULL when array type is a plain type

        @param depth
                A depth value denoting number of dimensions (e.g. plain array
   has 0 depth, matrix has 1 depth, etc.)

        @param elem_is_a_struct
                A boolean value which must be setted to true if the type of the
   array is a struct and false otherwise.

        @param r
                A function for reallocating the array. It is needed when the
   size of a serialized array could be greater than the size of the already
   allocated array of current object. It can be used if and only if the array
   has 1 dimension.

        @param passport
                It's used to check if method invocation is authorized

        @return
                The pointer to the value
*/
void Serializable_setArrayField(Serializable *this_, const char *name,
                                Properties_Content_Type type, void *array,
                                size_t elem_size, size_t const *sizes,
                                toSerializable f, unsigned int depth,
                                bool elem_is_a_struct, reallocArray r,
                                const void *passport);

/*! @brief
        Return the array 'array' filled with updated element values from
   Serializable this_

        @param _this
                The Serializable containing the up-to-date field values

        @param attr
                The field name

        @param array
                The array to fill

        @param elem_size
                The size of elements in the array

        @param sizes
                An array containing length of each dimension

        @param depth
                A depth value denoting number of dimensions (e.g. plain array
   has 0 depth, matrix has 1 depth, etc.)

        @param passport
                It's used to check if method invocation is authorized

        @return
                The pointer to the value
*/
const void *Serializable_getArrayField(Serializable *_this, char *attr,
                                       void *array, size_t elem_size,
                                       size_t const *sizes, unsigned int depth,
                                       const void *passport);

/*! @brief
        Serializes the Serializable instance saving it into an instance of the
   Properties data structure

        @param _this
                The Serializable to serialize

        @param serialized
                The istance of the Properties data structure including the old
   serialized object (if any)
*/
void Serializable_serializeInto(Serializable *_this, Properties *serialized);

/*! @brief
        Loads the serialized object from the Properties instance and updates
         the Serializable instance.

        @param this_
                The Serializable to update

        @param serialized
                The istance of the Properties data structure including the
   serialized object
*/
void Serializable_deserializeFrom(Serializable *_this, Properties *serialized);

/*! @brief
        Free the memory allocated for the Serializable this_ and sets *this_ to
   NULL.
 */
void Serializable_free(Serializable **thisPtr);

/*! @brief
        Initialize Serialization execution environment.
 */
void Serializable_init();

/*! @brief
        Terminate Serialization execution environment.
 */
void Serializable_finalize();

char *Serializable_deserializeString(char *dst, const char *src);

void Serializable_fprintFields(Serializable *this_, FILE *f);
/*! @brief
    Return the className of the subInstance of this_.
 */
const char *Serializable_className(const Serializable *this_);

/*! @brief
        Reduce array size by removing unused Properties from array->fields.

        @param this_
                The Serializable to update

        @param attr
                The name of the array field

        @param attr_length
                The name of the field in this_->fields containing array's
   length, if any

        @param type
                The type of field attr_length

        @param size
                New size for the array

        @param passport
                It's used to check if method invocation is authorized
*/
void Serializable_reduceArraySize(Serializable *this_, char *attr,
                                  char *attr_length,
                                  Properties_Content_Type type, void *size,
                                  const void *passport);

/*! @brief
        Replace oldArray with newArray in global variable arrayPtrs, and update
   Serializable's subInstance.

        @param oldArray
                The array that will be replaced

        @param newArray
                The array that will replace oldArray
*/
void Serializable_replaceArrayPtr(void *oldArray, void *newArray);

#endif
