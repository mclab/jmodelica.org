/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef __PROCSTATS_H__
#define __PROCSTATS_H__

/*! @file
	@brief
		The ProcStats package allows to compute statistics over memory usage.

	@date 2016-01-27
*/


/*!	@brief
		Returns the max resident memory size of the caller process
*/
long ProcStats_selfMaxResidentSetSize();


/*!	@brief
		Computes the current memory usage (virtual memory size and resident set size) in kilobytes of the caller process.
*/
int ProcStats_selfCurrentMemoryUsage(long* vmrss_kb, long* vmsize_kb);

#endif

