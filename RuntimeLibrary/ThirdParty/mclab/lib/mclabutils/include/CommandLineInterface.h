/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef __COMMAND_LINE_INTERFACE__
#define __COMMAND_LINE_INTERFACE__

#include <Properties.h>

/*! @file
                @brief
                        An instance of the CommandLineInterface data structure
   defines the interface of a command line tool, in terms of allowed options and
   flags and help messages.
                @date 2014-11-12
*/

/*! @brief
                An instance of the CommandLineInterface data structure defines
   the interface of a command line tool, in terms of allowed options and flags
   and help messages.
*/
typedef struct CommandLineInterface CommandLineInterface;

/*!	@brief
                Creates a new CommandLineInterface instance

        @param args0
                The name of the tool (_this is typically args[0], i.e., the
   first element of the char** argument of main())

        @param syntaxHeader
                A string giving a short description of the tool. It is displayed
   as the first output line if syntax help is requested by the user.
*/
CommandLineInterface* CommandLineInterface_new(const char* const args0,
                                               const char* const syntaxHeader);

/*!	@brief
                Returns a const Properties* object with all given values for the
   command line options required by _this, including default values for options
   not given
*/
const Properties* CommandLineInterface_args(
    const CommandLineInterface* const _this);

/*!	@brief
                Defines a new mandatory option for _this CommandLineInterface,
   whose value must be a double

        @param option
                The option name
        @param help
                A description of the meaning of _this option, to be shown when
   syntax help is requested by the user
*/
void CommandLineInterface_addDoubleOption(CommandLineInterface* _this,
                                          const char* const option,
                                          const char* const help);

/*!	@brief
                Defines a new non-mandatory option for _this
   CommandLineInterface, whose value must be a double

        @param option
                The option name
        @param default_value
                The default value of the option, if no value is given by the
   user at command line
        @param help
                A description of the meaning of _this option, to be shown when
   syntax help is requested by the user
*/
void CommandLineInterface_addDoubleOption_withDefault(
    CommandLineInterface* _this, const char* const option, double default_value,
    const char* const help);

/*!	@brief
                Defines a new mandatory option for _this CommandLineInterface,
   whose value must be an unsigned int

        @param option
                The option name
        @param help
                A description of the meaning of _this option, to be shown when
   syntax help is requested by the user
*/
void CommandLineInterface_addUIntOption(CommandLineInterface* _this,
                                       const char* const option,
                                       const char* const help);

/*!	@brief
                Defines a new mandatory option for _this CommandLineInterface,
   whose value must be an int

        @param option
                The option name
        @param help
                A description of the meaning of _this option, to be shown when
   syntax help is requested by the user
*/
void CommandLineInterface_addIntOption(CommandLineInterface* _this,
                                       const char* const option,
                                       const char* const help);

/*!	@brief
                Defines a new non-mandatory option for _this
   CommandLineInterface, whose value must be an unsigned int

        @param option
                The option name
        @param default_value
                The default value of the option, if no value is given by the
   user at command line
        @param help
                A description of the meaning of _this option, to be shown when
   syntax help is requested by the user
*/
void CommandLineInterface_addUIntOption_withDefault(CommandLineInterface* _this,
                                                   const char* const option,
                                                   unsigned int default_value,
                                                   const char* const help);

/*!	@brief
                Defines a new non-mandatory option for _this
   CommandLineInterface, whose value must be a int

        @param option
                The option name
        @param default_value
                The default value of the option, if no value is given by the
   user at command line
        @param help
                A description of the meaning of _this option, to be shown when
   syntax help is requested by the user
*/
void CommandLineInterface_addIntOption_withDefault(CommandLineInterface* _this,
                                                   const char* const option,
                                                   int default_value,
                                                   const char* const help);

/*!	@brief
                Defines a new mandatory option for _this CommandLineInterface,
   whose value must be an unsigned long

        @param option
                The option name
        @param help
                A description of the meaning of _this option, to be shown when
   syntax help is requested by the user
*/
void CommandLineInterface_addULongOption(CommandLineInterface* _this,
                                         const char* const option,
                                         const char* const help);

/*!	@brief
                Defines a new mandatory option for _this CommandLineInterface,
   whose value must be a long

        @param option
                The option name
        @param help
                A description of the meaning of _this option, to be shown when
   syntax help is requested by the user
*/
void CommandLineInterface_addLongOption(CommandLineInterface* _this,
                                        const char* const option,
                                        const char* const help);

/*!	@brief
                Defines a new non-mandatory option for _this
   CommandLineInterface, whose value must be an unsigned long

        @param option
                The option name
        @param default_value
                The default value of the option, if no value is given by the
   user at command line
        @param help
                A description of the meaning of _this option, to be shown when
   syntax help is requested by the user
*/
void CommandLineInterface_addULongOption_withDefault(
    CommandLineInterface* _this, const char* const option,
    unsigned long default_value, const char* const help);

/*!	@brief
                Defines a new non-mandatory option for _this
   CommandLineInterface, whose value must be a long

        @param option
                The option name
        @param default_value
                The default value of the option, if no value is given by the
   user at command line
        @param help
                A description of the meaning of _this option, to be shown when
   syntax help is requested by the user
*/
void CommandLineInterface_addLongOption_withDefault(CommandLineInterface* _this,
                                                    const char* const option,
                                                    long default_value,
                                                    const char* const help);

/*!	@brief
                Defines a new mandatory option for _this CommandLineInterface,
   whose value must be a string

        @param option
                The option name
        @param help
                A description of the meaning of _this option, to be shown when
   syntax help is requested by the user
*/
void CommandLineInterface_addStringOption(CommandLineInterface* _this,
                                          const char* const option,
                                          const char* const help);

/*!	@brief
                Defines a new non-mandatory option for _this
   CommandLineInterface, whose value must be a string

        @param option
                The option name
        @param default_value
                The default value of the option, if no value is given by the
   user at command line
        @param help
                A description of the meaning of _this option, to be shown when
   syntax help is requested by the user
*/
void CommandLineInterface_addStringOption_withDefault(
    CommandLineInterface* _this, const char* const option,
    const char* const default_value, const char* const help);

/*!	@brief
                Prints out all the given command line values to file *f
*/
void CommandLineInterface_fprint_args(const CommandLineInterface* const _this,
                                      FILE* f);

/*!	@brief
                Defines a new flag for _this CommandLineInterface

        @param flag
                The flag name
        @param help
                A description of the meaning of _this flag, to be shown when
   syntax help is requested by the user
*/
void CommandLineInterface_addFlag(CommandLineInterface* _this,
                                  const char* const flag,
                                  const char* const help);

/*! @brief
                Processes the command-line arguments given by user.
        @param argc
                The first (int) argument of the main() function
        @param argc
                The second (char**) argument of the main() function
        @return
                0 on success, and an error code (a negative int) otherwise.
*/
int CommandLineInterface_processArgs(CommandLineInterface* _this, int argc,
                                     char** args);

/*! @brief
                Frees the memory allocated for *thisP CommandLineInterface
   instance, and sets *thisP to NULL
*/
void CommandLineInterface_free(CommandLineInterface** thisP);

/*!	@brief
                Gets the value of string option 'option' in _this
   CommandLineInterface

        @details
                The function assumes that a value for 'option' exists (either
   given by the user or default), as, otherwise, previous call to
   CommandLineInterface_processArgs()  would have terminated the program, by
   showing syntax help.
*/
const char* CommandLineInterface_getStringOption(
    const CommandLineInterface* const _this, char* option);

/*!	@brief
                Gets the value of unsigned int option 'option' in _this
   CommandLineInterface

        @details
                The function assumes that a value for 'option' exists (either
   given by the user or default), as, otherwise, previous call to
   CommandLineInterface_processArgs()  would have terminated the program, by
   showing syntax help.
*/
unsigned int CommandLineInterface_getUIntOption(const CommandLineInterface* const _this,
                                      char* option);

/*!	@brief
                Gets the value of int option 'option' in _this
   CommandLineInterface

        @details
                The function assumes that a value for 'option' exists (either
   given by the user or default), as, otherwise, previous call to
   CommandLineInterface_processArgs()  would have terminated the program, by
   showing syntax help.
*/
int CommandLineInterface_getIntOption(const CommandLineInterface* const _this,
                                      char* option);

/*!	@brief
                Gets the value of unsigned long option 'option' in _this
   CommandLineInterface

        @details
                The function assumes that a value for 'option' exists (either
   given by the user or default), as, otherwise, previous call to
   CommandLineInterface_processArgs()  would have terminated the program, by
   showing syntax help.
*/
unsigned long CommandLineInterface_getULongOption(
    const CommandLineInterface* const _this, char* option);

/*!	@brief
                Gets the value of long option 'option' in _this
   CommandLineInterface

        @details
                The function assumes that a value for 'option' exists (either
   given by the user or default), as, otherwise, previous call to
   CommandLineInterface_processArgs()  would have terminated the program, by
   showing syntax help.
*/
long CommandLineInterface_getLongOption(const CommandLineInterface* const _this,
                                        char* option);

/*!	@brief
                Gets the value of double option 'option' in _this
   CommandLineInterface

        @details
                The function assumes that a value for 'option' exists (either
   given by the user or default), as, otherwise, previous call to
   CommandLineInterface_processArgs()  would have terminated the program, by
   showing syntax help.
*/
double CommandLineInterface_getDoubleOption(
    const CommandLineInterface* const _this, char* option);

/*!	@brief
                Gets the boolean value of flag 'flag' in _this
   CommandLineInterface

        @details
                The function assumes that a boolean value for 'flag' exists
   (either given by the user or default), as, otherwise, previous call to
   CommandLineInterface_processArgs()  would have terminated the program, by
   showing syntax help.
        @return
                1 if flag is set, 0 otherwise
*/
int CommandLineInterface_getFlag(const CommandLineInterface* const _this,
                                 char* option);

/*!	@brief
                Returns a read-only string containing the full command line
   executed by the user
*/
const char* CommandLineInterface_getFullCmdline(
    const CommandLineInterface* const _this);

/*!	@brief
                Returns a read-only string containing the working _this of the
   command line tool associated to _this CommandLineInterface
*/
const char* CommandLineInterface_getWorkingDir(
    const CommandLineInterface* const _this);

#endif
