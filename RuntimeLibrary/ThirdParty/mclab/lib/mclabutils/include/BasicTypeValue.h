/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef __BasicTypeValue_h__
#define __BasicTypeValue_h__

/*! @file
                @brief
                        An instance of the BasicTypeValue data structure defines
   a value of one of the types in the enum BasicType.
                @date 2014-11-15
*/
typedef enum {
  BASIC_TYPE_INT,
  BASIC_TYPE_LONG,
  BASIC_TYPE_ULONG,
  BASIC_TYPE_ULONGLONG,
  BASIC_TYPE_DOUBLE,
  BASIC_TYPE_CHAR
} BasicType;

/*! @brief
                An instance of the BasicTypeValue data structure defines a value
   of one of the types in the enum BasicType.

        @details
                Given an instance of BasicTypeValue, it is possible to retrieve
   both type and value. _this data structure is mainly useful in collections
   (where, e.g., values of different C basic types need to be stored).

        @date 2014-11-15
*/
typedef struct {
  BasicType type;
  union {
    int i;
    long l;
    unsigned long ul;
    unsigned long long ull;
    double d;
    char c;
  } value;
} BasicTypeValue;

/*!	@brief
                Returns the size in bytes of a value of type t
*/
size_t BasicType_sizeof(BasicType t);

/*!	@brief
                Prints to *f _this BasicTypeValue
*/
void BasicTypeValue_fprint(const void* const _this, FILE* f);

/*!	@brief
                Checks whether _this BasicTypeValue is equal (same type and same
   value) to 'other'
*/
int BasicTypeValue_equals(const void* const _this, const void* const other);

/*!	@brief
                Sets type and value for _this BasicTypeValue.
        @param type
                The new type for _this
        @param value_p
                A pointer to the value to be assigned to _this. 'value_p' will
   be casted to a pointer of type 'type'
*/
void BasicTypeValue_set(BasicTypeValue* _this, BasicType type, void* value_p);

/*!	@brief
                Sets _this BasicTypeValue as to store int value 'value'
*/
void BasicTypeValue_setInt(BasicTypeValue* _this, int value);

/*!	@brief
                Sets _this BasicTypeValue as to store long value 'value'
*/
void BasicTypeValue_setLong(BasicTypeValue* _this, long value);

/*!	@brief
                Sets _this BasicTypeValue as to store unsigned long value
   'value'
*/
void BasicTypeValue_setULong(BasicTypeValue* _this, unsigned long value);

/*!	@brief
                Sets _this BasicTypeValue as to store unsigned long long value
   'value'
*/
void BasicTypeValue_setULongLong(BasicTypeValue* _this, unsigned long long value);

/*!	@brief
                Sets _this BasicTypeValue as to store double value 'value'
*/
void BasicTypeValue_setDouble(BasicTypeValue* _this, double value);

/*!	@brief
                Sets _this BasicTypeValue as to store char value 'value'
*/
void BasicTypeValue_setChar(BasicTypeValue* _this, char value);

#endif
