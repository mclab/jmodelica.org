/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef __PROPERTIES__
#define __PROPERTIES__

#include <HashTable.h>

/*! @file
    @brief
        An instance of the Properties data structure defines an associative
   array whose elements are pairs of the form string -> Properties_Content.
    @date 2017-02-07
*/

/*! @brief
        An instance of the Properties data structure defines an associative
   array whose elements are pairs of the form string -> Properties_Content.

    @details
        In other words, each entry in a Properties instance has
        a string as key and a Property_Content as value.
*/
typedef struct Properties Properties;

/*! @brief
        An instance of the Properties_Content data structure defines a pair
   (type, value).

    @details
        type is a Properties_Content_Type, while value is an element of that
   type.
*/

typedef struct Properties_Content Properties_Content;

/*! @brief The possible types of values in entries of a Properties instance
 */
typedef enum {
  PROPERTIES_UNKNOWN,
  PROPERTIES_INT,
  PROPERTIES_UINT,
  PROPERTIES_LONG,
  PROPERTIES_ULONG,
  PROPERTIES_DOUBLE,
  PROPERTIES_NUMERIC,
  PROPERTIES_BOOLEAN,
  PROPERTIES_STRING,
  PROPERTIES_PROPERTIES,
  PROPERTIES_POINTER
} Properties_Content_Type;

// PropertyContent methods

/*! @brief
        Gets the type of the Properties_Content _this
*/
Properties_Content_Type Properties_Content_type(
    const Properties_Content* _this);

/*! @brief
        Gets the value of the Properties_Content _this.

    @return
        The returned pointer points to a value of type Properties_Content_type()
*/
const void* Properties_Content_value(const Properties_Content* _this);

/*! @brief
        Gets a string representation of the Property_Content_Type 'type'

    @return
        A constant (literal) string representing type
*/
const char* Properties_Content_Type_toString(Properties_Content_Type type);

/*! @brief
        Prints out _this Properties_Content to file handler *f

    @return
        0 in case of success and an error code (non-null integer) in case of
   error
*/
int Properties_Content_Value_fprint(const void* _this, FILE* file);

/*! @brief
        Creates a new instance of the Properties data structure
*/
Properties* Properties_new();

/*! @brief
        Creates a new instance of the Properties data structure and populates it
   with content in text file fname

    @param fname
        The fullpathname of the file to read

    @details
        - Comments start with #
        - Entries are of the form: type property name = property values
            - type is one of boolean, int, ulong, long, double, numeric, string,
   properties If type is omitted, it is guessed among: properties, boolean,
   string - property values start with '{\n' and end with '}\n'
*/
Properties* Properties_newFromFile(const char* fname);

/*! @brief
        Creates a new instance of the Properties data structure and populates it with content
        in stream f

    @param fname
        The pathname of the stream, it could be null because it used only for debug out.

    @details
        - Comments start with #
        - Entries are of the form: type property name = property values
            - type is one of boolean, int, ulong, long, double, numeric, string, properties
              If type is omitted, it is guessed among: properties, boolean, string
            - property values start with '{\n' and end with '}\n'
*/
Properties *Properties_newFromStream(FILE *f, const char *fname);

/*! @brief
        Frees memory allocated to this Properties instance and sets *_this to NULL.
*/
void Properties_free(Properties** _thisP);

/*! @brief
        Clones _this Properties instance
*/
Properties* Properties_clone(const Properties* _this);

/*! @brief
        (Clones and) copies all entries of _this into dest.
*/
void Properties_copyInto(const Properties* _this, Properties* dest);

/*! @brief
        Empties _this Properties instance, removing all its elements
*/
void Properties_clear(Properties* _this);

/*! @brief
        Removes 'property' from _this Properties instance.
*/
void Properties_remove(Properties* _this, const char* property);

/*! @brief
        Sets a property with a double value.
        After _this function is invoked, _this Properties will contain a mapping
   'property' -> value. Any old value associated to 'property' is deleted.

    @param property
        The name of the property
    @param value
        The (new) value to store
*/
void Properties_setDouble(Properties* _this, const char* property,
                          double value);

/*! @brief
        Sets a property with a double value.
        After _this function is invoked, _this Properties will contain a mapping
   'property' -> value. Any old value associated to 'property' is deleted.

    @param property
        The name of the property
    @param value
        The (new) value to store
*/
void Properties_setNumeric(Properties* _this, const char* property,
                           double value);

/*! @brief
        Sets a property with a int value.
        After _this function is invoked, _this Properties will contain a mapping
   'property' -> value. Any old value associated to 'property' is deleted.

    @param property
        The name of the property
    @param value
        The (new) value to store
*/
void Properties_setInt(Properties* _this, const char* property, int value);

/*! @brief
        Sets a property with a unsigned int value.
        After _this function is invoked, _this Properties will contain a mapping
   'property' -> value. Any old value associated to 'property' is deleted.

    @param property
        The name of the property
    @param value
        The (new) value to store
*/
void Properties_setUInt(Properties* _this, const char* property,
                        unsigned int value);

/*! @brief
        Sets a property with a unsigned long value.
        After _this function is invoked, _this Properties will contain a mapping
   'property' -> value. Any old value associated to 'property' is deleted.

    @param property
        The name of the property
    @param value
        The (new) value to store
*/
void Properties_setULong(Properties* _this, const char* property,
                         unsigned long value);

/*! @brief
        Sets a property with a long value.
        After _this function is invoked, _this Properties will contain a mapping
   'property' -> value. Any old value associated to 'property' is deleted.

    @param property
        The name of the property
    @param value
        The (new) value to store
*/
void Properties_setLong(Properties* _this, const char* property, long value);

/*! @brief
        Sets a property with a boolean value.
        After _this function is invoked, _this Properties will contain a mapping
   'property' -> value. Any old value associated to 'property' is deleted.

    @param property
        The name of the property
    @param value
        The (new) value to store
*/
void Properties_setBoolean(Properties* _this, const char* property, char value);

/*! @brief
        Sets a property with a string value.
        After _this function is invoked, _this Properties will contain a mapping
   'property' -> value. Any old value associated to 'property' is deleted.

    @param property
        The name of the property
    @param value
        The (new) value to store; 'value' can be NULL
*/
void Properties_setString(Properties* _this, const char* property,
                          const char* value);

/*! @brief
        Sets a property with a void pointer.
        After _this function is invoked, _this Properties will contain a mapping
   'property' -> value. Any old value associated to 'property' is deleted.

    @param property
        The name of the property
    @param value
        The (new) value to store; 'value' can be NULL
*/
void Properties_setPointer(Properties* _this, const char* property,
                           const void* value);

/*! @brief
        Sets a property with a (nested) Properties value.
        After _this function is invoked, _this Properties will contain a mapping
   'property' -> value. Any old value associated to 'property' is deleted.

    @param property
        The name of the property
    @param value
        The (new) value to store (a clone of "property")
*/
void Properties_setNestedProperties(Properties* _this, const char* property,
                                    const Properties* value);

/*! @brief
        Sets a property with a value of type 'type'.
        After _this function is invoked, _this Properties will contain a mapping
   'property' -> value. Any old value associated to 'property' is deleted.

    @param property
        The name of the property
    @param value
        The (new) value to store
    @param type
        The type of *value
*/
void Properties_set(Properties* _this, const char* property,
                    Properties_Content_Type type, const void* value);

// Returns 0 in case of success, -1 otherwise
/*! @brief
        Sets a property with the value of type 'type' obtained by parsing string
   value_string. After _this function is invoked, _this Properties will contain
   a mapping 'property' -> value. Any old value associated to 'property' is
   deleted.

    @param property
        The name of the property
    @param value_string
        The string containing the (new) value to store
    @param type
        The type of the value in value_string, which must be different than
   PROPERTIES_POINTER
*/
int Properties_setFromString(Properties* _this, const char* property,
                             Properties_Content_Type type,
                             const char* value_string);

/*! @brief
        Saves this to string str of capacity cap allocated by the caller. Any entry of type PROPERTIES_POINTER will be saved as a (useless) memory address.
*/
void Properties_intoString(Properties *this, char str[], size_t cap);

/*! @brief
        Saves this to string str of capacity cap. Any entry of type PROPERTIES_POINTER will be saved as a (useless) memory address.
*/
size_t Properties_toString(Properties *this, char **str, size_t cap);

/*! @brief
        Creates a new instance of the Properties data structure and populates it with content
        in string str

    @param str
        The string to read

    @param keyMaxLen
        The maximum length of a string usable as key. Key strings longer than this value are truncated.

    @details
        - Comments start with #
        - Entries are of the form: type property name = property values
            - type is one of boolean, int, ulong, long, double, numeric, string, properties
              If type is omitted, it is guessed among: properties, boolean, string
            - property values start with '{\n' and end with '}\n'
*/
Properties *Properties_newFromString(const char *str);

/*! @brief
        Prints out a textual representation of _this Properties to file *f

    @return
        If successful, the total number of characters written is returned,
   otherwise a negative number is returned.
*/
int Properties_fprint(const void* _this, FILE* f);

/*! @brief
        Saves _this to file 'filePath'. Any entry of type PROPERTIES_POINTER
   will be saved as a (useless) memory address.
    @return
        0 on success, and an error code (a non-zero integer) on error
*/
int Properties_save(const Properties* _this, const char* filePath);

// Getter methods: they fill-in 'property' value to *value

/*! @brief
        Fills in *value with the value of the int 'property' stored in _this
   Properties instance.
    @returns
        0 on success, and an error code (a non-zero integer) on error (i.e.,
   'property' does not exist in _this, or it is not of type PROPERTIES_INT)
*/
int Properties_getInt(const Properties* _this, const char* property,
                      int* value);

/*! @brief
        Fills in *value with the value of the unsigned long 'property' stored in
   _this Properties instance.
    @returns
        0 on success, and an error code (a non-zero integer) on error (i.e.,
   'property' does not exist in _this, or it is not of type PROPERTIES_UINT)
*/
int Properties_getUInt(const Properties* _this, const char* property,
                       unsigned int* value);

/*! @brief
        Fills in *value with the value of the unsigned long 'property' stored in
   _this Properties instance.
    @returns
        0 on success, and an error code (a non-zero integer) on error (i.e.,
   'property' does not exist in _this, or it is not of type PROPERTIES_ULONG)
*/
int Properties_getULong(const Properties* _this, const char* property,
                        unsigned long* value);

/*! @brief
        Fills in *value with the value of the long 'property' stored in _this
   Properties instance.
    @returns
        0 on success, and an error code (a non-zero integer) on error (i.e.,
   'property' does not exist in _this, or it is not of type PROPERTIES_LONG)
*/
int Properties_getLong(const Properties* _this, const char* property,
                       long* value);

/*! @brief
        Fills in *value with the value of the boolean 'property' stored in _this
   Properties instance.
    @returns
        0 on success, and an error code (a non-zero integer) on error (i.e.,
   'property' does not exist in _this, or it is not of type PROPERTIES_BOOLEAN)
*/
int Properties_getBoolean(const Properties* _this, const char* property,
                          char* value);

/*! @brief
        Fills in *value with the value of the double 'property' stored in _this
   Properties instance.
    @returns
        0 on success, and an error code (a non-zero integer) on error (i.e.,
   'property' does not exist in _this, or it is not of type PROPERTIES_DOUBLE)
*/
int Properties_getDouble(const Properties* _this, const char* property,
                         double* value);

/*! @brief
        Fills in *value with the value of the numeric 'property' stored in _this
   Properties instance.
    @returns
        0 on success, and an error code (a non-zero integer) on error (i.e.,
   'property' does not exist in _this, or it is not of type PROPERTIES_NUMERIC)
*/
int Properties_getNumeric(const Properties* _this, const char* property,
                          double* value);

/*! @brief
        Fills in *value with the value of the string 'property' stored in _this
   Properties instance.
    @returns
        0 on success, and an error code (a non-zero integer) on error (i.e.,
   'property' does not exist in _this, or it is not of type PROPERTIES_STRING)
*/
int Properties_getString(const Properties* _this, const char* property,
                         const char** value);

/*! @brief
        Fills in *value with the value of the Properties 'property' stored in
   _this Properties instance.
    @returns
        0 on success, and an error code (a non-zero integer) on error (i.e.,
   'property' does not exist in _this, or it is not of type PROPERTIES_POINTER)
*/
int Properties_getPointer(const Properties* _this, const char* property,
                          const void** value);

/*! @brief
        Fills in *value with the value of the Properties 'property' stored in
   _this Properties instance.
    @returns
        0 on success, and an error code (a non-zero integer) on error (i.e.,
   'property' does not exist in _this, or it is not of type
   PROPERTIES_PROPERTIES
*/
int Properties_getNestedProperties(const Properties* _this,
                                   const char* property, Properties** value);

/*! @brief
        Returns the Properties_Content instance associated to 'property' in
   _this Properties instance.
    @returns
        A pointer to Properties_Content instance, or NULL if 'property' does not
   occur in _this
*/
const Properties_Content* Properties_get(const Properties* _this,
                                         const char* property);

/*! @brief
        Checkes whether 'property' occurs in _this Properties instance

    @return
        1 if 'property' occurs in _this Properties instance, 0 otherwise
*/
int Properties_contains(const Properties* _this, const char* property);

/*! @brief
        Fills into *type the type of 'property' in _this Properties instance.
    @return
        0 on success ('property' occurs in _this), and an error code (a non-zero
   integer) otherwise
*/
int Properties_type(const Properties* _this, const char* property,
                    Properties_Content_Type* type);

/*! @brief
        Returns the number of properties in _this
*/
unsigned long Properties_size(const Properties* _this);

size_t Properties_getSerializedSize(Properties const *_this);
size_t Properties_serialize(Properties const *_this, void **bufPtr);
Properties *Properties_newFromSerialized(void const *buf);

typedef HashTable_Entry Properties_Entry;
typedef struct Properties_Iterator Properties_Iterator;

/*! @brief Creates an Iterator for _this Properties instance
 */
Properties_Iterator* Properties_Iterator_new(const Properties* _this);

/*! @brief
        Rewinds _this iterator
    @details
        After having called _this function, the first invocation of
   Properties_Iterator_next on _this Properties_Iterator will return the first
   Properties_Entry.
*/
void Properties_Iterator_rewind(Properties_Iterator* _this);

/*! @brief
        Checks whether _this iterator has more elements to return

    @return
        1 if a call to Properties_Iterator_next(_this) will return a non-NULL
   entry, 0 otherwise
*/
int Properties_Iterator_hasNext(const Properties_Iterator* _this);

/*! @brief Returns the next entry of the Properties instance which _this
   iterator is iterating on.

    @return
        a Properties_Entry instance, or NULL if no more entries exist
*/
const Properties_Entry* Properties_Iterator_next(Properties_Iterator* _this);

/*! @brief
        Returns the property name of _this Properties_Entry
*/
const char* Properties_Entry_property(const Properties_Entry* _this);

/*! @brief
        Returns a pointer to the Properties_Content instance associated to _this
   Properties_Entry
*/
const Properties_Content* Properties_Entry_value(const Properties_Entry* _this);

/*! @brief
        Frees memory used by _this iterator. Also sets *thisP to NULL.
*/
void Properties_Iterator_free(Properties_Iterator** thisP);
#endif
