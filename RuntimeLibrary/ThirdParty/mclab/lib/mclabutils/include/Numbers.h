/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef __NUMBERS_H__
#define __NUMBERS_H__

#include <math.h>

/*!	@file
        @brief
                The Numbers package provides functions to compare double values.
        @details
                Comparison between double values can be problematic, given the
   limited precision of their representation.

                _this package offers functions to compare two doubles, using
   both an absolute and a relative tolerance.

                In particular, given two doubles a and b, we have a ~ b (i.e., a
   is approximately equal to b) iff

                        - a == b or

                        - |a-b| <= absoluteTolerance or

                        - |a-b| <= relativeTolerance * max(|a|, |b|)

                The package provides 1E-6 as default values for both the
   absolute and relative tolerances.

        @date 2014-11-15
*/

/*! @brief
                Changes the value for the absolute tolerance
*/
void Numbers_setAbsoluteTolerance(double t);
/*! @brief
                Gets the value for the absolute tolerance
*/
double Numbers_getAbsoluteTolerance();

/*! @brief
                Changes the value for the relative tolerance
*/
void Numbers_setRelativeTolerance(double t);
/*! @brief
                Gets the value for the relative tolerance
*/
double Numbers_getRelativeTolerance();

/*! @brief
                Checks whether double d1 is approximately equal to double d2
        @return
                1 if yes, 0 if no
*/
int Numbers_approxEQ(double d1, double d2);

/*! @brief
                Checks whether double d1 is less than or approximately equal to
   double d2
        @return
                1 if yes, 0 if no
*/
int Numbers_approxLE(double d1, double d2);

/*! @brief
                Checks whether double d1 is less than, but not approximately
   equal to, double d2
        @return
                1 if yes, 0 if no
*/
int Numbers_approxL(double d1, double d2);

/*! @brief
                Checks whether double d1 is greater than or approximately equal
   to double d2
        @return
                1 if yes, 0 if no
*/
int Numbers_approxGE(double d1, double d2);

/*! @brief
                Checks whether double d1 is greater than than, but not
   approximately equal to, double d2
        @return
                1 if yes, 0 if no
*/
int Numbers_approxG(double d1, double d2);

/*! @brief
                Computes the relative error of d2 wrt. d1, i.e., (d1-d2)/Z
                where Z is relativeTolerance if d1 == 0 and d1 otherwise
*/
double Numbers_relativeError(double d1, double d2);

/* @brief
        Computes the absolute value of int v
*/
int Numbers_abs_int(int v);

/* @brief
        Computes the absolute value of long v
*/
long Numbers_abs_long(long v);

/* @brief
        Computes the absolute value of double v
*/
double Numbers_abs_double(double v);
#endif
