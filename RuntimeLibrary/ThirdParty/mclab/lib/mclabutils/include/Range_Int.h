/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef __RANGE_INT_H__
#define __RANGE_INT_H__

#include <stdio.h>

/*! @file
                @brief
                        An instance of the Range_Int data structure defines a
   finite set of integers.
                @date 2014-11-14
*/

/*! @brief
                An instance of the Range_Int data structure defines a finite set
   of integers.

                @details
                A Range_Int is specified by three values:

                - minimum value 'min'

                - maximum value 'max'

                - step value 'step'

                A Range_Int has two semantics: the 'basic' semantics and the
   'advanced' (or 'looping') semantics

                In its basic form, a Range_Int defines the following finite set
   of integers:
                @verbatim { x  | min <= x <= max & (x-min) % step == 0 }
   @endverbatim For example, the following Range_Int:  (min=0, max=10, step=3)
   defines, in its basic form, the following set of integers:
                @verbatim {0, 3, 6, 9} @endverbatim
                Using  Range_Int_Iterator_new()  it is possible to iterate over
   these values forward and backward.

                In its advanced form, a Range_Int may be set to define the
   following _sequence_ Seq of integers
                @verbatim
    - Seq_0 = min
    - for all i > 0
      Seq_i = if (Seq_{i-1} + step <= max)
            then Seq_{i-1} + step
            else min + Seq_{i-1} + step - max - 1
                @endverbatim

                For example, the following Range_Int:  (min=0, max=10, step=3)
   defines, in its advanced form, the following set of integers:
                @verbatim {0, 3, 6, 9, 0+12-10-1=1, 4, 7, 10, 0+13-10-1=2, 5, 8,
   ...} @endverbatim Using  Range_Int_Iterator_new_looping()  it is possible to
   iterate over these values forward and backward.
*/
typedef struct Range_Int Range_Int;

/*! @brief
  An instance of the Range_Int_Iterator data structure defines an iterator over
  a Range_Int.
*/
typedef struct Range_Int_Iterator Range_Int_Iterator;

/*!	@brief
                Creates a new Range_Int
*/
Range_Int* Range_Int_new(int min, int max, int step);

/*!	@brief
                Creates a new Range_Int from data in 'string'.

                'string' has the format:
                        @verbatim[<min>{{, <max>}]{/<step>}}@endverbatim
                        or
                        min

                where: "[" and "]" are terminal characters, and {...} denote an
   optional part. If max is omitted, then it is assumed to be equal to min. If
   step is omitted, then it is assumed to be 1.
*/
Range_Int* Range_Int_new_fromString(const char* string);

/*!	@brief
                Gets the minimum value of _this Range_Int
*/
int Range_Int_min(const Range_Int* const _this);

/*!	@brief
                Gets the upper bound of _this Range_Int
*/
int Range_Int_max(const Range_Int* const _this);

/*! @brief
                Returns the max value of _this Range_Int, i.e.,
                g such that exists k integer > 0  s.t.  g = min + step*k and g
   <= max
*/
int Range_Int_greatest(const Range_Int* _this);

/*!	@brief
                Gets the 'step' value of _this Range_Int
*/
int Range_Int_step(const Range_Int* const _this);

void Range_Int_fprint(const Range_Int* _this, FILE* file);

void Range_Int_toString(const Range_Int* _this, char* result, size_t maxlen);

/*!	@brief
                Gets the number of elements in _this Range_Int (under basic
   semantics)
*/
int Range_Int_size(const Range_Int* const _this);

/*!	@brief
                Frees memory used by *thisP and sets *thisP to NULL
*/
void Range_Int_free(Range_Int** thisP);

/*! @brief
                Checks whether _this and other are equal
*/
int Range_Int_equals(const Range_Int* _this, const Range_Int* other);

/*! @brief
                Writes in *indexP the index of 'value', if 'value' belongs to
   _this Range_Int (under basic semantics)
        @return
                0 on success, -1 on failure
*/
int Range_Int_index(const Range_Int* const _this, int value, int* indexP);

/*! @brief
                Writes in *valueP the value of 'index' if 'index' is a valid
   index of _this Range_Int (under basic semantics)
        @return
                0 on success, -1 on failure
*/
int Range_Int_value(const Range_Int* const _this, int index, int* valueP);

/*! @brief
                Checks whether 'value' belongs to _this Range_Int (under basic
   semantics)
        @return
                1 if 'value' belongs to _this, 0 if it does not
*/
int Range_Int_contains(const Range_Int* const _this, int value);

/*! @brief
                Creates a new iterator for _this Range_Int (under basic
   semantics).
        @details
                The created iterator will scan all values in _this Range_Int,
   under basic semantics, forward and backward.
*/
Range_Int_Iterator* Range_Int_Iterator_new(const Range_Int* const _this);

/*! @brief
                Creates a new looping iterator for _this Range_Int (under
   advanced semantics).
        @details
                The created iterator will scan all values in _this Range_Int
   under advanced semantics, forward and backward
*/
Range_Int_Iterator* Range_Int_Iterator_new_looping(
    const Range_Int* const _this);

/*! @brief
                Changes the 'step' value of the Range_Int associated to _this
   iterator. The new step value is visible only to _this iterator.
*/
void Range_Int_Iterator_setStep(Range_Int_Iterator* const _this, int step);

/*! @brief
                Checks whether a call to function  Range_Int_Iterator_next()  on
   _this iterator will produce a value. _this function returns always 1 if _this
   is a looping iterator.
        @return
                1 if true, 0 if false
*/
int Range_Int_Iterator_hasNext(Range_Int_Iterator* const _this);
/*! @brief
                Checks whether a call to function  Range_Int_Iterator_prev()  on
   _this iterator will produce a value. _this function returns always 1 if _this
   is a looping iterator.
        @return
                1 if true, 0 if false
*/
int Range_Int_Iterator_hasPrev(Range_Int_Iterator* const _this);

/*! @brief
                Writes in *result the next value of _this iterator.
        @return
                1 if the next value exists (i.e., a call to
   Range_Int_Iterator_hasNext() would return 1), 0 otherwise
*/
int Range_Int_Iterator_next(Range_Int_Iterator* const _this, int* result);

/*! @brief
                Writes in *result the previous value of _this iterator.
        @return
                1 if the previous value exists (i.e., a call to
   Range_Int_Iterator_hasPrev() would return 1), 0 otherwise
*/
int Range_Int_Iterator_prev(Range_Int_Iterator* const _this, int* result);

/*! @brief
                Rewinds _this iterator, as it were just created
*/
void Range_Int_Iterator_rewind(Range_Int_Iterator* const _this);

/*! @brief
                Frees memory allocated to *thisP iterator and sets *thisP to
   NULL
*/
void Range_Int_Iterator_free(Range_Int_Iterator** thisP);

/*! @brief
                Prints out to *f the current state of _this iterator (mainly to
   assist in debugging)
*/
void Range_Int_Iterator_fprint(const void* const _this, FILE* f);
#endif
