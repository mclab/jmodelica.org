/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef __CSV__
#define __CSV__

#include <Array.h>

typedef struct CSV CSV;

CSV* CSV_new(const char* fileName, int hasHeader, char separator,
             unsigned int rowMaxLen);
void CSV_free(CSV** thisP);

int CSV_hasNextRow(CSV* _this);

Array* CSV_nextRow(CSV* _this);
Array* CSV_header(CSV* _this);

#endif
