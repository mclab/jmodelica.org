# MCLabUtils #

MCLabUtils is a general-purpose C library.

The package is versioned using the [semantic versioning policy](http://semver.org).

## Building prerequisites ##

MCLabUtils Library depends on the following:

* [GSL](https://www.gnu.org/software/gsl/)
* [CMake](https://cmake.org)
* [MCLab CMake modules](https://bitbucket.org/mclab/cmake)

## For users ##

Make sure that your CMake module path contains path to [MCLab CMake modules](https://bitbucket.org/mclab/cmake).

Use the following CMake macro to link your library/tool to MCLabUtils:

```
mclab_link_library(MCLabUtils)
```

For an example of a tool that links to MCLabUtils see [MCLab tool template](https://bitbucket.org/mclab/tool-template).

## For developers ##

Use [MCLab libraries devenv](https://bitbucket.org/mclab/devenv) when working on the library.

