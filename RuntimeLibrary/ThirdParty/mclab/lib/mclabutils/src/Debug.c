#include <stdio.h>
/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>

#include <Debug.h>
#include <Properties.h>

#define MAX_STRING_LEN 1024

static char indent_char = ' ';
static unsigned max_indent = 100;
static unsigned indent_amount = 3;
static char indent_string[MAX_STRING_LEN+1] = { '\0' };
static unsigned indent_string_len = 0;

const char* Debug_indent_string() {
	return indent_string;
}

void Debug_setIndentAmount(unsigned n) {
	indent_amount = n;
}
void Debug_setMaxIndent(unsigned n) {
	if (n > MAX_STRING_LEN) return;
	max_indent = n;
}
void Debug_setIndentChar(char c) {
	indent_char = c;
}


// Increase indent
void Debug_outNest_real_function() {
	if (indent_string_len >= max_indent) return;

	for (unsigned i = 0; i < indent_amount && indent_string_len < max_indent; i++) {
		indent_string[indent_string_len] = indent_char;
		indent_string_len++;
	}
	indent_string[indent_string_len+1] = '\0';
}

// Increase indent
void Debug_outUnnest_real_function() {
	if (indent_string_len < indent_amount) return;
	indent_string_len -= indent_amount;
	indent_string[indent_string_len] = '\0';
}

static Properties* categoriesOut = NULL;
static Properties* categoriesAssertions = NULL;

static int defaultValueOut = 1; // categories not occurring in 'categoriesOut'
static int defaultValueAssertions = 1; // categories not occurring in 'categoriesAssertions'


void Debug_setDefaultOut(int d) {
	defaultValueOut = d;
	Debug_out(DEBUG_ALWAYS, "Debug: debug output for all categories %s by default\n",
	 	(d ? "enabled" : "disabled"));
}
void Debug_setDefaultAssertions(int d) {
	defaultValueAssertions = d;
	Debug_out(DEBUG_ALWAYS, "Debug: assertion checking for all categories %s by default\n",
	 	(d ? "enabled" : "disabled"));
}

#define myfprintf(f, ...)((void) 0) /* \
	do { \
		fprintf(f, __VA_ARGS__); \
	} while(0)*/

int Debug_isEnabled(const char* id, int WHAT) {
	int result = 0;
	myfprintf(stderr, "Debug_isEnabled('%s', %d) start\n", id, WHAT);
	//myfprintf(stderr, " - checking whether '%s' equals '%s': ", id, DEBUG_ALWAYS);
	if (id == NULL) {
		fprintf(stderr, "Debug_isEnabled('%s', %d) - ERROR\n", id, WHAT);
	}
	if (strcmp(id, DEBUG_ALWAYS) == 0) {
		//myfprintf(stderr, "yes --> returning 1\n");
		result = 1;
	} else {
		//myfprintf(stderr, "no\n");
		char value = 0;
		Properties* p = (WHAT == _OUT) ? categoriesOut : categoriesAssertions;
		int error = 0;
		if (p == NULL) {
			error = 1;
			//myfprintf(stderr, " - Properties == NULL --> error = %d\n", error);
		} else {
			//myfprintf(stderr, " - getting value for '%s' from Properties %p...", id, (void*)p);
			error = Properties_getBoolean(p, id, &value);
			//myfprintf(stderr, " error = %d\n", error);
		}
		if (error) {
			//myfprintf(stderr, " - error! --> returning defaultValue\n");
			result = (WHAT == _OUT) ? defaultValueOut : defaultValueAssertions;
		} else {
			//myfprintf(stderr, " - returning %d\n", value);
			result = (int) value;
		}
	}
	myfprintf(stderr, "Debug_isEnabled('%s', %d) returns %d\n", id, WHAT, result);
	return result;
}

void Debug_enableOut(const char* id) {
	if (strcmp(id, DEBUG_ALWAYS) == 0) {
		fprintf(stderr, "You cannot change the behaviour of the special Debug category '%s'\n",
		 	DEBUG_ALWAYS);
		assert(0);
	}
	if (categoriesOut == NULL) {
		categoriesOut =
			Properties_new(CATEGORY_MAX_LENGTH);
		//fprintf(stderr, "Created categoriesOut = %p\n", (void*)categoriesOut);
	}
	//fprintf(stderr, "categoriesOut = %p\n", (void*)categoriesOut);
	assert(categoriesOut != NULL);
	Properties_setBoolean(categoriesOut, id, 1);
	Debug_out(DEBUG_ALWAYS, "Debug: debug output for category '%s' enabled\n", id);
}
void Debug_enableAssertions(const char* id) {
	Debug_assert(DEBUG_ALWAYS, strcmp(id, DEBUG_ALWAYS) != 0, "You cannot change the behaviour of the special Debug category '%s'\n", DEBUG_ALWAYS);
	//fprintf(stderr, "0 - cat:%p, id:'%s':%p\n", (void*)categoriesAssertions, id, (void*)id);
	if (categoriesAssertions == NULL) categoriesAssertions = Properties_new(CATEGORY_MAX_LENGTH);
	Debug_assert(DEBUG_ALWAYS, categoriesAssertions != NULL, "Could not allocate categoriesAssertions\n");
	//fprintf(stderr, "1 - %p\n", (void*)categoriesAssertions);
	Properties_setBoolean(categoriesAssertions, id, 1);
	//fprintf(stderr, "2 - %p\n", (void*)categoriesAssertions);
	Debug_out(DEBUG_ALWAYS, "Debug: assertion checking for category '%s' enabled\n", id);
}

void Debug_disableOut(const char* id) {
	Debug_assert(DEBUG_ALWAYS, strcmp(id, DEBUG_ALWAYS) != 0, "You cannot change the behaviour of the special Debug category '%s'\n", DEBUG_ALWAYS);
	if (categoriesOut == NULL) categoriesOut = Properties_new(CATEGORY_MAX_LENGTH);
	Debug_assert(DEBUG_ALWAYS, categoriesOut != NULL, "Could not allocate categoriesOut\n");
	Properties_setBoolean(categoriesOut, id, 0);
	Debug_out(DEBUG_ALWAYS, "Debug: debug output for category '%s' disabled\n", id);
}
void Debug_disableAssertions(const char* id) {
	Debug_assert(DEBUG_ALWAYS, strcmp(id, DEBUG_ALWAYS) != 0, "You cannot change the behaviour of the special Debug category '%s'\n", DEBUG_ALWAYS);
	if (categoriesAssertions == NULL) categoriesAssertions = Properties_new(CATEGORY_MAX_LENGTH);
	Debug_assert(DEBUG_ALWAYS, categoriesAssertions != NULL, "Could not allocate categoriesAssertions\n");
	Properties_setBoolean(categoriesAssertions, id, 0);
	Debug_out(DEBUG_ALWAYS, "Debug: assertion checking for category '%s' disabled\n", id);
}

void Debug_enable(const char* id) {
	Debug_enableOut(id);
	Debug_enableAssertions(id);
}
void Debug_disable(const char* id) {
	Debug_disableOut(id);
	Debug_disableAssertions(id);
}



void Debug_freeAll() {
	if (categoriesOut != NULL) {
		Properties_free(&categoriesOut);
	}
	if (categoriesAssertions != NULL) {
		Properties_free(&categoriesAssertions);
	}
}


unsigned long long Debug_timestamp_millisec() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return ((unsigned long long)tv.tv_sec) * 1000 +
		(((unsigned long long)tv.tv_usec) / 1000);
}
