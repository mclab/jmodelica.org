/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <sys/stat.h>
#include <string.h>
#include <stdarg.h>

#include <Debug.h>
#include <StringUtils.h>

#define DEBUG_FU "FileUtils"

#include <FileUtils.h>

#define STRING_MAX_LEN 1025


void FileUtils_lineByline(const char *fileName, Array* lines) {

	Debug_assert(DEBUG_ALL, fileName != NULL, "fileName == NULL");

  int lnumber = 0;
	FILE *f = fopen(fileName, "r");
	char buffer[STRING_MAX_LEN];
	Debug_out(DEBUG_FU, "Starting reading file '%s'\n", fileName);
	while (fgets(buffer, STRING_MAX_LEN, f) != NULL) {
		Debug_out(DEBUG_FU, "Line %d (%ld chars): '%s'\n", lnumber, strlen(buffer), buffer);
		StringUtils_trim(buffer);
		char *line = StringUtils_clone(buffer);
		Array_add(lines, &line);
    lnumber++;
	}
  fclose(f);
}

void FileUtils_listDir(const char * dirName, char *fileExtension, int depth, Array *fileNames) {

    DIR * d;
    d = opendir(dirName);
    struct dirent *entry = {0};

	Debug_assert(DEBUG_ALL, d != NULL, "Cannot open directory '%s'\n", dirName);

	char fullFileName[STRING_MAX_LEN] = {0};

    while ( (entry = readdir(d)) != NULL ) {

        Debug_out(DEBUG_FU, "Processing file '%s'\n", entry->d_name);

		int lastDotIdx = StringUtils_strpos(entry->d_name, '.');
		if (lastDotIdx == 0) {
			Debug_out(DEBUG_FU, " --> starting with '.': skipped\n");
			continue;
		}

		//skip file with a different extension
		if (!(entry->d_type & DT_DIR) && (!StringUtils_endsWith(entry->d_name, fileExtension)) ) {
			Debug_out(DEBUG_FU, " --> ends with different extension from '%s': skipped\n", fileExtension);
			continue;
		}


		if (!(entry->d_type & DT_DIR)) {

			sprintf(fullFileName, "%s/%s", dirName, entry->d_name);
			char *fileName = StringUtils_clone(fullFileName);
			Array_add(fileNames, &fileName);
		}

        if (entry->d_type & DT_DIR) {

            if (strcmp (entry->d_name, "..") != 0 &&
                strcmp (entry->d_name, ".") != 0) {

                int path_length = sprintf(fullFileName, "%s/%s", dirName, entry->d_name);

                Debug_assert(DEBUG_ALL, path_length < STRING_MAX_LEN, "Path length has got too long.\n");

              	if (depth > 0) {
                	FileUtils_listDir(fullFileName, fileExtension, depth - 1, fileNames);
               	}
            }
		}
    }
    int err = closedir(d);
    Debug_assert(DEBUG_ALL, err == 0, "Could not close '%s'\n", dirName);

    return;
}


int FileUtils_copyFile(const char *from, const char *to) {
    int fd_to, fd_from;
    char buf[4096];
    ssize_t nread;
    int saved_errno;

    fd_from = open(from, O_RDONLY);
    if (fd_from < 0) {
		Debug_out(DEBUG_FU, "fd_from == NULL\n");
		return -1;
	}

    fd_to = creat(to, 0666);
    if (fd_to < 0) goto out_error;

    while (nread = read(fd_from, buf, sizeof buf), nread > 0) {
        char *out_ptr = buf;
        ssize_t nwritten;

        do {
            nwritten = write(fd_to, out_ptr, nread);

            if (nwritten >= 0) {
                nread -= nwritten;
                out_ptr += nwritten;
            }
            else if (errno != EINTR) {
                goto out_error;
            }
        } while (nread > 0);
    }

    if (nread == 0) {
        if (close(fd_to) < 0) {
            fd_to = -1;
            goto out_error;
        }
        close(fd_from);

        /* Success! */
        return 0;
    }

  out_error:
    saved_errno = errno;

    close(fd_from);
    if (fd_to >= 0)
        close(fd_to);

    errno = saved_errno;
	Debug_out(DEBUG_FU, "errno = %d\n", errno);
    return -1;
}

int FileUtils_isFile(const char* const path) {
	struct stat s;
	int err = stat(path, &s);
	if (err) {
		return 0;
	}
	else {
		return ( S_ISREG(s.st_mode) );
	}
}
int FileUtils_isDir(const char* const path) {
	struct stat s;
	int err = stat(path, &s);
	if (err) {
		return 0;
	}
	else {
		return ( S_ISDIR(s.st_mode) );
	}
}

const char* FileUtils_dirName(const char* const path, char* const out_dir) {
	int pos_slash = StringUtils_lastIndexOf(path, '/');
	if (pos_slash < 0) {
		// there are no slashes
		strcpy(out_dir, "./");
	} else {
		if (path[pos_slash+1] != '\0') {
			// path does not terminate with a slash
			strncpy(out_dir, path, pos_slash+1);
			out_dir[pos_slash+1] = '\0';
		} else {
			// path terminates with /, hence points to a directory
			int pos_prev_slash = StringUtils_lastIndexOf_atMost(path, '/', pos_slash-1);
			if (pos_prev_slash < 0) {
				// no previous slashes, dirname is './'
				strcpy(out_dir, "./");
			} else {
				strncpy(out_dir, path, pos_prev_slash+1);
				out_dir[pos_prev_slash+1] = '\0';
			}
		}
	}
	return out_dir;
}
const char* FileUtils_baseName(const char* const path, char* const out_fname) {
	int pos_slash = StringUtils_lastIndexOf(path, '/');
	if (pos_slash < 0) {
		strcpy(out_fname, path);
	} else {
		if (path[pos_slash+1] != '\0') {
			strcpy(out_fname, &(path[pos_slash+1]));
		} else {
			// path terminates with '/', hence is a directory
			// find previous slash
			int pos_prev_slash = StringUtils_lastIndexOf_atMost(path, '/', pos_slash-1);
			if (pos_prev_slash < 0) {
				pos_prev_slash = -1;
			}
			strncpy(out_fname, &(path[pos_prev_slash+1]), pos_slash - (pos_prev_slash+1) + 1);
			out_fname[pos_slash - (pos_prev_slash+1)+1] = '\0';
		}
	}
	return out_fname;
}

