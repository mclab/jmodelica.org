/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <stdbool.h>

#include <Debug.h>
#include <HashTable.h>
#include <StringUtils.h>

#include <Properties.h>

#define DEBUG_OUT_ENABLED
#ifndef NDEBUG
	#define __Debug_assert(cond, ...) do { \
		if (! (cond) ) { \
			fprintf(stderr, "[timestamp=%llu] %s:%d: ", \
				TIMESTAMP, __FILE__, __LINE__); \
			fprintf(stderr, __VA_ARGS__); \
		} \
		assert(cond); \
	} while(0)

	#ifdef DEBUG_OUT_ENABLED
		#define __Debug_out(...) do { \
			fprintf(stderr, __VA_ARGS__); \
		} while(0)

		#define __Debug_perform(f) do { \
			f ; \
		} while(0)
	#else
		#define __Debug_out(...)((void) 0)
		#define __Debug_perform(f)((void) 0)
	#endif
#else
	#define __Debug_assert(cond, ...)((void) 0)
	#define __Debug_out(...)((void) 0)
	#define __Debug_perform(f)((void) 0)
#endif



#define LINE_MAX_LEN 1024*1024
#define INDENT_STRING ("  ")

/*
 * private methods declaration
 */

static void _Properties_free(Properties **thisP);
static void Properties_freeProtected(Properties **thisP);
static Properties *_Properties_new();
static Properties *Properties_newProtected();
static Properties_Content const *
getAux(Properties const *this, char const *property, size_t propertyLen);
static void setAux(Properties *_this, const char *property, size_t propertyLen,
                   Properties_Content *content);

int Properties_fprint_withIndent(const Properties* this, FILE* f, int level);

// Properties_Content_Type methods
Properties_Content_Type _Properties_Content_Type_parse(const char* t) {
	Properties_Content_Type result = PROPERTIES_UNKNOWN;
	if (StringUtils_equalsIgnoreCase(t, "string")) {
		result = PROPERTIES_STRING;
	}
	if (StringUtils_equalsIgnoreCase(t, "int")) {
		result = PROPERTIES_INT;
	}
	else if (StringUtils_equalsIgnoreCase(t, "uint")) {
		result = PROPERTIES_UINT;
	}
	else if (StringUtils_equalsIgnoreCase(t, "ulong")) {
		result = PROPERTIES_ULONG;
	}
	else if (StringUtils_equalsIgnoreCase(t, "long")) {
		result = PROPERTIES_LONG;
	}
	else if (StringUtils_equalsIgnoreCase(t, "double")) {
		result = PROPERTIES_DOUBLE;
	}
	else if (StringUtils_equalsIgnoreCase(t, "numeric")) {
		result = PROPERTIES_NUMERIC;
	}
	else if (StringUtils_equalsIgnoreCase(t, "boolean") ||
		StringUtils_equalsIgnoreCase(t, "bool")) {
			result = PROPERTIES_BOOLEAN;
	}
	else if (StringUtils_equalsIgnoreCase(t, "pointer")) {
			result = PROPERTIES_POINTER;
	}
	else if (StringUtils_equalsIgnoreCase(t, "properties")) {
			result = PROPERTIES_PROPERTIES;
	}
	return result;
}

Properties_Content_Type _Properties_Content_Type_infer(const char* value_string) {
	// - Properties, if value == '{'
	// - Boolean if true|false
	// - String, otherwise
	char value_char = 0;
	double value_double = 0;
	int err = 0;
	if (StringUtils_equals(value_string, "{")) {
		return PROPERTIES_PROPERTIES;
	}
	if (!StringUtils_toBoolean(value_string, &value_char)) return PROPERTIES_BOOLEAN;
	return PROPERTIES_STRING;
}

const char* Properties_Content_Type_toString(Properties_Content_Type type) {
	switch (type) {
		case PROPERTIES_INT : return "int";
		case PROPERTIES_UINT: return "uint";
		case PROPERTIES_ULONG: return "ulong";
		case PROPERTIES_LONG: return "long";
		case PROPERTIES_BOOLEAN : return "boolean";
		case PROPERTIES_DOUBLE : return "double";
		case PROPERTIES_NUMERIC : return "numeric";
		case PROPERTIES_STRING : return "string";
		case PROPERTIES_POINTER : return "pointer";
		case PROPERTIES_PROPERTIES : return "properties";
		default:
			__Debug_assert(0, "Type unknown = %d\n", type);
			return NULL;
	}
}


// Properties struct definition

struct Properties {
	HashTable* hTable;	// char[keyMaxLen] --> Properties_Content*
	bool protected;
};


typedef union {
	double d;
	int i;
	char b;
	unsigned int ui;
	long l;
	unsigned long ul;
	char* s;
	Properties* p;
	const void* v;
} Properties_Content_Value;

// Properties_Content struct definition and methods
struct Properties_Content {
	Properties_Content_Type type;
	Properties_Content_Value value;
};

void _Properties_Content_free(Properties_Content** thisP) {
	Properties_Content* this = *thisP;
	if (this->type == PROPERTIES_STRING) {
		free((char*)this->value.s);
	}
	else if (this->type == PROPERTIES_PROPERTIES) {
		Properties* nested = (Properties*)this->value.p;
		if (nested->protected) Properties_freeProtected(&nested);
		else Properties_free(&nested);
	}
	free(this);
	*thisP = NULL;
}


int _fprint_INDENT_STRING(FILE* f, int level) {
	int result = 0;
	for (int i=0; i<level; i++) {
		result += fprintf(f, "%s", INDENT_STRING);
	}
	return result;
}



/** Builds a new Properties_Content instance having type t and a *copy* of *v as value */
Properties_Content* _Properties_Content_new(Properties_Content_Type t, const void* v) {
	Properties_Content* new = calloc(1, sizeof(Properties_Content));
	__Debug_assert(new != NULL, "new == NULL\n");
	new->type = t;
	switch (t) {
		case PROPERTIES_INT:
			new->value.i = *(int*)v;
			break;
		case PROPERTIES_BOOLEAN:
			new->value.b = *(char*)v;
			//fprintf(stderr, "new->value.i = %d\n", new->value.i);
			break;
		case PROPERTIES_UINT:
			new->value.ui = *(unsigned int*)v;
			break;
		case PROPERTIES_ULONG:
			new->value.ul = *(unsigned long*)v;
			break;
		case PROPERTIES_LONG:
			new->value.l = *(long*)v;
			break;
		case PROPERTIES_DOUBLE:
			case PROPERTIES_NUMERIC:
			new->value.d = *(double*)v;
			break;
		case PROPERTIES_STRING:
			new->value.s = StringUtils_clone((char*)v);
			break;
		case PROPERTIES_PROPERTIES:
			new->value.p = Properties_clone((Properties*)v);
			new->value.p->protected = true;
			break;
		case PROPERTIES_POINTER:
			new->value.v = v;
			break;
		default:
			__Debug_assert(0, "Type unknown = %d\n", t);
			_Properties_Content_free(&new);
			return NULL;
	}
	return new;
}

Properties_Content* _Properties_Content_clone(Properties_Content* this) {
	Properties_Content* new = calloc(1, sizeof(Properties_Content));
	new->type = this->type;
	new->value = this->value;
	if (new->type == PROPERTIES_STRING) {
		new->value.s = StringUtils_clone(this->value.s);
	}
	else if (new->type == PROPERTIES_PROPERTIES) {
		new->value.p = Properties_clone(this->value.p);
	}

	return new;
}

/** Appends the string representation of the value of 'this' to stream 'file'.
	Return 0 on success and an error code (int < 0) on failure
*/
int Properties_Content_Value_fprint_withIndent(const Properties_Content* this, FILE* file, int level) {
	int result = 0;
	// fprintf returns the nb of chars written, or an int < 0 in case of error
	switch (this->type) {
		case PROPERTIES_INT :
			result = fprintf(file, "%d", this->value.i);
			break;
		case PROPERTIES_UINT :
			result = fprintf(file, "%u", this->value.ui);
			break;
		case PROPERTIES_ULONG :
			result = fprintf(file, "%lu", this->value.ul);
			break;
		case PROPERTIES_LONG :
			result = fprintf(file, "%ld", this->value.l);
			break;
		case PROPERTIES_BOOLEAN :
			result = fprintf(file, "%s", (this->value.b ? "TRUE" : "FALSE" )	);
			break;
		case PROPERTIES_DOUBLE :
			result = fprintf(file, "%.17g", this->value.d);
			break;
		case PROPERTIES_NUMERIC :
			result = fprintf(file, "%.17g", this->value.d);
			break;
		case PROPERTIES_STRING :
			result = fprintf(file, "%s", this->value.s);
			break;
		case PROPERTIES_POINTER :
			result = fprintf(file, "%p", this->value.v);
			break;
		case PROPERTIES_PROPERTIES :
			result = fprintf(file, "{\n");
			result += Properties_fprint_withIndent(this->value.p, file, level+1);
			result += _fprint_INDENT_STRING(file, level);
			result += fprintf(file, "}");
			break;
		default:
			__Debug_assert(0, "Type unknown = %d\n", this->type);
			fprintf(file, "ERROR");
			result = -1;
	}
	return result;
}

int Properties_Content_Value_fprint(const void* this_v, FILE* file) {
	Properties_Content* this = (Properties_Content*)this_v;
	return Properties_Content_Value_fprint_withIndent(this, file, 0);
}


/* Builds a new Properties_Content instance having type t (!= PROPERTIES) and a *copy* of *value_string as a value (converted to type t).

This function is used when loading a Properties object from a text file. Thus,
if type = PROPERTIES_POINTER, value_stirng is read as NULL (as it would make no sense doing otherwise).
*/
Properties_Content* _Properties_Content_newFromString(Properties_Content_Type type, const char* value_string) {
	__Debug_assert(type != PROPERTIES_PROPERTIES, "type == PROPERTIES\n");
	__Debug_out( "Parsing %s value '%s'...\n", Properties_Content_Type_toString(type), value_string);
	const void* value = NULL;
	int err = 0;
	Properties_Content_Value value_cv = {0};
	switch (type) {
		case PROPERTIES_INT :
			err = StringUtils_toInt(value_string, &value_cv.i);
			value = &value_cv.i;
			break;
		case PROPERTIES_UINT :
			err = StringUtils_toUInt(value_string, &value_cv.ui);
			value = &value_cv.ui;
			break;
		case PROPERTIES_ULONG :
			err = StringUtils_toULong(value_string, &value_cv.ul);
			value = &value_cv.ul;
			break;
		case PROPERTIES_LONG :
			err = StringUtils_toLong(value_string, &value_cv.l);
			value = &value_cv.l;
			break;
		case PROPERTIES_BOOLEAN :
			err = StringUtils_toBoolean(value_string, &value_cv.b);
			value = &value_cv.b;
			__Debug_out( " --> Boolean value parsed: '%s' --> %c (err = %d)\n",
				value_string, value_cv.b, err);
			break;
		case PROPERTIES_NUMERIC :
		case PROPERTIES_DOUBLE :
			err = StringUtils_toDouble(value_string, &value_cv.d);
			value = &value_cv.d;
			break;
		case PROPERTIES_STRING :
			if (StringUtils_equalsIgnoreCase("(null)", value_string)) {
				value = NULL;
			} else {
				value = value_string;
			}
			break;
		case PROPERTIES_POINTER :
			value = NULL;
			break;
		default:
			__Debug_assert(0, "Type unknown = %d\n", type);
			return NULL;
	}
	if (err) {
		__Debug_assert(0, "Value '%s' is not of type '%s'\n",
			value_string, Properties_Content_Type_toString(type));
		return NULL;
	}
	Properties_Content* result = _Properties_Content_new(type, value);
	__Debug_out( "	--> done. Value is: ");
	__Debug_perform(Properties_Content_Value_fprint(result, stderr));
	__Debug_perform(fprintf(stderr, "\n"));
	return result;
}
Properties_Content_Type Properties_Content_type(const Properties_Content* this) {
	return this->type;
}
const void* Properties_Content_value(const Properties_Content* this) {
	const void* value = NULL;
	switch (this->type) {
		case PROPERTIES_INT:
			value = &this->value.i;
			break;
		case PROPERTIES_UINT:
			value = &this->value.ui;
			break;
		case PROPERTIES_ULONG:
			value = &this->value.ul;
			break;
		case PROPERTIES_LONG:
			value = &this->value.l;
			break;
		case PROPERTIES_BOOLEAN:
			value = &this->value.b;
			break;
		case PROPERTIES_DOUBLE:
			case PROPERTIES_NUMERIC:
			value = &this->value.d;
			break;
		case PROPERTIES_STRING:
			value = this->value.s;
			break;
		case PROPERTIES_POINTER:
			value = this->value.v;
			break;
		case PROPERTIES_PROPERTIES:
			value = this->value.p;
			break;
		default:
			__Debug_assert(0, "Type unknown = %d\n", this->type);
			return NULL;
	}
	return value;
}




// Properties methods



/* char* _Properties_fullLengthKey(const Properties* this, const char* property, size_t propertyLen) { */
/* 	assert(this != NULL); */
/* 	__Debug_out( "_Properties_fullLengthKey(%p, '%s')\n", (void*)this, property); */
/* 	assert(this->auxKeyMaxLen != NULL); //, "this->auxKeyMaxLen == NULL\n"); */
/* 	assert(property != NULL); //, "property == NULL\n"); */
/*   size_t propertyLenActual = this->keyMaxLen; */
/*   if ((propertyLen > 0) && (propertyLen < propertyLenActual)) { */
/*     propertyLenActual = propertyLen; */
/*   } */
/* 	strncpy(this->auxKeyMaxLen, property, propertyLenActual); */
/*   this->auxKeyMaxLen[propertyLenActual] = '\0'; */
/* 	__Debug_out( "_Properties_fullLengthKey(): auxKeyMaxLen now '%s'\n", this->auxKeyMaxLen); */

/* 	StringUtils_rightPad(this->auxKeyMaxLen, this->keyMaxLen, '\0'); */
/* 	return this->auxKeyMaxLen; */
/* } */

static Properties *_Properties_new() {
	// Here we cannot use Debug_assert()!
	Properties* new = calloc(1, sizeof(Properties));
	assert(new != NULL);
	new->protected = false;
	new->hTable = HashTable_new();
	assert(new->hTable != NULL);
	return new;
}

static Properties *Properties_newProtected() {
	// Here we cannot use Debug_assert()!
	Properties *new = _Properties_new();
	new->protected = true;
	return new;
}

Properties* Properties_new() {
	// Here we cannot use Debug_assert()!
	Properties *new = _Properties_new();
	new->protected = false;
	return new;
}

void Properties_copyInto(const Properties* this, Properties* dest) {
	assert(this != NULL);
	assert(dest != NULL);

	HashTableIterator* it = HashTableIterator_new(this->hTable);
	HashTable_Entry* e = NULL;
	while ((e = HashTableIterator_next(it)) != NULL) {
		const char* key = (const char*)HashTable_Entry_key(e);
    /* fprintf(stdout, "%s %zu\n", key, HashTable_Entry_keySize(e)); */
		Properties_Content* content = (Properties_Content*)HashTable_Entry_value(e);
		Properties_Content *contentDest = HashTable_put(dest->hTable, key, HashTable_Entry_keySize(e), _Properties_Content_clone(content));
    if (contentDest != NULL) {
      _Properties_Content_free(&contentDest);
    }
	}
	HashTableIterator_free(&it);
}

Properties* Properties_clone(const Properties* this) {
	Properties* new = Properties_new();
	Properties_copyInto(this, new);
	return new;
}





void Properties_clear(Properties* this) {
	assert(this != NULL);
	HashTableIterator* it = HashTableIterator_new(this->hTable);
	HashTable_Entry* e = NULL;
  char const *key = NULL;
	while ((e = HashTableIterator_next(it)) != NULL) {
		Properties_Content* content = (Properties_Content*)HashTable_Entry_value(e);
		if (content != NULL) {
			_Properties_Content_free(&content);
		}
    key = HashTable_Entry_key(e);
		HashTable_remove(this->hTable, key, HashTable_Entry_keySize(e));
	}
	HashTableIterator_free(&it);
}

void Properties_remove(Properties* _this, const char* property) {
	__Debug_assert(_this != NULL, "this == NULL\n");
	__Debug_assert(property != NULL, "property == NULL\n");
  Properties_Content const *content = NULL;
  int sepPos = StringUtils_strpos(property, '/');
  if (sepPos >= 0) {
    content = getAux(_this, property, (size_t)sepPos);
    if (content != NULL) {
      if (content->type == PROPERTIES_PROPERTIES) {
        Properties *nested = content->value.p;
        Properties_remove(nested, property + sepPos + 1);
      }
    }
  } else {
    Properties_Content* old_content =
      (Properties_Content*)HashTable_remove(_this->hTable, property, strlen(property) + 1);
    if (old_content != NULL) {
      _Properties_Content_free(&old_content);
    }
  }
}

static void setAux(Properties *_this, const char *property, size_t propertyLen,
                   Properties_Content *content) {
  Properties_Content *old_content = (Properties_Content *)HashTable_put(_this->hTable, property, propertyLen, content);
  if (old_content != NULL) {
    __Debug_out("Value for property '%s' redefined. New value:\n", property);
    __Debug_perform(Properties_Content_Value_fprint(content, stderr));
    // free old value
    _Properties_Content_free(&old_content);
  }
}

void _Properties_set(Properties *_this, const char *property,
                     Properties_Content *content) {
  __Debug_assert(content != NULL, "content == NULL\n");
  /* int sepPos = StringUtils_strpos(property, '/'); */
  /* size_t attrLength = 0; */
  /* if (sepPos >= 0) { */
  /*   attrLength = (size_t)sepPos; */
  /* } else { */
  /*   attrLength = strlen(property); */
  /* } */
  /* if (sepPos >= 0) { */
  /*   Properties_Content const *contentNested = */
  /*       getAux(_this, property, (size_t)sepPos); */
  /*   Properties *nested = NULL; */
  /*   bool nestedCreated = false; */
  /*   if ((contentNested == NULL) || */
  /*       (contentNested->type != PROPERTIES_PROPERTIES)) { */
  /*     nested = Properties_new(); */
  /*     nestedCreated = true; */
  /*   } else { */
  /*     nested = contentNested->value.p; */
  /*   } */
  /*   _Properties_set(nested, property + sepPos + 1, content); */
  /*   if (nestedCreated) { */
  /*     setAux(_this, property, (size_t)sepPos, */
  /*            _Properties_Content_new(PROPERTIES_PROPERTIES, nested)); */
  /*     Properties_free(&nested); */
  /*   } */
  /* } else { */
    setAux(_this, property, strlen(property) + 1, content);
  /* } */
}

void Properties_setDouble(Properties* this, const char* property, double value) {
	_Properties_set(this, property,
		_Properties_Content_new(PROPERTIES_DOUBLE, &value));
}

void Properties_setNumeric(Properties* this, const char* property, double value) {
	_Properties_set(this, property,
			_Properties_Content_new(PROPERTIES_NUMERIC, &value));
}

void Properties_setInt(Properties* this, const char* property, int value) {
	_Properties_set(this, property,
		_Properties_Content_new(PROPERTIES_INT, &value));
}
void Properties_setUInt(Properties* this, const char* property, unsigned int value) {
	_Properties_set(this, property,
		_Properties_Content_new(PROPERTIES_UINT, &value));
}
void Properties_setULong(Properties* this, const char* property, unsigned long value) {
	_Properties_set(this, property,
		_Properties_Content_new(PROPERTIES_ULONG, &value));
}
void Properties_setLong(Properties* this, const char* property, long value) {
	_Properties_set(this, property,
		_Properties_Content_new(PROPERTIES_LONG, &value));
}

void Properties_setBoolean(Properties* this, const char* property, char value) {
	if (this == NULL) fprintf(stderr, "this==NULL\n");
	__Debug_assert(this != NULL, "this == NULL\n");
	__Debug_assert(property != NULL, "property == NULL\n");
	Properties_Content* content = _Properties_Content_new(PROPERTIES_BOOLEAN, &value);
	_Properties_set(this, property, content);
}

void Properties_setString(Properties* this, const char* property, const char* value) {
	_Properties_set(this, property,
		_Properties_Content_new(PROPERTIES_STRING, value));
}

void Properties_setPointer(Properties* this, const char* property, const void* value) {
	_Properties_set(this, property,
		_Properties_Content_new(PROPERTIES_POINTER, value));
}

void Properties_setNestedProperties(Properties* this, const char* property, const Properties* value) {
	_Properties_set(this, property,
		_Properties_Content_new(PROPERTIES_PROPERTIES, value));
}

// Setter methods: the value is always cloned (also if it is of type Properties)
void Properties_set(Properties* this, const char* property, Properties_Content_Type type, const void* value) {
	_Properties_set(this, property, _Properties_Content_new(type, value));
}

int Properties_setFromString(Properties* this, const char* property, Properties_Content_Type type, const char* value_string) {
	Properties_Content* content = _Properties_Content_newFromString(type, value_string);
	if (content == NULL) return -1;
	_Properties_set(this, property, content);
	return 0;
}


// File syntax:  <type> <property> = <value>
Properties* Properties_newFromFile(const char* fname) {
	__Debug_out( "Properties_newFromFile(): start\n");

	FILE* f = fopen(fname, "r");
	__Debug_assert(f != NULL, "Properties_newFromFile(): could not open file '%s'\n", fname);
	if (f == NULL) {
		perror("Could not open Properties file\n");
		return NULL;
	}
  Properties *ret = Properties_newFromStream(f, fname);
	int err = fclose(f);
	if (err) {
		fprintf(stderr, "Error while closing file '%s'. fclose() returned error %d\n", fname, err);
		__Debug_assert(0, "Error while closing file '%s'. "
			"fclose() returned error %d\n", fname, err);
	}
  return ret;
}

Properties *Properties_newFromStream(FILE *f, const char *fname) {
  __Debug_assert(f != NULL, "f == NULL\n");
	Array* nestedPropertiesStack = Array_new(1024, sizeof(Properties*), 1024, NULL, NULL);
	Array* nestedPropertiesNamesStack = Array_new(1024, sizeof(char*), 1024, NULL, NULL);

	Properties* new = Properties_new();
	Array_push(nestedPropertiesStack, &new);
	__Debug_out( "Properties_newFromFile(): pushing Properties (new): %p\n", (void*)new);

	char* line = calloc(LINE_MAX_LEN+1, sizeof(char));
	int lnumber = 0;
	char* s = NULL;
	// Read file lines
	while (fgets(line, LINE_MAX_LEN, f) != NULL) {
		lnumber++;
		__Debug_out( "newFromFile(): reading line %d\n", lnumber);
		int startComment = StringUtils_strpos(line, '#');
		if (startComment >= 0) {
			__Debug_out( "line %d: found comment sign at column %d\n", lnumber, startComment);
			line[startComment] = '\0';
		}
		StringUtils_trim(line);
		__Debug_out( "line %d: '%s'\n", lnumber, line);
		// Skip comments
		if (strlen(line) == 0) continue;

		// check whether end-of-properties block is reached (i.e., line is "}")
		if (StringUtils_equals(line, "}")) {
			Properties* nested = NULL;
			Array_pop(nestedPropertiesStack, &nested);
			__Debug_perform(Properties_fprint(nested, stderr));
			char* property = NULL;
			Array_pop(nestedPropertiesNamesStack, &property);
			__Debug_assert(property != NULL, "property == NULL\n");
			__Debug_out( "Properties_newFromFile(): popping Properties '%s': %p\n",
				property, (void*)nested);
			Properties* currentProperties = NULL;
			Array_top(nestedPropertiesStack, &currentProperties);
			__Debug_perform(Properties_fprint(currentProperties, stderr));
			Properties_Content* content =
					_Properties_Content_new(PROPERTIES_PROPERTIES, nested);
			if (content == NULL) {
				__Debug_assert(content != NULL, "content == NULL\n");
				fprintf(stderr, "Error in file '%s': could not parse correctly value for property name "
					"'%s' of type Properties\n",
					fname, property);
			}
			_Properties_set(currentProperties, property, content);
			Properties_freeProtected(&nested);
			free(property);
		}
		else {
			// Tokenize line: type property name = propertyValue
			char* type_and_property = line;
			int eq_pos = StringUtils_strpos(line, '=');
			if (eq_pos < 0) {
				// No '=' -> malformed line
				fprintf(stderr, "Error in reading Properties from file '%s':%d. "
					"No occurrence of '='\n",
					fname, lnumber);
				__Debug_assert(0, "Error in reading Properties from file '%s':%d. "
					"No occurrence of '='\n",
					fname, lnumber);
				continue;
			}

			line[eq_pos] = '\0'; // terminator for type_and_property
			char* value_string = &line[eq_pos+1];

			StringUtils_trim(type_and_property);
			StringUtils_trim(value_string);

			char* type_string = type_and_property;
			char* property = NULL;
			Properties_Content_Type type = PROPERTIES_UNKNOWN;
			int space_pos = StringUtils_strpos(type_and_property, ' ');

			if (space_pos < 0) {
				// Omitted type
				type = PROPERTIES_UNKNOWN;
			} else {
				// Type given (or omitted in a multi-word property)
				type_string[space_pos] = '\0';
				property = &type_and_property[space_pos+1];
				type = _Properties_Content_Type_parse(type_string);
				if (type == PROPERTIES_UNKNOWN) {
					type_string[space_pos] = ' ';
				}
			}
			if (type == PROPERTIES_UNKNOWN) {
				type = _Properties_Content_Type_infer(value_string);
				property = type_and_property;
			}
			StringUtils_trim(property);

			// line read in vars: type "property" = "value_string"

			__Debug_out( "newFromFile(): type = '%s', property = '%s', value = '%s'\n",
				Properties_Content_Type_toString(type), property, value_string);

			if (type == PROPERTIES_PROPERTIES) {
				// nested properties start
				__Debug_assert(StringUtils_equals(value_string, "{"),
					"{ '<newline>' missing after "
					"'Properties pname = '\n");
				Properties* nested = Properties_newProtected();
				Array_push(nestedPropertiesStack, &nested);
				__Debug_out( "Properties_newFromFile(): pushing Properties '%s': %p\n",
					property, (void*)nested);
				char* property_clone = StringUtils_clone(property);
				Array_push(nestedPropertiesNamesStack, &property_clone);
			} else {
				// "single-line" property
				Properties_Content* content =
					_Properties_Content_newFromString(type, value_string);
				if (content == NULL) {
					__Debug_assert(content != NULL, "content == NULL\n");
					fprintf(stderr, "Error in file '%s': value '%s' is not a value of type '%s'. "
						"Skipping...",
						fname, value_string, Properties_Content_Type_toString(type));
				}
				Properties* currentProperties = NULL;
				Array_top(nestedPropertiesStack, &currentProperties);
				__Debug_assert(currentProperties != NULL, "currentProperties == NULL\n");
				__Debug_out( "Adding single-line property '%s' property to Properties %p...\n",
					property, (void*)currentProperties);
				_Properties_set(currentProperties, property, content);
				__Debug_out( "	--> done\n");
			}
		}
	}

	free(line);

	// check whether nested properties have all been terminated
	__Debug_assert(Array_length(nestedPropertiesStack) == 1,
		"Error: Missing '}' somewhere in file file '%s'\n", fname);

	Array_free(&nestedPropertiesStack);
		Array_free(&nestedPropertiesNamesStack);


	return new;
}

#include "memmap/src/fmemopen.h"

void Properties_intoString(Properties *this, char str[], size_t cap) {
  __Debug_assert(this != NULL, "this == NULL\n");
  __Debug_assert(cap > 0, "cap <= 0\n");
  FILE *f_str = fmemopen(str, cap * sizeof(char), "w");
  Properties_fprint(this, f_str);
  fclose(f_str);
}

size_t Properties_toString(Properties *this, char **str, size_t cap) {
  __Debug_assert(this != NULL, "this == NULL\n");
  __Debug_assert(cap > 0, "cap <= 0\n");
  *str = (char *) calloc(cap, sizeof(char));
  FILE *f_str = fmemopen(*str, cap * sizeof(char), "w");
  int ret = Properties_fprint(this, f_str);
  size_t size = 0;
  if (ret > 0) size = (size_t) ret;
  fclose(f_str);
  return size;
}

Properties *Properties_newFromString(const char *str) {
  __Debug_assert(str != NULL, "str == NULL\n");
  FILE *f_str = fmemopen((void *) str, strlen(str), "r");
  Properties *ret = Properties_newFromStream(f_str, "from string");
  fclose(f_str);
  return ret;
}

int Properties_fprint(const void* this_v, FILE* f) {
	return Properties_fprint_withIndent((Properties*)this_v, f, 0);
}

int Properties_fprint_withIndent(const Properties* this, FILE* f, int level) {
	Properties_Iterator* it = Properties_Iterator_new(this);
	const Properties_Entry* e = NULL;
	int result = 0;
	while ((e = Properties_Iterator_next(it)) != NULL) {
		const char* property = Properties_Entry_property(e);
		const Properties_Content* content = Properties_Entry_value(e);
		result += _fprint_INDENT_STRING(f, level);
		result += fprintf(f, "%s %s = ",
			Properties_Content_Type_toString(content->type),
			property
		);
		result += Properties_Content_Value_fprint_withIndent(content, f, level);
		result += fprintf(f, "\n");
	}
	Properties_Iterator_free(&it);
	return result;
}

int Properties_save(const Properties* this, const char* filePath) {
	FILE* f = fopen(filePath, "w");
	if (f == NULL) {
		fprintf(stderr, "Cannot open file '%s' to write properties\n", filePath);
		return -1;
	}
	Properties_fprint(this, f);
	int err = fclose(f);
	if (err) {
		fprintf(stderr, "Cannot close file '%s' after writing. fclose() returned error %d\n", filePath, err);
		return -1;
	}
	return 0;
}

unsigned long Properties_size(const Properties* this) {
	return HashTable_size(this->hTable);
}


// Getter methods
int Properties_contains(const Properties* this, const char* property) {
	const Properties_Content* const result = Properties_get(this, property);
	return (result != NULL);
}

static Properties_Content const *
getAux(Properties const *this, char const *property, size_t propertyLen) {
  __Debug_assert(this != NULL, "this == NULL\n");
  Properties_Content const *result = NULL;
  __Debug_assert(this->hTable != NULL, "this->hTable == NULL\n");
  HashTable_get(this->hTable, property, propertyLen, (void **)&result);
  __Debug_out("Properties_get(): HashTable_get() done: result = %p\n",
              (void *)result);
  return result;
}

Properties_Content const *Properties_get(Properties const *_this,
                                         char const *property) {
  __Debug_assert(_this != NULL, "this == NULL\n");
  __Debug_assert(property != NULL, "property == NULL\n");
  Properties_Content const *result = NULL;
  /* int sepPos = StringUtils_strpos(property, '/'); */
  /* if (sepPos >= 0) { */
  /*   result = getAux(_this, property, (size_t)sepPos); */
  /*   if (result != NULL) { */
  /*     if (result->type != PROPERTIES_PROPERTIES) { */
  /*       result = NULL; */
  /*     } else { */
  /*       Properties const *nested = result->value.p; */
  /*       result = Properties_get(nested, property + sepPos + 1); */
  /*     } */
  /*   } */
  /* } else { */
    result = getAux(_this, property, strlen(property) + 1);
  /* } */
  return result;
}


int Properties_getInt(const Properties* this, const char* property, int* value) {
	const Properties_Content* const result = Properties_get(this, property);
	if (result == NULL) return -1;
	if (result->type != PROPERTIES_INT) return -2;
	*value = result->value.i;
	return 0;
}
int Properties_getUInt(const Properties* this, const char* property, unsigned int* value) {
	const Properties_Content* const result = Properties_get(this, property);
	if (result == NULL) return -1;
	if (result->type != PROPERTIES_UINT) return -2;
	*value = result->value.ui;
	return 0;
}
int Properties_getULong(const Properties* this, const char* property, unsigned long* value) {
	const Properties_Content* const result = Properties_get(this, property);
	if (result == NULL) return -1;
	if (result->type != PROPERTIES_ULONG) return -2;
	*value = result->value.ul;
	return 0;
}
int Properties_getLong(const Properties* this, const char* property, long* value) {
	const Properties_Content* const result = Properties_get(this, property);
	if (result == NULL) return -1;
	if (result->type != PROPERTIES_LONG) return -2;
	*value = result->value.l;
	return 0;
}
int Properties_getBoolean(const Properties* this, const char* property, char* value) {
	const Properties_Content* const result = Properties_get(this, property);
	if (result == NULL) return -1;
	if (result->type != PROPERTIES_BOOLEAN) return -2;
	*value = result->value.b;
	return 0;
}

int Properties_getNumeric(const Properties* this, const char* property, double* value) {
	__Debug_out( "Properties_getDouble(this, '%s', %p) start\n", property, (void*)value);
	const Properties_Content* const result = Properties_get(this, property);
	__Debug_out( "Properties_getDouble(this, '%s', %p): _get() returned %p\n",
		property, (void*)value, (void*)result);

	if (result == NULL) return -1;
	if (result->type != PROPERTIES_NUMERIC) return -2;
	*value = result->value.d;
	return 0;
}

int Properties_getDouble(const Properties* this, const char* property, double* value) {
	__Debug_out( "Properties_getDouble(this, '%s', %p) start\n", property, (void*)value);
	const Properties_Content* const result = Properties_get(this, property);
	__Debug_out( "Properties_getDouble(this, '%s', %p): _get() returned %p\n",
		property, (void*)value, (void*)result);

	if (result == NULL) return -1;
	if (result->type != PROPERTIES_DOUBLE) return -2;
	*value = result->value.d;
	return 0;
}

int Properties_getString(const Properties* this, const char* property, const char** value) {
	const Properties_Content* const result = Properties_get(this, property);
	if (result == NULL) return -1;
	if (result->type != PROPERTIES_STRING) return -2;
	*value = result->value.s;
	return 0;
}

int Properties_getPointer(const Properties* this, const char* property, const void** value) {
	const Properties_Content* const result = Properties_get(this, property);
	if (result == NULL) return -1;
	if (result->type != PROPERTIES_POINTER) return -2;
	*value = result->value.v;
	return 0;
}

int Properties_getNestedProperties(const Properties* this, const char* property, Properties** value) {
	const Properties_Content* const result = Properties_get(this, property);
	if (result == NULL) return -1;
	if (result->type != PROPERTIES_PROPERTIES) return -2;
	*value = result->value.p;
	return 0;

}


int Properties_type(const Properties* this, const char* property, Properties_Content_Type* type) {
	const Properties_Content* const content = Properties_get(this, property);
	//__Debug_assert(content != NULL, "Property '%s' not found\n", property);
	if (content == NULL) return -1;
	else {
		*type = content->type;
		return 0;
	}
}

static void _Properties_free(Properties **thisP){
	Properties* this = *thisP;
	HashTableIterator* it = HashTableIterator_new(this->hTable);
	HashTable_Entry* e = NULL;
	while ((e = HashTableIterator_next(it)) != NULL) {
		Properties_Content* content = (Properties_Content*)HashTable_Entry_value(e);
		Properties_Content_Type type = Properties_Content_type(content);
		_Properties_Content_free(&content);
	}
	HashTableIterator_free(&it);
	HashTable_free(&this->hTable);
	free(this);
	*thisP = NULL;
}

static void Properties_freeProtected(Properties **thisP) {
	__Debug_assert(thisP != NULL, "thisP == NULL\n");
	__Debug_assert((*thisP)->protected == true, "Invalid free of a non-protected property.\n");
	_Properties_free(thisP);
}

void Properties_free(Properties** thisP) {
	__Debug_assert(thisP != NULL, "thisP == NULL\n");
	__Debug_assert((*thisP)->protected == false, "Invalid free of a protected properties.\n");
	_Properties_free(thisP);
}

static size_t Properties_getContentSerializedSize(Properties_Content const *content) {
  size_t res = sizeof(unsigned char); // for the type
	switch (content->type) {
		case PROPERTIES_INT : res += sizeof(int); break;
		case PROPERTIES_UINT: res += sizeof(unsigned int); break;
		case PROPERTIES_ULONG: res += sizeof(unsigned long); break;
		case PROPERTIES_LONG: res += sizeof(long); break;
		case PROPERTIES_BOOLEAN : res += sizeof(int); break;
		case PROPERTIES_DOUBLE : res += sizeof(double); break;
		case PROPERTIES_NUMERIC : res += sizeof(double); break;
		case PROPERTIES_STRING : res += strlen(content->value.s) + 1; break;
		case PROPERTIES_POINTER : res += sizeof(void *); break;
		case PROPERTIES_PROPERTIES : res += Properties_getSerializedSize(content->value.p); break;
    default: __Debug_assert(false, "Unknown properties type!\n");
	}
  return res;
}

size_t Properties_getSerializedSize(Properties const *_this) {
  size_t res = 0;
  res += sizeof(unsigned long); // for the number of properties inside _this
  Properties_Iterator *iter = Properties_Iterator_new(_this);
  Properties_Entry const *entry = NULL;
  while ((entry = Properties_Iterator_next(iter)) != NULL) {
    char const *property = Properties_Entry_property(entry);
    res += strlen(property) + 1; // property key
    Properties_Content const *content = Properties_Entry_value(entry);
    res += Properties_getContentSerializedSize(content);
  }
  return res;
}

static size_t serializeAux(Properties const *_this, void *bufPtr);

static size_t Properties_Content_serialize(Properties_Content const *content, void **bufPtr) {
  unsigned char *b = *((unsigned char **) bufPtr);
  *b = (unsigned char) Properties_Content_type(content);
  b += 1;
  size_t sz = sizeof(unsigned char);
  void const *p = NULL;
  size_t szValue = 0;
  switch (content->type) {
  case PROPERTIES_INT:
    szValue = sizeof(int);
    p = &content->value.i;
    break;
  case PROPERTIES_UINT:
    szValue = sizeof(unsigned int);
    p = &content->value.ui;
    break;
  case PROPERTIES_ULONG:
    szValue = sizeof(unsigned long);
    p = &content->value.ul;
    break;
  case PROPERTIES_LONG:
    szValue = sizeof(long);
    p = &content->value.l;
    break;
  case PROPERTIES_BOOLEAN:
    szValue = sizeof(int);
    p = &content->value.i;
    break;
  case PROPERTIES_DOUBLE:
    szValue = sizeof(double);
    p = &content->value.d;
    break;
  case PROPERTIES_NUMERIC:
    szValue = sizeof(double);
    p = &content->value.d;
    break;
  case PROPERTIES_STRING:
    szValue = strlen(content->value.s) + 1;
    p = content->value.s;
    break;
  case PROPERTIES_POINTER:
    szValue = sizeof(void *);
    p = content->value.p;
    break;
  case PROPERTIES_PROPERTIES:
    szValue = serializeAux(content->value.p, b);
    break;
  default:
    __Debug_assert(false, "Unknown properties type!\n");
  }
  if (content->type != PROPERTIES_PROPERTIES) {
    memcpy(b, p, szValue);
  }
  b += szValue;
  sz += szValue;
  *bufPtr = b;
  return sz;
}

static size_t serializeAux(Properties const *_this, void *b) {
  unsigned long num = Properties_size(_this);
  *((unsigned long *) b) = Properties_size(_this);
  size_t sz = 0;
  b += sizeof(unsigned long);
  sz += sizeof(unsigned long);
  Properties_Iterator *iter = Properties_Iterator_new(_this);
  Properties_Entry const *entry = NULL;
  while ((entry = Properties_Iterator_next(iter)) != NULL) {
    char const *property = Properties_Entry_property(entry);
    sprintf((char *) b, "%s", property);
    size_t szProperty = strlen(property) + 1;
    b += szProperty;
    sz += szProperty;
    Properties_Content const *content = Properties_Entry_value(entry);
    size_t szContent = Properties_Content_serialize(content, (void **) &b);
    sz += szContent;
  }
  return sz;
}

size_t Properties_serialize(Properties const *_this, void **bufPtr) {
  if (*bufPtr == NULL) {
    size_t bufSize = Properties_getSerializedSize(_this);
    *bufPtr = malloc(bufSize);
  }
  return serializeAux(_this, *bufPtr);
}

static Properties *deserializeAux(unsigned char *buf, size_t *sz);

static Properties_Content *Properties_Content_deserialize(unsigned char **buf, size_t *sz) {
  Properties_Content *res = NULL;
  unsigned char *b = *buf;
  Properties_Content_Type type = (Properties_Content_Type) (*b);
  b += sizeof(unsigned char);
  *sz = sizeof(unsigned char);
  size_t szValue = 0;
  switch (type) {
  case PROPERTIES_INT:
    szValue = sizeof(int);
    break;
  case PROPERTIES_UINT:
    szValue = sizeof(unsigned int);
    break;
  case PROPERTIES_ULONG:
    szValue = sizeof(unsigned long);
    break;
  case PROPERTIES_LONG:
    szValue = sizeof(long);
    break;
  case PROPERTIES_BOOLEAN:
    szValue = sizeof(int);
    break;
  case PROPERTIES_DOUBLE:
    szValue = sizeof(double);
    break;
  case PROPERTIES_NUMERIC:
    szValue = sizeof(double);
    break;
  case PROPERTIES_STRING:
    szValue = strlen((char *) b) + 1;
    break;
  case PROPERTIES_POINTER:
    szValue = sizeof(void *);
    break;
  case PROPERTIES_PROPERTIES:
    res = calloc(1, sizeof(Properties_Content));
    res->type = type;
    res->value.p = deserializeAux(b, &szValue);
    res->value.p->protected = true;
    break;
  default:
    __Debug_assert(false, "Unknown properties type!\n");
  }
  if (type != PROPERTIES_PROPERTIES) {
    res = _Properties_Content_new(type, b);
  }
  b += szValue;
  *sz += szValue;
  *buf = b;
  return res;
}

static Properties *deserializeAux(unsigned char *b, size_t *sz) {
  Properties *res = Properties_new();
  *sz = 0;
  unsigned long num = *((unsigned long *) b);
  b += sizeof(unsigned long);
  *sz += sizeof(unsigned long);
  size_t szElem = 0;
  for (size_t i = 0; i < num; ++i) {
    char const *property = (char const *) b;
    b += strlen(property) + 1;
    *sz += strlen(property) + 1;
    Properties_Content *c = Properties_Content_deserialize(&b, &szElem);
    *sz += szElem;
    _Properties_set(res, property, c);
  }
  return res;
}

Properties *Properties_newFromSerialized(void const *buf) {
  unsigned char *b = (unsigned char *) buf;
  size_t sz = 0;
  return deserializeAux(b, &sz);
}


// Properties Iterator
struct Properties_Iterator {
	HashTableIterator* hTableIt;
};

/** Create an Iterator for this Properties */
Properties_Iterator* Properties_Iterator_new(const Properties* this) {
	Properties_Iterator* new = calloc(1, sizeof(Properties_Iterator));
	__Debug_assert(new != NULL, "new == NULL\n");
	new->hTableIt = HashTableIterator_new(this->hTable);
	__Debug_assert(new->hTableIt != NULL, "new->hTableIt == NULL\n");
	return new;
}

void Properties_Iterator_rewind(Properties_Iterator* this) {
	HashTableIterator_rewind(this->hTableIt);
}

/** Return 1 if a call to Properties_Iterator_next(this) will return an entry, 0 otherwise */
int Properties_Iterator_hasNext(const Properties_Iterator* this) {
	return HashTableIterator_hasNext(this->hTableIt);
}

/** Return the next entry of the HashTable which 'this' is iterating on,
	NULL if no more entries exist */
const Properties_Entry* Properties_Iterator_next(Properties_Iterator* this) {
	return HashTableIterator_next(this->hTableIt);
}

const char* Properties_Entry_property(const Properties_Entry* this) {
	return (char*)HashTable_Entry_key(this);
}
const char* Properties_Entry_key(const Properties_Entry* this) {
	return Properties_Entry_property(this);
}
const Properties_Content* Properties_Entry_value(const Properties_Entry* this) {
	return (Properties_Content*)HashTable_Entry_value(this);
}

void Properties_Iterator_free(Properties_Iterator** thisP) {
	__Debug_assert(thisP != NULL, "thisP == NULL\n");
	Properties_Iterator* this = *thisP;
	HashTableIterator_free(&this->hTableIt);
	free(this);
	*thisP = NULL;
}
