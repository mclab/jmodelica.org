/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>

#include <Debug.h>

#include <Range_Int.h>
#include <StringUtils.h>

#define DEBUG "Range_Int"



struct Range_Int {
	int min;
	int max;
	int step;

	// cache
	int greatest; // greatest value
	int size;
};


int _size_debug(const Range_Int* const this) {
	int result = 0;
	for(int i = this->min; i <= this->max; i += this->step) {
		result++;
	}
	return result;
}


int Range_Int_equals(const Range_Int* this, const Range_Int* other) {
	if (this == NULL) return other == NULL;
	if (other == NULL) return 0;
	return this->min == other->min
		&& this->max == other->max
		&& this->step == other->step;
}

int _Range_Int_greatest(const Range_Int* this, int step) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	Debug_assert(DEBUG_ALWAYS, step >= 0, "step <= 0\n");
	int result = this->min + step*((this->max-this->min)/step);

	Debug_assert(DEBUG_ALWAYS, result >= this->min, "greatest = %d < min = %d\n",
		result, this->min);
	Debug_assert(DEBUG_ALWAYS, result <= this->max, "greatest = %d > max = %d\n",
		result, this->max);
	Debug_assert(DEBUG_ALWAYS, result+step > this->max, "greatest + step = "
		"%d + %d <= max = %d\n",
		result, step, this->max);
	return result;
}
int Range_Int_greatest(const Range_Int* this) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	return _Range_Int_greatest(this, this->step);
}

Range_Int* Range_Int_new(int min, int max, int step) {
	Debug_assert(DEBUG_ALWAYS, max >= min, "max = %d < %d = min\n", max, min);
	Debug_assert(DEBUG_ALWAYS, step > 0, "step = %d <= '\n", step);
	Debug_assert(DEBUG_ALWAYS, step <= max-min+1, "step = %d > max-min+1 = %d '\n", step, max-min+1);
	Range_Int* new = calloc(1, sizeof(Range_Int));
	Debug_assert(DEBUG_ALWAYS, new != NULL, "new == NULL\n");
	new->min = min;
	new->max = max;
	new->step = step;

	new->greatest = Range_Int_greatest(new);

	new->size = 1 + (max-min)/step;
	Debug_assert(DEBUG, new->size == _size_debug(new), "new->size = %d != "
		"%d = _size_debug()\n", new->size, _size_debug(new));
	return new;
}

void Range_Int_fprint(const Range_Int* this, FILE* file) {
	//Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	Debug_assert(DEBUG_ALWAYS, file != NULL, "FILE == NULL\n");
	if (this == NULL) {
		fprintf(file, "NULL");
	} else {
		fprintf(file, "[%d, %d]/%d", this->min, this->max, this->step);
	}
}
void Range_Int_toString(const Range_Int* this, char* result, size_t maxlen) {
	Debug_assert(DEBUG_ALWAYS, result != NULL, "result == NULL\n");
	if (this == NULL) {
		snprintf(result, maxlen, "NULL");
	} else {
		snprintf(result, maxlen, "[%d, %d]/%d", this->min, this->max, this->step);
	}
}


Range_Int* Range_Int_new_fromString(const char* string) {
	Debug_assert(DEBUG_ALWAYS, string != NULL, "string == NULL\n");
	int min = 0;
	int max = 0;
	int step = 1;
	int tokens = sscanf(string, "%d", &min);
	if (tokens == 1) {
		max = min;
	} else {
		sscanf(string, "[%d, %d]/%d", &min, &max, &step);
		if (max < min) max = min;
	}
	return Range_Int_new(min, max, step);
}

int Range_Int_size(const Range_Int* const this) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	return this->size;
}



int Range_Int_min(const Range_Int* const this) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	return this->min;
}
int Range_Int_max(const Range_Int* const this) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	return this->max;
}
int Range_Int_step(const Range_Int* const this) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	return this->step;
}
void Range_Int_free(Range_Int** thisP) {
	Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
	Range_Int* this = *thisP;
	if (this == NULL) return;
	free(this);
	*thisP = NULL;
}


int Range_Int_index(const Range_Int* const this, int value, int* indexP) {
	Debug_assert(DEBUG, this != NULL, "this == NULL\n");
	Debug_assert(DEBUG, indexP != NULL, "indexP == NULL\n");
	int i = ( (value-this->min) / this->step);
	if (i * this->step == (value - this->min)) {
		*indexP = i;
		return 0;
	} else {
		return -1;
	}
}
int Range_Int_value(const Range_Int* const this, int index, int* valueP) {
	Debug_assert(DEBUG, this != NULL, "this == NULL\n");
	Debug_assert(DEBUG, valueP != NULL, "valueP == NULL\n");
	if (index < 0 || index >= Range_Int_size(this)) return -1;

	int result = this->min + this->step * index;
	if (result < this->min || result > this->max) {
		return -1;
	}
	*valueP = result;
	return 0;
}


int Range_Int_contains(const Range_Int* const this, int value) {
	if (value < this->min) return 0;
	if (value > this->max) return 0;
	int diff = value - this->min;
	return (diff % this->step == 0);
}



struct Range_Int_Iterator {
	const Range_Int* range;

	int hasNext;
	int next;

	int hasPrev;
	int prev;

	int step;
	int looping;

	// cache
	int greatest;

};

Range_Int_Iterator* Range_Int_Iterator_new(const Range_Int* const range) {
	Debug_assert(DEBUG_ALWAYS, range != NULL, "range == NULL");
	Range_Int_Iterator* new = calloc(1, sizeof(Range_Int_Iterator));
	Debug_assert(DEBUG_ALWAYS, new != NULL, "new == NULL");
	new->range = range;
	new->step = range->step;
	new->looping = 0;
	new->greatest = Range_Int_greatest(range);
	Range_Int_Iterator_rewind(new);
	return new;
}
Range_Int_Iterator* Range_Int_Iterator_new_looping(const Range_Int* const range) {
	Range_Int_Iterator* new = Range_Int_Iterator_new(range);
	new->looping = 1;
	Range_Int_Iterator_rewind(new);
	return new;
}

void Range_Int_Iterator_rewind(Range_Int_Iterator* const this) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	this->hasNext = 1;
	this->next = this->range->min;

	this->hasPrev = 1;
	this->prev = this->greatest;
}


void Range_Int_Iterator_setStep(Range_Int_Iterator* const this, int step) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	Debug_assert(DEBUG_ALWAYS, step > 0, "step = %d <= 0\n", step);
	Debug_assert(DEBUG_ALWAYS, step <= this->range->max - this->range->min+1,
		"step = %d > max-min+1 = %d '\n", step, this->range->max - this->range->min + 1);
	this->step = step;
	this->greatest = _Range_Int_greatest(this->range, step);
}

int Range_Int_Iterator_hasNext(Range_Int_Iterator* const this) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	return this->looping || this->hasNext;
}
int Range_Int_Iterator_hasPrev(Range_Int_Iterator* const this) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	return this->looping || this->hasPrev;
}


void Range_Int_Iterator_fprint(const void* const this_v, FILE* f) {
	Range_Int_Iterator* const this = (Range_Int_Iterator*)this_v;
	fprintf(f, "[state: hasPrev=%d, prev=%d, hasNext=%d, next=%d] ",
		this->hasPrev, this->prev,
		this->hasNext, this->next);
}

int _Range_Int_Iterator_computeNext(Range_Int_Iterator* this, int curr) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	this->hasNext = 1;
	int result = curr + this->step;
	if (result > this->range->max) {
		// too large:
		if (this->looping) {
			// loop
			int delta = result - this->range->max; // delta > 0
			result = (this->range->min + (delta - 1));
			Debug_assert(DEBUG_ALWAYS, result <= this->range->max, "Error!! computeNext(curr=%d): result=%d > max = %d\n", curr, result, this->range->max);
		} else {
			this->hasNext = 0;
		}
	}
	return result;
}

int _Range_Int_Iterator_computePrev(Range_Int_Iterator* this, int curr) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	this->hasPrev = 1;
	int result = curr - this->step;
	if (result < this->range->min) {
		// too small:
		if (this->looping) {
			// loop
			int delta = this->range->min - result; // delta > 0
			result = (this->range->max - (delta - 1));
			Debug_assert(DEBUG_ALWAYS, result >= this->range->min, "Error!! computePrev(curr=%d): result=%d < min = %d\n", curr, result, this->range->min);
		} else {
			this->hasPrev = 0;
		}
	}
	return result;
}

int Range_Int_Iterator_next(Range_Int_Iterator* const this, int* result) {
	if (!Range_Int_Iterator_hasNext(this)) return 0;
	Debug_assert(DEBUG_ALWAYS, result != NULL, "result == NULL\n");

	*result = this->next;
	this->next = _Range_Int_Iterator_computeNext(this, *result);
	this->prev = _Range_Int_Iterator_computePrev(this, *result);
	return 1;
}
int Range_Int_Iterator_prev(Range_Int_Iterator* const this, int* result) {
	if (!Range_Int_Iterator_hasPrev(this)) return 0;
	Debug_assert(DEBUG_ALWAYS, result != NULL, "result == NULL\n");

	*result = this->prev;
	this->next = _Range_Int_Iterator_computeNext(this, *result);
	this->prev = _Range_Int_Iterator_computePrev(this, *result);
	return 1;
}

void Range_Int_Iterator_free(Range_Int_Iterator** thisP) {
	Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
	Range_Int_Iterator* this = *thisP;
	if (this == NULL) return;
	free(this);
	*thisP = NULL;
}



