/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>

#include <Debug.h>
#include <BasicTypeValue.h>
#include <Numbers.h>

#define DEBUG "BasicTypeValue"

void BasicTypeValue_fprint(const void* const elem, FILE* f) {
	if (elem == NULL) {
		fprintf(f, "<NULL>");
	} else {
		BasicTypeValue btvalue = *((BasicTypeValue*)elem);
		switch (btvalue.type) {
			case BASIC_TYPE_INT :
				fprintf(f, "%d", btvalue.value.i);
				break;
			case BASIC_TYPE_LONG :
				fprintf(f, "%ld", btvalue.value.l);
				break;
			case BASIC_TYPE_ULONG :
				fprintf(f, "%lu", btvalue.value.ul);
				break;
			case BASIC_TYPE_ULONGLONG :
				fprintf(f, "%llu", btvalue.value.ull);
				break;
			case BASIC_TYPE_DOUBLE :
				fprintf(f, "%lf", btvalue.value.d);
				break;
			case BASIC_TYPE_CHAR :
				fprintf(f, "%c", btvalue.value.c);
				break;
			default :
				Debug_assert(DEBUG_ALWAYS, 0, "Type '%d' btvalue.type unknown\n", btvalue.type);
		}
	}
}

int BasicTypeValue_equals(const void* const e1, const void* const e2) {
	if (e1 == NULL) return (e2 == NULL);
	if (e2 == NULL) return 0;

	BasicTypeValue btvalue1 = *((BasicTypeValue*)e1);
	BasicTypeValue btvalue2 = *((BasicTypeValue*)e2);
	if (btvalue1.type != btvalue2.type) return 0;
	switch (btvalue1.type) {
		case BASIC_TYPE_INT :
			return (btvalue1.value.i == btvalue2.value.i);
		case BASIC_TYPE_LONG :
			return (btvalue1.value.l == btvalue2.value.l);
		case BASIC_TYPE_ULONG :
			return (btvalue1.value.ul == btvalue2.value.ul);
		case BASIC_TYPE_ULONGLONG :
			return (btvalue1.value.ull == btvalue2.value.ull);
		case BASIC_TYPE_DOUBLE :
			return (Numbers_approxEQ(btvalue1.value.d, btvalue2.value.d));
		case BASIC_TYPE_CHAR :
			return (btvalue1.value.c == btvalue2.value.c);
	}
	Debug_assert(DEBUG_ALWAYS, 0, "Type '%d' btvalue1.type = btvalue2.type unknown\n", btvalue1.type);
	return 0;
}


size_t BasicType_sizeof(BasicType t) {
	switch(t) {
		case BASIC_TYPE_INT : return sizeof(int);
		case BASIC_TYPE_LONG : return sizeof(long);
		case BASIC_TYPE_ULONG : return sizeof(unsigned long);
		case BASIC_TYPE_ULONGLONG : return sizeof(unsigned long long);
		case BASIC_TYPE_DOUBLE : return sizeof(double);
		case BASIC_TYPE_CHAR : return sizeof(char);
	}
	Debug_assert(DEBUG_ALWAYS, 0, "Unknown BasicType '%d'\n", t);
	return 0;
}


void BasicTypeValue_set(BasicTypeValue* this, BasicType type, void* value_p) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	Debug_assert(DEBUG_ALWAYS, value_p != NULL, "value_p == NULL\n");
	switch(type) {
		case BASIC_TYPE_INT :
			this->value.i = *((int*)value_p);
			break;
		case BASIC_TYPE_LONG :
			this->value.l = *((long*)value_p);
			break;
		case BASIC_TYPE_ULONG :
			this->value.ul = *((unsigned long*)value_p);
			break;
		case BASIC_TYPE_ULONGLONG :
			this->value.ull = *((unsigned long long*)value_p);
			break;
		case BASIC_TYPE_DOUBLE :
			this->value.d = *((double*)value_p);
			break;
		case BASIC_TYPE_CHAR :
			this->value.c = *((char*)value_p);
			break;
		default:
			Debug_assert(DEBUG_ALWAYS, 0, "BasicTypeValue_set(): Type '%d' unknown\n", type);
	}
	this->type = type;
}

void BasicTypeValue_setInt(BasicTypeValue* this, int value) {
	Debug_assert(DEBUG, this != NULL, "this == NULL\n");
	this->type = BASIC_TYPE_INT;
	this->value.i = value;
}
void BasicTypeValue_setLong(BasicTypeValue* this, long value) {
	Debug_assert(DEBUG, this != NULL, "this == NULL\n");
	this->type = BASIC_TYPE_LONG;
	this->value.l = value;
}
void BasicTypeValue_setULong(BasicTypeValue* this, unsigned long value) {
	Debug_assert(DEBUG, this != NULL, "this == NULL\n");
	this->type = BASIC_TYPE_ULONG;
	this->value.ul = value;
}
void BasicTypeValue_setULongLong(BasicTypeValue* this, unsigned long long value) {
	Debug_assert(DEBUG, this != NULL, "this == NULL\n");
	this->type = BASIC_TYPE_ULONGLONG;
	this->value.ull = value;
}
void BasicTypeValue_setDouble(BasicTypeValue* this, double value) {
	Debug_assert(DEBUG, this != NULL, "this == NULL\n");
	this->type = BASIC_TYPE_DOUBLE;
	this->value.d = value;
}
void BasicTypeValue_setChar(BasicTypeValue* this, char value) {
	Debug_assert(DEBUG, this != NULL, "this == NULL\n");
	this->type = BASIC_TYPE_CHAR;
	this->value.c = value;
}

