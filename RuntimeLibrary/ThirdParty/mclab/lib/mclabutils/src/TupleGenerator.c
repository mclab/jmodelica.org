/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>

#include <Debug.h>
#include <Array.h>
#include <Numbers.h>
#include <HashSet.h>

#include <TupleGenerator.h>

#define DEBUG_THIS "TupleGenerator"

typedef struct {
	double min;
	double step;
	double max;
} TupleGeneratorElem;

struct TupleGenerator_Iterator {
	const TupleGenerator* gen;
	TupleGenerator_Iterator_Type iteratorType;
	int hasNext;
	Array* next; // Array<double>: next   double only????
};


struct TupleGenerator {
	Array* space; // Array<TupleGeneratorElem>
	HashSet* iterators; // HashSet<TupleGenerator_Iterator*>
};

void TupleGenerator_free(TupleGenerator** thisP) {
	Debug_assert(DEBUG_THIS, thisP != NULL, "thisP == NULL\n");
	TupleGenerator* this = *thisP;
	if (this == NULL) return;
	Debug_assert(DEBUG_THIS, HashSet_size(this->iterators) == 0, "TupleGenerator %p still has %ld iterators\n", (void*)this, HashSet_size(this->iterators));
	Array_free(&this->space);
	HashSet_free(&this->iterators);
	free(this);
	*thisP = NULL;
}

TupleGenerator* TupleGenerator_new() {
	TupleGenerator* new = (TupleGenerator*)calloc(1, sizeof(*new));
	Debug_assert(DEBUG_THIS, new != NULL, "new == NULL\n");
	new->space = Array_new(10, sizeof(TupleGeneratorElem), 10, NULL, NULL);
	Debug_assert(DEBUG_THIS, new->space != NULL, "new->space == NULL\n");

	new->iterators = HashSet_new(sizeof(TupleGenerator_Iterator*));
	Debug_assert(DEBUG_THIS, new->iterators != NULL, "new->iterators == NULL\n");

	return new;
}

int TupleGenerator_addElem(TupleGenerator* this, double min, double max, double step) {
	Debug_assert(DEBUG_THIS, this != NULL, "this == NULL\n");
	Debug_assert(DEBUG_THIS, !(max < min), "max < min\n");

	if (HashSet_size(this->iterators) > 0) return -1;

	TupleGeneratorElem newElem = {
		.min = min,
		.step = step,
		.max = max
	};
	Array_add(this->space, &newElem);

	return 0;
}

int TupleGenerator_addElem_n_times(TupleGenerator* this, unsigned n, double min, double max, double step) {
	int err = 0;
	for (unsigned i = 0; i < n; i++) {
		err -= TupleGenerator_addElem(this, min, max, step);
		if (err) break;
	}
	return err;
}

void TupleGenerator_Iterator_free(TupleGenerator_Iterator** thisP) {
	Debug_assert(DEBUG_THIS, thisP != NULL, "thisP == NULL\n");
	TupleGenerator_Iterator* this = *thisP;
	if (this == NULL) return;

	HashSetIterator* it = HashSetIterator_new(this->gen->iterators);
	const void* elem = NULL;
	Debug_out(DEBUG_THIS, "Registered iterators (%ld)\n",
		HashSet_size(this->gen->iterators));
	while ( (elem = HashSetIterator_next(it)) != NULL ) {
		Debug_assert(DEBUG_THIS, HashSet_contains(this->gen->iterators, elem), "!!!\n");
		Debug_out(DEBUG_THIS, " - %p\n", elem);
	}
	Debug_out(DEBUG_THIS, "end.\n");
	HashSetIterator_free(&it);

	Debug_assert(DEBUG_THIS, HashSet_contains(this->gen->iterators, &this), "this->gen->iterators (size = %ld) does not contain iterator %p\n",
	              HashSet_size(this->gen->iterators), (void*)this);
	HashSet_remove(this->gen->iterators, &this);
	Array_free(&this->next);
	free(this);
	*thisP = NULL;
}

int _TupleGenerator_Iterator_rewind_all(TupleGenerator_Iterator* this, unsigned fromIdx) {
	Debug_assert(DEBUG_THIS, this != NULL, "this == NULL\n");
	Debug_assert(DEBUG_THIS, this->next != NULL, "this->next == NULL\n");

	this->hasNext = 1;
	Debug_out(DEBUG_THIS, "Rewinding iterator of type: ALL\n");
	for (unsigned i = fromIdx; i < Array_length(this->gen->space); i++) {
		TupleGeneratorElem elem = { .min = 0, .step = 0, .max = 0 };
		Debug_out(DEBUG_THIS, " - %d-th element\n", i);
		Array_get(this->gen->space, i, &elem);
		Debug_out(DEBUG_THIS, "    (min=%lf, max=%lf, step=%lf)\n", elem.min, elem.max, elem.step);
		Array_set(this->next, i, &(elem.min));
		Debug_out(DEBUG_THIS, "    -> elem set to %lf\n", elem.min);
	}
	Debug_assert(DEBUG_THIS, Array_length(this->next) == Array_length(this->gen->space), "|this->next| = %d != |this->gen->space| = %d\n",
	           Array_length(this->next), Array_length(this->gen->space));
	return 0;
}


int _TupleGenerator_Iterator_rewind_alldiff_asc(TupleGenerator_Iterator* this, unsigned fromIdx) {
	Debug_assert(DEBUG_THIS, this != NULL, "this == NULL\n");

	this->hasNext = 1;
	double value = 0;

	int first = 1;
	if (fromIdx > 0) {
		Array_get(this->next, fromIdx-1, &value);
		first = 0;
	}
	for(unsigned i = fromIdx; i < Array_length(this->gen->space); i++) {

		double valueLastElem = value;
		TupleGeneratorElem tg_elem = { .min = 0, .step = 0, .max = 0};
		Array_get(this->gen->space, i, &tg_elem);

		value = tg_elem.min;

		if (first) {
			first = 0;
		} else {
			if (Numbers_approxLE(value, valueLastElem)) {
				// Jump close to new value
				value += (tg_elem.step) *
					(1+floor((valueLastElem - value)/tg_elem.step));

				// as floor() may have "noise", check value and possibly adjust it
				while(Numbers_approxLE(value, valueLastElem)) {
					value += tg_elem.step;
				}
				while(
				      // domain for i-th elem allows decreasing
				      Numbers_approxGE(value - tg_elem.step, tg_elem.min)
					  &&
					  // decreasing i-th elem would keep ASC order
				      Numbers_approxG(value - tg_elem.step, valueLastElem)) {

					value -= tg_elem.step;
				}
			}
			Debug_assert(DEBUG_THIS, Numbers_approxGE(value, tg_elem.min), "value = %lf < tg_elem.min = %lf\n", value, tg_elem.min);

			Debug_assert(DEBUG_THIS, Numbers_approxG(value, valueLastElem),
				"Error: i=%d, value = %lf <= valueLastElem = %lf\n",
				i, value, valueLastElem);
			Debug_assert(DEBUG_THIS, Numbers_approxLE(value - tg_elem.step, valueLastElem), "Error: i=%d, value - tg_elem.step = %lf - %lf = %f > valueLastElem = %lf\n",
					i,
					value, tg_elem.step,
					value - tg_elem.step,
					valueLastElem);
		}
		if (value > tg_elem.max) {
			this->hasNext = 0;
			Debug_out(DEBUG_THIS, "Warning: tuple too long for range\n");
			return -1;
		} else {
			Array_set(this->next, i, &value);
		}

	}
	return 0;
}

void TupleGenerator_Iterator_rewind(TupleGenerator_Iterator* this) {
	Debug_assert(DEBUG_THIS, this != NULL, "this == NULL\n");
	int err = 0;
	Debug_out(DEBUG_THIS, "Rewinding iterator\n");
	switch(this->iteratorType) {
		case TUPLEGENERATOR_ITERATOR_ALL :
			Debug_out(DEBUG_THIS, "  -> type: ALL\n");
			err -= _TupleGenerator_Iterator_rewind_all(this, 0);
			break;
		case TUPLEGENERATOR_ITERATOR_ALLDIFF_ASC:
			Debug_out(DEBUG_THIS, "  -> type: ALLDIFF_ASC\n");
			err -= _TupleGenerator_Iterator_rewind_alldiff_asc(this, 0);
			break;
		default:
			Debug_assert(DEBUG_THIS, 0, "Error: TupleGenerator_Iterator_type unknown: %d\n", this->iteratorType);
			err--;
	}
	Debug_assert(DEBUG_THIS, !err, "Error: err = %d\n", err);
}


TupleGenerator_Iterator* TupleGenerator_Iterator_new(TupleGenerator* gen, TupleGenerator_Iterator_Type type) {
	Debug_assert(DEBUG_THIS, gen != NULL, "gen == NULL\n");
	TupleGenerator_Iterator* new = (TupleGenerator_Iterator*)calloc(1, sizeof(*new));
	Debug_assert(DEBUG_THIS, new != NULL, "new == NULL\n");
	HashSet_add(gen->iterators, &new);

	unsigned length = Array_length(gen->space);
	new->gen = gen;
	new->iteratorType = type;
	new->next = Array_newDouble(length, 1);
	Debug_assert(DEBUG_THIS, new->next != NULL, "new->next == NULL\n");

	TupleGenerator_Iterator_rewind(new);
	Debug_out(DEBUG_THIS, "TupleGenerator_Iterator %p created\n", (void*)new);
	return new;
}

int TupleGenerator_Iterator_hasNext(TupleGenerator_Iterator* this) {
	Debug_assert(DEBUG_THIS, this != NULL, "this == NULL\n");
	return this->hasNext;
}




int _TupleGenerator_Iterator_next_all(TupleGenerator_Iterator* this, Array* result) {

	if (!this->hasNext) return 0;

	Array_clear(result);
	Array_copy(this->next, result);

	// Advance 'next'
	Debug_out(DEBUG_THIS, "Advancing next (having length %d):\n", Array_length(this->next));
	// 1. Find right-most index 'r' which can be incremented
	Debug_out(DEBUG_THIS, " - Finding right-most index r which can be incremented\n");
	unsigned r = Array_length(this->next); // initialisation to illegal value
	double value = 0;
	int found = 0;
	unsigned i = Array_length(this->next)-1;
	while (!found) {
		Array_get(this->next, i, &value);
		Debug_out(DEBUG_THIS, "   - Trying i=%d: this->next[i] = %lf\n", i, value);
		// Try to increase it
		TupleGeneratorElem tg_elem = {.min = 0, .step = 0, .max = 0};
		Array_get(this->gen->space, i, &tg_elem);
		value += tg_elem.step;
		Debug_out(DEBUG_THIS, "      - Trying to increase it by %lf: %lf\n", tg_elem.step, value);
		if (value >
		    tg_elem.max) {
			Debug_out(DEBUG_THIS, "        - too large\n");
		} else {
			// found
			r = i;
			found = 1;
			Debug_out(DEBUG_THIS, "        - OK!\n");
		}
		if (i==0) break;
		else i--;
	}

	if (!found) {
		// Cannot advance next
		Debug_out(DEBUG_THIS, " - Cannot advance this->next, this is the last call to next()\n");
		this->hasNext = 0;
	} else {
		// 2. Set (already incremented) i-th value in array
		Debug_out(DEBUG_THIS, " - Setting r=%d-th elem to %lf\n", r, value);
		Array_set(this->next, r, &value);

		// 3. Set all j-th values (j>i) to their minimum
		_TupleGenerator_Iterator_rewind_all(this, r+1);
	}
	Debug_out(DEBUG_THIS, "done.\n");
	return 1;
}



int _TupleGenerator_Iterator_next_alldiff_asc(TupleGenerator_Iterator* this, Array* result) {

	if (!this->hasNext) return 0;

	Array_clear(result);
	Array_copy(this->next, result);

	// Advance 'next'
	Debug_out(DEBUG_THIS, "Advancing next (having length %d):\n", Array_length(this->next));
	// 1. Find right-most index 'r' which can be incremented
	Debug_out(DEBUG_THIS, " - Finding right-most index r which can be incremented\n");
	unsigned r = Array_length(this->next); // initialisation to illegal value
	double value = 0;
	int found = 0;
	unsigned i = Array_length(this->next)-1;
	while (!found) {
		Array_get(this->next, i, &value);
		Debug_out(DEBUG_THIS, "   - Trying i=%d: this->next[i] = %lf\n", i, value);
		// Try to increase it
		TupleGeneratorElem tg_elem = {.min = 0, .step = 0, .max = 0};
		Array_get(this->gen->space, i, &tg_elem);
		value += tg_elem.step;
		Debug_out(DEBUG_THIS, "      - Trying to increase it by %lf: %lf\n", tg_elem.step, value);

		if (value >
		    tg_elem.max) {
			// try to the left
			Debug_out(DEBUG_THIS, "        - too large\n");
		} else {
			// found
			r = i;
			found = 1;
			Debug_out(DEBUG_THIS, "        - OK!\n");
		}
		if (i==0) break;
		else i--;
	}
	if (!found) {
		// Cannot advance next
		this->hasNext = 0;
		Debug_out(DEBUG_THIS, " - Cannot advance this->next, this is the last call to next()\n");
	} else {
		// 2. Set (already incremented) i-th value in array
		Debug_out(DEBUG_THIS, " - Setting r=%d-th elem to %lf\n", r, value);
		Array_set(this->next, r, &value);

		// 3. Set all j-th values (j>i) to asc values ensuring ASC order, if possible
		_TupleGenerator_Iterator_rewind_alldiff_asc(this, r+1);
	}
	return 1;
}


/* Fills result (Array<double>) with next tuple.
	Returns 1 in case of success and 0 in case this has no next tuple.
*/
int TupleGenerator_Iterator_next(TupleGenerator_Iterator* this, Array* result) {
	Debug_assert(DEBUG_THIS, this != NULL, "this == NULL\n");
	Debug_assert(DEBUG_THIS, result != NULL, "result == NULL\n");
	Debug_assert(DEBUG_THIS, Array_elemSize(result) == sizeof(double), "result->elemSize = %ld != sizeof(double) = %ld\n",
	             Array_elemSize(result), sizeof(double));
	int hasNext = 0;
	switch(this->iteratorType) {
		case TUPLEGENERATOR_ITERATOR_ALL :
			Debug_out(DEBUG_THIS, "Calling next_all()\n");
			hasNext = _TupleGenerator_Iterator_next_all(this, result);
			break;
		case TUPLEGENERATOR_ITERATOR_ALLDIFF_ASC:
		Debug_out(DEBUG_THIS, "Calling next_alldiff_asc()\n");
			hasNext = _TupleGenerator_Iterator_next_alldiff_asc(this, result);
			break;
		default:
			Debug_assert(DEBUG_THIS, 0, "Error: TupleGenerator_Iterator_type unknown: %d\n", this->iteratorType);
			hasNext = 0;
	}
	return hasNext;
}
