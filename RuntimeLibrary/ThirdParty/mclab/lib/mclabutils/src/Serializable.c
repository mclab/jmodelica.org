/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <Array.h>
#include <Debug.h>
#include <HashSet.h>
#include <Serializable.h>
#include <StringUtils.h>

#define DEBUG "Serializable"
#define DEBUG_VERBOSE "Serializable Verbose"
#define SERIALIZABLE_FIELD_LEN 1024
#define SERIALIZABLE_ARRAY_FIELD_LEN 100
#define SERIALIZABLE_ID_LEN 100
#define SERIALIZABLE_ID "__Serializable_id__"
#define SERIALIZABLE_CLASS "__Class__"
#define SERIALIZABLE_CLASS_ARRAY "Array"
#define SERIALIZABLE_REF "__Serializable__reference__"
#define SERIALIZABLE_PENDING_POINTERS_CAPACITY 1

#define ELEM_PTR(a, i, sz) ((void *)(((char *)a) + ((i) * (sz))))

typedef struct {
  Serializable **pointer;
  const char *id;
} Reference;

static Properties *arrayPtrs = NULL;

static void Serializable_serializeInto_aux(Serializable *this_,
                                           Properties *result,
                                           HashSet *savedObjects, int level);
static Reference *Reference_new(Serializable **pointer, const char *id);
static void Reference_free(Reference **const thisP);
static void Reference_fprint(const void *const elemP, FILE *f);
static void Reference_fprint_pp(const void *const vpp, FILE *f);
static void Serializable_deserializeFrom_aux(Serializable *parent,
                                             Serializable *this_,
                                             const Properties *serialized,
                                             Properties *loadedObjects,
                                             Array *pendingPointers);
static void Serializable_free_aux(Serializable **thisPtr,
                                  HashSet *freedObjects);
static void Serializable_clearArrayPtrs();
static const char *Serializable_serializeArray_className(void *obj);
static void Serializable_serializeArray_serializeFields(void *obj);
static Serializable *
Serializable_serializeArray(void *array, size_t elem_size, size_t const *sizes,
                            Properties_Content_Type type, toSerializable f,
                            unsigned int depth, bool elem_is_a_struct,
                            reallocArray r);
static void Serializable_deserializeArray(Serializable *serArray, void *array,
                                          size_t elem_size, size_t *sizes,
                                          unsigned int depth);

struct Serializable {
  void *subInstance;
  Properties *fields; // of attributeName -> Serializable_Field)
  SerializableMethods methods;
};

Serializable *Serializable_new(void *subInstance, SerializableMethods methods) {
  Debug_assert(DEBUG_ALWAYS, arrayPtrs != NULL,
               "Error: You forgot to initialize Serializable framework.");
  Serializable *new = (Serializable *)calloc(1, sizeof(Serializable));
  Debug_assert(DEBUG_ALWAYS, new != NULL, "Error: new == NULL\n");
  new->subInstance = subInstance;
  Debug_assert(DEBUG_ALWAYS, methods.className != NULL,
               "methods.className == NULL");
  Debug_assert(DEBUG_ALWAYS, methods.serializeFields != NULL,
               "methods.serializeFields == NULL");
  Debug_assert(DEBUG_ALWAYS, methods.deserializeFields != NULL,
               "methods.deserializeFields == NULL");
  new->methods = methods;
  new->fields = Properties_new();
  Debug_assert(DEBUG_ALWAYS, new->fields != NULL,
               "Error: new->fields == NULL\n");
  return new;
}

/*PROTECTED*/ void Serializable_setField(Serializable *this_, const char *name,
                                         Properties_Content_Type type,
                                         void *valueP, const void *passport) {
  Debug_assert(DEBUG_ALWAYS, name != NULL, "Error: name == NULL");
  Debug_assert(DEBUG_ALWAYS, valueP != NULL, "Error: valueP == NULL");
  Debug_assert(DEBUG_ALWAYS, passport != NULL, "Error: passport == NULL");
  Debug_assert(DEBUG_ALWAYS, passport == this_->methods.passport,
               "Error: Permission denied");
  Properties_set(this_->fields, name, type, valueP);
  Debug_out(DEBUG_VERBOSE,
            "Info: Serializable fields of object of class_ %s:\n",
            this_->methods.className(this_->subInstance));
  Debug_perform(DEBUG_VERBOSE, Properties_fprint(this_->fields, stderr));
}

/*PROTECTED*/ const void *Serializable_getField(Serializable *this_, char *attr,
                                                const void *passport) {
  Debug_assert(DEBUG_ALWAYS, this_ != NULL, "Error: this_ == NULL\n");
  Debug_assert(DEBUG_ALWAYS, this_->methods.passport == passport,
               "Error: this_->methods.passport != passport\n");
  const Properties_Content *content = Properties_get(this_->fields, attr);
  Debug_assert(DEBUG_ALWAYS, content != NULL,
               "Error: this_ object does not contain field %s\n", attr);
  const void *value = Properties_Content_value(content);
  return value;
}

void Serializable_changeSubInstance(Serializable *this_, void *subInstance,
                                    const void *passport) {
  Debug_assert(DEBUG_ALWAYS, this_ != NULL, "Error: this_ == NULL\n");
  Debug_assert(DEBUG_ALWAYS, subInstance != NULL,
               "Error: subInstance == NULL\n");
  Debug_assert(DEBUG_ALWAYS, passport != NULL, "Error: passport == NULL\n");
  Debug_assert(DEBUG_ALWAYS, this_->methods.passport == passport,
               "Error: this_->methods.passport != passport\n");
  this_->subInstance = subInstance;
  Debug_out(
      DEBUG,
      "Warning: Serializable subInstace %p of %p has been changed with %p\n",
      this_->subInstance, (void *)this_, subInstance);
}

typedef struct SerializableStackEntry {
  Serializable *s;
  Properties *p;
} SerializableStackEntry;
static void Serializable_serializeInto_aux(Serializable *this_,
                                           Properties *result,
                                           HashSet *savedObjects, int level) {
  Debug_assert(DEBUG_ALWAYS, this_ != NULL, "Error: this_ == NULL\n");
  Debug_assert(DEBUG_ALWAYS, result != NULL, "Error: result == NULL\n");
  Debug_assert(DEBUG_ALWAYS, savedObjects != NULL,
               "Error: savedObjects == NULL\n");
  Array *stack = Array_new(1000, sizeof(SerializableStackEntry), 500, NULL, NULL);
  SerializableStackEntry e = {.s = this_, .p = result};
  Array_push(stack, &e);
  while(Array_length(stack) > 0) {
    Array_pop(stack, &e);
    Debug_outNest(DEBUG);
    Debug_out(DEBUG, "Info: Ask the subInstance %p which fields to serialize\n",
              e.s->subInstance);
    e.s->methods.serializeFields(e.s->subInstance);
    Properties_Iterator *iterator = Properties_Iterator_new(e.s->fields);
    const Properties_Entry *field = NULL;
    while ((field = Properties_Iterator_next(iterator)) != NULL) {
      const char *field_name = Properties_Entry_property(field);
      const Properties_Content *content = Properties_Entry_value(field);
      const void *field_value = Properties_Content_value(content);
      Properties_Content_Type field_type = Properties_Content_type(content);
      if (field_type != PROPERTIES_POINTER) { // attr is a data field
        Debug_out(DEBUG, "Info: %s is a data field\n", field_name);
        Properties_set(e.p, field_name, field_type, field_value);
      } else { // attr is an object
        Serializable *child = (Serializable *)field_value;
        char child_id[SERIALIZABLE_ID_LEN] = {0};
        sprintf(child_id, "%p", field_value);
        Properties *nested = NULL;
        if (Properties_getNestedProperties(e.p, field_name, &nested) < 0) {
          nested = Properties_new();
          Properties_setNestedProperties(e.p, field_name, nested);
          Properties_free(&nested);
          Properties_getNestedProperties(e.p, field_name, &nested);
        }
        if (HashSet_contains(savedObjects, &child)) {
          Properties_setString(nested, SERIALIZABLE_REF, child_id);
          Debug_out(DEBUG, "Info: %s is an already serialized object of class_ "
                           "%s with id %s\n",
                    field_name, child->methods.className(child->subInstance),
                    child_id);
          /* if (nested_to_be_freed == true) { */
          /*   Properties_setNestedProperties(result, field_name, nested); */
          /* } */
        } else {
          HashSet_add(savedObjects, &child);
          Properties_setString(nested, SERIALIZABLE_CLASS,
                               child->methods.className(child->subInstance));
          Properties_setString(nested, SERIALIZABLE_ID, child_id);
          Debug_out(
              DEBUG,
              "Info: %s is a serialiazable object of class_ %s with id %s \n",
              field_name, child->methods.className(child->subInstance), child_id);
          /* Timer_start(child_id); */
          SerializableStackEntry e_child = {.s = child, .p = nested};
          Array_push(stack, &e_child);
          /* Serializable_serializeInto_aux(child, nested, savedObjects, level+1); */
          /* fprintf(stderr, "%d ser recursive call %s:%s time: %llums\n", level, field_name, child->methods.className(child->subInstance), Timer_end(child_id)); */
          /* if (nested_to_be_freed == true) { */
          /*   Timer_start(field_name); */
          /*   Properties_setNestedProperties(result, field_name, nested); */
          /*   fprintf(stderr, "%d ser setNestedProperties %s:%s time: %llums\n", level, field_name, child->methods.className(child->subInstance), Timer_end(field_name)); */
          /* } */
        }
        /* if (nested_to_be_freed) { */
        /*   Properties **nestedPtr = (Properties **)&nested; */
        /*   Properties_free(nestedPtr); */
        /* } */
      }
      Debug_out(DEBUG_VERBOSE, "Info: Result of serialization:\n");
      Debug_perform(DEBUG_VERBOSE, Properties_fprint(e.p, stderr));
    }
    Properties_Iterator_free(&iterator);
    Debug_outUnnest(DEBUG);
  }
  Array_free(&stack);
}

void Serializable_serializeInto(Serializable *this_, Properties *serialized) {
  Debug_assert(DEBUG_ALWAYS, this_ != NULL, "Error: this_ == NULL\n");
  Debug_assert(DEBUG_ALWAYS, serialized != NULL, "Error: serialized == NULL\n");

  Debug_out(DEBUG, "Info: Serialize %p\n", (void *)this_);
  HashSet *savedObjects =
      HashSet_new(sizeof(Serializable *)); // HashSet_new<Serializable*>();
  void *this_v = (void *)this_;
  char this_id[SERIALIZABLE_ID_LEN] = {0};
  sprintf(this_id, "%p", this_v);
  Properties_setString(serialized, SERIALIZABLE_CLASS,
                       this_->methods.className(this_->subInstance));
  Properties_setString(serialized, SERIALIZABLE_ID, this_id);
  HashSet_add(savedObjects, &this_);
  Timer_start("serialize time");
  Serializable_serializeInto_aux(this_, serialized, savedObjects, 0);
  /* fprintf(stderr, "Serialize time: %llu\n", Timer_end("serialize time")); */
  HashSet_free(&savedObjects);
  Debug_out(DEBUG, "Info: Done serialization of %p\n", (void *)this_);
}

static Reference *Reference_new(Serializable **pointer, const char *id) {
  Reference *new = calloc(1, sizeof(Reference));
  Debug_assert(DEBUG_ALWAYS, new != NULL, "Error: new == NULL\n");
  Serializable **this_p = calloc(1, sizeof(Serializable *));
  Debug_assert(DEBUG_ALWAYS, this_p != NULL, "Error: this_p == NULL\n");
  memcpy(this_p, pointer, sizeof(Serializable *));
  new->pointer = this_p;
  new->id = StringUtils_clone(id);
  return new;
}

static void Reference_free(Reference **const thisP) {
  Reference *this_ = *thisP;
  free((char *)this_->id);
  free(this_->pointer);
  free(this_);
  *thisP = NULL;
}

static void Reference_fprint(const void *const elemP, FILE *f) {
  Reference *r = (Reference *)elemP;
  if (r == NULL) {
    fprintf(f, "(%p)", (void *)r);
  } else {
    fprintf(f, "[pointer: %p, *pointer: %p, id: '%s']\n", (void *)r->pointer,
            *(void **)r->pointer, r->id);
  }
}

static void Reference_fprint_pp(const void *const vpp, FILE *f) {
  Debug_assert(DEBUG_ALWAYS, vpp != NULL, "Error: vpp == NULL\n");
  Reference **pp = (Reference **)vpp;
  Reference_fprint(*pp, f);
}

static void Serializable_deserializeFrom_aux(Serializable *parent,
                                             Serializable *this_,
                                             const Properties *serialized,
                                             Properties *loadedObjects,
                                             Array *pendingPointers) {
  Debug_assert(DEBUG_ALWAYS, this_ != NULL, "Error: this_ == NULL\n");
  Debug_assert(DEBUG_ALWAYS, serialized != NULL, "Error: serialized == NULL\n");
  Debug_assert(DEBUG_ALWAYS, loadedObjects != NULL,
               "Error: loadedObjects == NULL\n");
  Debug_outNest(DEBUG);
  Debug_out(DEBUG, "Info: Starting deserialization for %p\n", (void *)this_);
  const char *this_id = NULL;
  Properties_getString(serialized, SERIALIZABLE_ID, &this_id);
  if (this_id == NULL) {
    Properties_getString(serialized, SERIALIZABLE_REF, &this_id);
    Debug_assert(DEBUG_ALWAYS, this_id != NULL, "Error: this_id == NULL\n");
    const void *this_ptr = NULL;
    if (Properties_getPointer(loadedObjects, this_id, &this_ptr) < 0) {
      Reference *ref = Reference_new(&this_, this_id);
      Debug_out(
          DEBUG,
          "Info: Storing address %p of a not yet deserialized object %s\n",
          (void *)&this_, this_id);
      Array_enqueue(pendingPointers, (const void *const) & ref);
    } else {
      Debug_out(DEBUG,
                "Info: %s is an already loaded object, assigning %p to %p\n",
                this_id, this_ptr, (void *)this_);
      this_ = (Serializable *)this_ptr;
    }
  } else {
    const char *this_class = this_->methods.className(this_->subInstance);
    const char *serialized_class = NULL;
    int err =
        Properties_getString(serialized, SERIALIZABLE_CLASS, &serialized_class);
    Debug_assert(DEBUG_ALWAYS, !err,
                 "Error: Class %s not found in serialized object", this_class);
    Properties_setPointer(loadedObjects, this_id, this_);
    Debug_out(DEBUG, "Info: Add %s = %s with pointer %p to loadedObjects\n",
              SERIALIZABLE_ID, this_id, (void *)this_);
    if (Properties_size(this_->fields) == 0) {
      this_->methods.serializeFields(this_->subInstance);
    }
    if (Properties_size(this_->fields) <
        Properties_size(serialized) - 2) { // class and id fields
      if (StringUtils_equals(serialized_class, SERIALIZABLE_CLASS_ARRAY) &&
          this_->methods.reallocArrayField != NULL) {
        char arrayPtrOldID[SERIALIZABLE_ID_LEN] = {0};
        sprintf(arrayPtrOldID, "%p", this_->subInstance);
        Debug_perform(DEBUG_VERBOSE, Properties_fprint(arrayPtrs, stderr));
        Properties_remove(arrayPtrs, arrayPtrOldID);
        Debug_out(DEBUG_VERBOSE, "Verbose: Remove %s:%p from arrayPtrs\n",
                  arrayPtrOldID, (void *)this_);
        Debug_perform(DEBUG_VERBOSE, Properties_fprint(arrayPtrs, stderr));
        Debug_out(DEBUG,
                  "Warning: Extending size of array %p from %zu to %zu:\n",
                  this_->subInstance, Properties_size(this_->fields),
                  Properties_size(serialized) - 2);
        Debug_out(DEBUG, "         such reallocation could cause NULL pointer "
                         "values if the array %p is referenced by others "
                         "object\n");
        Debug_out(DEBUG, "         So, please be sure that your function for "
                         "reallocating the array will update also pointers to "
                         "this array.\n",
                  this_->subInstance);
        this_->subInstance = this_->methods.reallocArrayField(
            parent->subInstance, Properties_size(serialized) - 2);
        char arrayPtrID[SERIALIZABLE_ID_LEN] = {0};
        sprintf(arrayPtrID, "%p", this_->subInstance);
        Properties_setPointer(arrayPtrs, arrayPtrID, this_);
        Debug_out(DEBUG_VERBOSE, "Verbose: Set %s:%p into arrayPtrs\n",
                  arrayPtrID, (void *)this_);
        Debug_perform(DEBUG_VERBOSE, Properties_fprint(arrayPtrs, stderr));
        parent->methods.serializeFields(parent->subInstance);
      }
    }
    Properties_Iterator *iterator = Properties_Iterator_new(serialized);
    const Properties_Entry *current = NULL;
    while ((current = Properties_Iterator_next(iterator)) != NULL) {
      const char *attr = Properties_Entry_property(current);
      const Properties_Content *current_content =
          Properties_Entry_value(current);
      Properties_Content_Type type = Properties_Content_type(current_content);
      const void *value = Properties_Content_value(current_content);
      if (StringUtils_equals(attr, SERIALIZABLE_ID))
        continue;
      if (StringUtils_equals(attr, SERIALIZABLE_REF))
        continue;
      if (type == PROPERTIES_PROPERTIES) {
        const void *childPtr = NULL;
        Properties_getPointer(this_->fields, attr, &childPtr);
        Serializable *child = (Serializable *)childPtr;
        const char *child_class = child->methods.className(child->subInstance);
        Properties *child_property = NULL;
        Properties_getNestedProperties(serialized, attr, &child_property);
        Debug_out(DEBUG, "Info: deserializing field %s with pointer %p\n", attr,
                  (void *)child);
        if (!StringUtils_equals(this_class, child_class) &&
            StringUtils_equals(child_class, SERIALIZABLE_CLASS_ARRAY)) {
          Serializable_deserializeFrom_aux(this_, child, child_property,
                                           loadedObjects, pendingPointers);
        } else {
          Serializable_deserializeFrom_aux(parent, child, child_property,
                                           loadedObjects, pendingPointers);
        }
      } else {
        Properties_set(this_->fields, attr, type, value);
      }
    }
    Properties_Iterator_free(&iterator);
    Debug_assert(
        DEBUG_ALWAYS, err == 0,
        "Error: Serialized Properties does not contain class_ attribute");
    Debug_assert(DEBUG_ALWAYS, StringUtils_equals(serialized_class, this_class),
                 "Error: class_ %s of this_ is not equal to class_ of "
                 "this_->subInstance %s\n",
                 serialized_class, this_class);
    if (!StringUtils_equals(serialized_class, SERIALIZABLE_CLASS_ARRAY))
      this_->methods.deserializeFields(this_->subInstance);
    Debug_out(DEBUG_VERBOSE, "Info: Deserialized fields:\n");
    Debug_perform(DEBUG_VERBOSE, Properties_fprint(this_->fields, stderr));
  }
  Debug_outUnnest(DEBUG);
}

void Serializable_deserializeFrom(Serializable *this_, Properties *serialized) {
  Debug_assert(DEBUG_ALWAYS, this_ != NULL, "Error: this_ == NULL\n");
  Debug_assert(DEBUG_ALWAYS, serialized != NULL, "Error: serialized == NULL\n");
  Properties *loadedObjects = Properties_new();
  Array *pendingPointers =
      Array_new(SERIALIZABLE_PENDING_POINTERS_CAPACITY, sizeof(Reference *), 5,
                Reference_fprint_pp, NULL);
  Timer_start("deserialize time");
  Serializable_deserializeFrom_aux(NULL, this_, serialized, loadedObjects,
                                   pendingPointers);
  /* fprintf(stderr, "Deserialize time: %llu\n", Timer_end("deserialize time")); */
  void *value = calloc(1, sizeof(Reference **));
  while (Array_length(pendingPointers) > 0) {
    Reference **ref_p = (Reference **)Array_dequeue(pendingPointers, value);
    Reference *ref = *ref_p;
    const void *new_ptr = NULL;
    if (Properties_getPointer(loadedObjects, ref->id, &new_ptr) == 0) {
      Debug_out(DEBUG, "Info: Resolving pending pointer %p of %s %s to %p\n",
                (void *)*ref->pointer, SERIALIZABLE_ID, ref->id,
                (void *)new_ptr);
      *ref->pointer = (Serializable *)new_ptr;
      Reference_free(ref_p);
    } else {
      Debug_out(DEBUG_ALWAYS,
                "Warning: pending pointer %p of %s %s cannot be resolved\n",
                (void *)*ref->pointer, SERIALIZABLE_ID, ref->id);
    }
  }
  free(value);
  Properties_free(&loadedObjects);
  Array_free(&pendingPointers);
}

static void Serializable_free_aux(Serializable **thisPtr,
                                  HashSet *freedObjects) {
  Debug_assert(DEBUG_ALWAYS, thisPtr != NULL, "Error: thisPtr == NULL");
  Debug_assert(DEBUG_ALWAYS, freedObjects != NULL,
               "Error: freedObjects == NULL");
  Serializable *this_ = *thisPtr;
  if (this_ == NULL)
    return;
  if (!HashSet_contains(freedObjects, &this_)) {
    HashSet_add(freedObjects, &this_);
    if (this_->fields != NULL) {
      Properties_Iterator *iterator = Properties_Iterator_new(this_->fields);
      const Properties_Entry *e = NULL;
      while ((e = Properties_Iterator_next(iterator)) != NULL) {
        const Properties_Content *current_content = Properties_Entry_value(e);
        Properties_Content_Type type = Properties_Content_type(current_content);
        if (type == PROPERTIES_POINTER) {
          Serializable *child =
              (Serializable *)Properties_Content_value(current_content);
          Serializable_free_aux(&child, freedObjects);
        }
      }
      Properties_Iterator_free(&iterator);
      Properties_free(&this_->fields);
    }
    free(this_);
    *thisPtr = NULL;
  }
}

void Serializable_free(Serializable **thisPtr) {
  Debug_assert(DEBUG_ALWAYS, thisPtr != NULL, "Error: thisPtr == NULL");
  HashSet *freedObjects = HashSet_new(sizeof(Serializable *));
  Serializable_free_aux(thisPtr, freedObjects);
  HashSet_free(&freedObjects);
}

void Serializable_init() {
  if (arrayPtrs != NULL)
    return;
  arrayPtrs = Properties_new();
  Debug_out(DEBUG, "Info: Initialization of Serializable");
}

void Serializable_finalize() { Properties_free(&arrayPtrs); }

static const char *Serializable_serializeArray_className(void *obj) {
  Serializable *serObj =
      (Serializable *)obj; // for unused parameter warning only
  return SERIALIZABLE_CLASS_ARRAY;
}

static void Serializable_serializeArray_serializeFields(void *obj) {
  Serializable *serObj =
      (Serializable *)obj; // for unused parameter warning only
  return;
}

static void Serializable_serializeArray_deserializeFields(void *obj) {
  Serializable *serObj =
      (Serializable *)obj; // for unused parameter warning only
  return;
}

void Serializable_setArrayField(Serializable *this_, const char *name,
                                Properties_Content_Type type, void *array,
                                size_t elem_size, size_t const *sizes,
                                toSerializable f, unsigned int depth,
                                bool elem_is_a_struct, reallocArray r,
                                const void *passport) {
  Debug_assert(DEBUG_ALWAYS, name != NULL, "Error: name == NULL");
  Debug_assert(DEBUG_ALWAYS, passport != NULL, "Error: passport == NULL");
  Debug_assert(DEBUG_ALWAYS, passport == this_->methods.passport,
               "Error: Permission denied");
  Debug_assert(
      DEBUG_ALWAYS, r == NULL || depth == 0,
      "Error: Reallocation of array is allowed only for one dimension arrays.");
  Properties_set(this_->fields, name, PROPERTIES_POINTER,
                 Serializable_serializeArray(array, elem_size, sizes, type, f,
                                             depth, elem_is_a_struct, r));
  Debug_out(DEBUG_VERBOSE,
            "Info: Serializable fields of object of class_ %s:\n",
            this_->methods.className(this_->subInstance));
  Debug_perform(DEBUG_VERBOSE, Properties_fprint(this_->fields, stderr));
}

static Serializable *
Serializable_serializeArray(void *array, size_t elem_size, size_t const *sizes,
                            Properties_Content_Type type, toSerializable f,
                            unsigned int depth, bool elem_is_a_struct,
                            reallocArray r) {
  Debug_assert(DEBUG_ALWAYS, array != NULL, "Error: array == NULL");
  Debug_assert(DEBUG_ALWAYS, arrayPtrs != NULL, "Error: arrayPtrs == NULL");
  Debug_assert(DEBUG_ALWAYS, (type == PROPERTIES_POINTER) || !elem_is_a_struct,
               "Error: elem_is_a_struct must be false when type is not a "
               "PROPERTIES_POINTER\n");
  Debug_outNest(DEBUG_VERBOSE);
  Serializable *serObj = NULL;
  char arrayPtr[SERIALIZABLE_ID_LEN] = {0};
  sprintf(arrayPtr, "%p", array);
  const Properties_Content *content = Properties_get(arrayPtrs, arrayPtr);
  Debug_out(DEBUG_VERBOSE, "Info: ArrayPtrs:\n");
  Debug_perform(DEBUG_VERBOSE, Properties_fprint(arrayPtrs, stderr));
  if (content == NULL) {
    SerializableMethods methods = {
        .className = Serializable_serializeArray_className,
        .passport = NULL,
        .serializeFields = Serializable_serializeArray_serializeFields,
        .deserializeFields = Serializable_serializeArray_deserializeFields,
        .reallocArrayField = r};
    serObj = Serializable_new(array, methods);
    Properties_setPointer(arrayPtrs, arrayPtr, (void *)serObj);
    Debug_out(DEBUG,
              "Info: new serializableObject %p from array %p with size %zu\n",
              (void *)serObj, array, sizes[0]);
    Debug_perform(DEBUG_VERBOSE, Properties_fprint(arrayPtrs, stderr));
  } else {
    serObj = (Serializable *)Properties_Content_value(content);
    Debug_out(DEBUG, "Info: already created a serializableObject %p from array "
                     "%p with size %zu\n",
              (void *)serObj, array, sizes[0]);
  }
  Debug_out(DEBUG_VERBOSE, "Info: ArrayPtrs:\n");
  Debug_perform(DEBUG_VERBOSE, Properties_fprint(arrayPtrs, stderr));
  for (size_t i = 0; i < sizes[0]; i++) {
    void *elem = NULL;
    char property[SERIALIZABLE_ARRAY_FIELD_LEN] = {0};
    sprintf(property, "%zu", i);
    if (depth > 0) {
      elem = ELEM_PTR(array, i, sizeof(void *));
      Properties_set(serObj->fields, property, PROPERTIES_POINTER,
                     Serializable_serializeArray(*(void **)elem, elem_size,
                                                 sizes + 1, type, f, depth - 1,
                                                 elem_is_a_struct, r));
    } else {
      elem = ELEM_PTR(array, i, elem_size);
      if (type == PROPERTIES_POINTER && elem_is_a_struct)
        elem = f(elem); // call function for serialize struct elem
      else if (type == PROPERTIES_POINTER && !elem_is_a_struct)
        elem = f(*(
            void **)elem); // call function for serialize pointer to struct elem
      if (type == PROPERTIES_STRING)
        elem = *(void **)elem;
      Properties_set(
          serObj->fields, property, type,
          elem); // set pointer or plain type into fields of this serObj
      Debug_out(DEBUG_VERBOSE, "Info: reading field no. %s/%zu with value %p\n",
                property, sizes[0] - 1, (void *)elem);
    }
  }
  Debug_out(DEBUG_VERBOSE, "Info: ArrayPtrs:\n");
  Debug_perform(DEBUG_VERBOSE, Properties_fprint(arrayPtrs, stderr));
  Debug_out(DEBUG_VERBOSE, "Info: Serializable Array:\n");
  Debug_perform(DEBUG_VERBOSE, Serializable_fprintFields(serObj, stderr));
  Debug_outUnnest(DEBUG_VERBOSE);
  return serObj;
}

const void *Serializable_getArrayField(Serializable *this_, char *attr,
                                       void *array, size_t elem_size,
                                       size_t const *sizes, unsigned int depth,
                                       const void *passport) {
  Debug_assert(DEBUG_ALWAYS, this_ != NULL, "Error: this_ == NULL\n");
  Debug_assert(DEBUG_ALWAYS, this_->methods.passport == passport,
               "Error: this_->methods.passport != passport\n");
  const Properties_Content *content = Properties_get(this_->fields, attr);
  Debug_assert(DEBUG_ALWAYS, content != NULL,
               "Error: this_ object does not contain field %s\n", attr);
  const void *value = Properties_Content_value(content);
  Serializable_deserializeArray((Serializable *)value, array, elem_size,
                                (size_t *)sizes, depth);
  return array;
}

static void Serializable_deserializeArray(Serializable *serArray, void *array,
                                          size_t elem_size, size_t *sizes,
                                          unsigned int depth) {
  Debug_assert(DEBUG_ALWAYS, serArray != NULL, "Error: serArray == NULL");
  Debug_assert(DEBUG_ALWAYS, array != NULL, "Error: array == NULL");
  Debug_assert(DEBUG_ALWAYS, serArray->fields != NULL,
               "Error: serArray->fields == NULL");
  Properties *fields = serArray->fields;
  size_t classField = 0;
  const char *class_ = NULL;
  Properties_getString(fields, SERIALIZABLE_CLASS, &class_);
  if (class_ != NULL)
    classField = 1;
  if (depth > 0) {
    Debug_assert(DEBUG_ALWAYS, class_ != NULL,
                 "Error: Object must have a class_ specifier");
    Debug_assert(
        DEBUG_ALWAYS, StringUtils_equals(class_, SERIALIZABLE_CLASS_ARRAY),
        "Error: At depth %d, objects must be of class_ %s instead of %s\n",
        depth, SERIALIZABLE_CLASS_ARRAY, class_);
  }
  size_t size = Properties_size(fields) - classField;
  Debug_assert(DEBUG_ALWAYS, sizes[0] <= size, "Error: Array object len %zu is "
                                               "greater than deserialized "
                                               "array len %zu\n",
               sizes[0], size);
  for (size_t i = 0; i < sizes[0]; i++) {
    void *elem = NULL;
    char i_str[SERIALIZABLE_ARRAY_FIELD_LEN] = {0};
    sprintf(i_str, "%zu", i);
    const Properties_Content *field = NULL;
    if (depth > 0) {
      elem = ELEM_PTR(array, i, sizeof(void *));
      field = Properties_get(fields, i_str);
      Serializable *field_array =
          (Serializable *)Properties_Content_value(field);
      Serializable_deserializeArray(field_array, *(void **)elem, elem_size,
                                    sizes + 1, depth - 1);
    } else {
      elem = ELEM_PTR(array, i, elem_size);
      field = Properties_get(fields, i_str);
      const void *field_value = Properties_Content_value(field);
      Debug_out(DEBUG_VERBOSE,
                "Info: Deserializing field %s: %p into array elem: %p\n", i_str,
                field_value, elem);
      if (Properties_Content_type(field) != PROPERTIES_POINTER) {
        // plain type copy from field_value of properties serObj->fields into
        // elem
        // if type of field_value is a pointer there is no need to do memcopy
        // because the
        // address of field_value is the same of elem (see how serialization of
        // array works)
        Debug_assert(DEBUG, elem != field_value, "Error: elem %p, field_value "
                                                 "%p, elem_size %zu, class_ of "
                                                 "serArray %s\n",
                     elem, field_value, elem_size,
                     serArray->methods.className(serArray->subInstance));
        if (Properties_Content_type(field) == PROPERTIES_STRING)
          Serializable_deserializeString(*(char **)elem, (char *)field_value);
        else
          memcpy(elem, field_value, elem_size);
      }
    }
  }
  Debug_out(DEBUG_VERBOSE, "Info: Array fields deserialized:\n");
  Debug_perform(DEBUG_VERBOSE, Properties_fprint(fields, stderr));
}

char *Serializable_deserializeString(char *dst, const char *src) {
  Debug_assert(DEBUG_ALWAYS, src != NULL, "Error: src == NULL");
  Debug_assert(DEBUG_ALWAYS, dst != NULL, "Error: dst == NULL");
  return strncpy(dst, src, strlen(dst) + 1);
}

void Serializable_replaceArrayPtr(void *oldArray, void *newArray) {
  Debug_assert(DEBUG_ALWAYS, oldArray != NULL, "Error: oldArray == NULL\n");
  Debug_assert(DEBUG_ALWAYS, newArray != NULL, "Error: newArray == NULL\n");
  Debug_assert(DEBUG_ALWAYS, arrayPtrs != NULL, "Error: arrayPtrs == NULL\n");
  Debug_out(DEBUG, "Replacing array %p with array %p in arrayPtrs\n", oldArray,
            newArray);
  Serializable *serObj = NULL;
  char oldArrayPtr[SERIALIZABLE_ID_LEN] = {0};
  sprintf(oldArrayPtr, "%p", oldArray);
  char newArrayPtr[SERIALIZABLE_ID_LEN] = {0};
  sprintf(newArrayPtr, "%p", newArray);
  const Properties_Content *content = Properties_get(arrayPtrs, oldArrayPtr);
  serObj = (Serializable *)Properties_Content_value(content);
  Debug_assert(DEBUG_ALWAYS, serObj->subInstance == oldArray,
               "Error: serObj->subInstance != oldArray\n");
  serObj->subInstance = newArray;
  Properties_remove(arrayPtrs, oldArrayPtr);
  Properties_setPointer(arrayPtrs, newArrayPtr, (void *)serObj);
}

void Serializable_reduceArraySize(Serializable *this_, char *attr,
                                  char *attr_length,
                                  Properties_Content_Type type, void *size,
                                  const void *passport) {
  Debug_assert(DEBUG_ALWAYS, this_ != NULL, "Error: this_ == NULL\n");
  Debug_assert(DEBUG_ALWAYS, this_->methods.passport == passport,
               "Error: this_->methods.passport != passport\n");
  const Properties_Content *content = Properties_get(this_->fields, attr);
  Debug_assert(DEBUG_ALWAYS, content != NULL,
               "Error: this_ object does not contain field %s\n", attr);
  const void *value = Properties_Content_value(content);
  Serializable *serArray = (Serializable *)value;
  Debug_assert(DEBUG_ALWAYS, serArray != NULL, "Error: serArray == NULL\n");
  Debug_assert(DEBUG_ALWAYS, serArray->fields != NULL,
               "Error: serArray->fields == NULL\n");
  Properties *fields = serArray->fields;
  if (attr_length != NULL) {
    Debug_assert(DEBUG_ALWAYS, size != NULL, "Error: size == NULL\n");
    Properties_set(this_->fields, attr_length, type, size);
  }
  Properties_free(&fields);
  serArray->fields = Properties_new();
  this_->methods.serializeFields(this_->subInstance);
  Debug_out(DEBUG, "Reduce array: done!\n");
}

const char *Serializable_className(const Serializable *this_) {
  Debug_assert(DEBUG_ALWAYS, this_ != NULL, "Error: this == NULL");
  return this_->methods.className(this_->subInstance);
}

void Serializable_fprintFields(Serializable *this_, FILE *f) {
  Properties_fprint(this_->fields, f);
}
