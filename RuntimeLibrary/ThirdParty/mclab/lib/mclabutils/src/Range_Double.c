/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>

#include <Debug.h>
#include <Numbers.h>

#include <Range_Double.h>

#define DEBUG "Range_Double"



struct Range_Double {
	double min;
	double max;
	double step;

	// cache
	int size;
};

void Range_Double_fprint(const Range_Double* const this, FILE* f) {
	if (this==NULL) fprintf(f, "(Range_Double*)NULL");
	else {
		fprintf(f, "Range_Double(min=%lf, max=%lf, step=%lf)", this->min, this->max, this->step);
	}
}

void Range_Double_toString(const Range_Double* this, char* result, size_t maxlen) {
	Debug_assert(DEBUG_ALWAYS, result != NULL, "result == NULL\n");
	if (this == NULL) {
		snprintf(result, maxlen, "NULL");
	} else {
		snprintf(result, maxlen, "[%lf, %lf]/%lf", this->min, this->max, this->step);
	}
}


int Range_Double_equals(const Range_Double* this, const Range_Double* other) {
	if (this == NULL) return other == NULL;
	if (other == NULL) return 0;
	return Numbers_approxEQ(this->min, other->min)
		&& Numbers_approxEQ(this->max, other->max)
		&& Numbers_approxEQ(this->step, other->step);
}


typedef enum {NORMAL, FLIPPING} It_Type;
struct Range_Double_Iterator {
	Range_Double* range;
	It_Type type;

	int hasNext;
	double next;

	// Additional info for FLIPPING iterators
	double med;
	int absDelta;
	int wentRight;
	int minReached;
	int maxReached;

};


Range_Double* Range_Double_new(double min, double max, double step) {
	Debug_assert(DEBUG_ALWAYS, max >= min, "max = %lf < %lf = min\n", max, min);
	Debug_assert(DEBUG_ALWAYS, step > 0, "step = %lf <= 0\n", step);
	Range_Double* new = calloc(1, sizeof(Range_Double));
	Debug_assert(DEBUG_ALWAYS, new != NULL, "new == NULL\n");
	new->min = min;
	new->max = max;
	new->step = step;
	new->size = -1;
	return new;
}

Range_Double* Range_Double_new_fromString(const char* string) {
	Debug_assert(DEBUG_ALWAYS, string != NULL, "string == NULL\n");
	double min = 0;
	double max = 0;
	double step = 1;
	int tokens = sscanf(string, "%lf", &min);
	if (tokens == 1) {
		max = min;
	} else {
		sscanf(string, "[%lf, %lf]/%lf", &min, &max, &step);
		if (max < min) max = min;
	}
	return Range_Double_new(min, max, step);
}


double Range_Double_min(const Range_Double* const this) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	return this->min;
}
double Range_Double_max(const Range_Double* const this) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	return this->max;
}
double Range_Double_step(const Range_Double* const this) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	return this->step;
}

double Range_Double_value(Range_Double* const this, int index) {

	if (index <= 0) return this->min;
	else if (index >= Range_Double_size(this)-1) return this->max;
	else return this->min + index*this->step;
}

int Range_Double_closest_index(Range_Double* const this, double value) {
	Debug_out(DEBUG, "Range_Double_closest_index(this, %lf): start\nthis=", value);
	Debug_perform(DEBUG, { Range_Double_fprint(this, stderr); fprintf(stderr, "\n"); });

	int result = 0;
	if (Numbers_approxLE(value, this->min)) result = 0;
	else {
		double steps_from_min = round((value - this->min)/this->step);
		Debug_out(DEBUG, " - steps_from_min = round( (%lf - %lf)/%lf ) = %lf\n",
			value, this->min, this->step, steps_from_min);

		result = (int)steps_from_min;
		Debug_assert(DEBUG, result >= 0, "too low: %d\n", result);
    int size = Range_Double_size(this) - 1;
		if (result > size) result = size;
		Debug_out(DEBUG, " - result = %d\n", result);
	}
	Debug_out(DEBUG, "Range_Double_closest_index(): end. Returning %d\n", result);
	return result;
}


double Range_Double_closest_value(Range_Double* const this, double value) {
	Debug_out(DEBUG, "Range_Double_closest_value(this, %lf): start\nthis=", value);
	Debug_perform(DEBUG, { Range_Double_fprint(this, stderr); fprintf(stderr, "\n"); });

	int index = Range_Double_closest_index(this, value);
	double result  = Range_Double_value(this, index);

	Debug_out(DEBUG, "Range_Double_closest_value(): end. Returning %lf\n", result);
	return result;
}

void Range_Double_free(Range_Double** thisP) {
	Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
	Range_Double* this = *thisP;
	if (this == NULL) return;
	free(this);
	*thisP = NULL;
}

int Range_Double_size(Range_Double* const this) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
  if (this->size < 0) {
    this->size = (int) floor((this->max - this->min)/this->step) + 1;
  }
  return this->size;
}







Range_Double_Iterator* __Range_Double_Iterator_new(Range_Double* const range, It_Type type) {
	Debug_assert(DEBUG_ALWAYS, range != NULL, "range == NULL");
	Range_Double_Iterator* new = calloc(1, sizeof(Range_Double_Iterator));
	Debug_assert(DEBUG_ALWAYS, new != NULL, "new == NULL");
	new->range = range;
	new->type = type;
	new->absDelta = 0;
	new->med = 0;
	new->wentRight = 1;
	new->minReached = 0;
	new->maxReached = 0;
	Range_Double_Iterator_rewind(new);
	return new;
}
Range_Double_Iterator* Range_Double_Iterator_new(Range_Double* const range) {
	return __Range_Double_Iterator_new(range, NORMAL);
}
Range_Double_Iterator* Range_Double_Iterator_new_flipping(
	Range_Double* const range) {
		return __Range_Double_Iterator_new(range, FLIPPING);
}

int Range_Double_Iterator_hasNext(Range_Double_Iterator* const this) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	return this->hasNext;
}

void next_flipping(Range_Double_Iterator* const this) {
	Debug_out(DEBUG, "Start next_flipping(): start (this->next = %lf, this->hasNext = %d) \n", this->next, this->hasNext);
	int done = 0;
	while (!done && this->hasNext) {

		if ( !this->wentRight) {
			// I just went left

			// prepare to go right
			this->absDelta++;
			if (!this->maxReached) {
				this->next = this->med + (this->absDelta * Range_Double_step(this->range));
				Debug_out(DEBUG, "max NOT reached, going right: this->next = %lf\n", this->next);
				/*Debug_assert(DEBUG, this->next <= Range_Double_max(this->range),
					"this->next > this->range->max\n");*/
				this->wentRight = 1;

			} else {
				// max reached, keep going left
				this->next = this->med - (this->absDelta * Range_Double_step(this->range));
				Debug_out(DEBUG, "max reached, keeping going left: this->next = %lf\n", this->next);
				/*Debug_assert(DEBUG, this->next >= Range_Double_min(this->range),
					"this->next < this->range->min\n");*/
			}
		} else {
			// I just went right

			// prepare to go left
			if (!this->minReached) {
				this->next = this->med - (this->absDelta * Range_Double_step(this->range));
				Debug_out(DEBUG, "min NOT reached, going left: this->next = %lf\n", this->next);
				/*Debug_assert(DEBUG, this->next >= Range_Double_min(this->range),
					"this->next < this->range->min\n");*/
				this->wentRight = 0;
			} else {
				// keep going right
				this->absDelta++;
				this->next = this->med + (this->absDelta * Range_Double_step(this->range));
				Debug_out(DEBUG, "min reached, keeping going right: this->next = %lf\n", this->next);
				/*Debug_assert(DEBUG, this->next <= Range_Double_max(this->range),
					"this->next > this->range->max\n");*/
			}
		}

		if (this->wentRight) {
			if (Numbers_approxLE(this->next, Range_Double_max(this->range))) {
				done = 1;
			} else {
				Debug_out(DEBUG, "MAX REACHED\n");
				this->maxReached = 1;	// remember that max was reached and re-do.
			}
		} else {
			if (Numbers_approxGE(this->next, Range_Double_min(this->range))) {
				done = 1;
			} else {
				Debug_out(DEBUG, "MIN REACHED\n");
				this->minReached = 1;	// remember that min was reached and re-do.
			}
		}
		this->hasNext = (!this->minReached || !this->maxReached);
	} // end while(!done)

	Debug_out(DEBUG, "End next_flipping()\n");
}


int Range_Double_Iterator_next(Range_Double_Iterator* const this, double* result) {
	if (!Range_Double_Iterator_hasNext(this)) return 0;
	Debug_assert(DEBUG_ALWAYS, result != NULL, "result == NULL\n");
	*result = this->next;

	switch(this->type) {
		case NORMAL :
			this->next += Range_Double_step(this->range);
			this->hasNext =
				Numbers_approxLE(this->next, Range_Double_max(this->range));
			break;

		case FLIPPING :
			next_flipping(this);
			break;
	}
	return 1;
}
void Range_Double_Iterator_rewind(Range_Double_Iterator* const this) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");

	//Debug_out(DEBUG, "Start rewind()\n");
	this->hasNext = 1;
	switch(this->type) {
		case NORMAL :
			this->next = Range_Double_min(this->range);
			break;

		case FLIPPING :
			this->absDelta = 0;
			this->med = Range_Double_min(this->range) +
				Range_Double_step(this->range) * (Range_Double_size(this->range)/2);
			//Debug_out(DEBUG, " - this->med = %lf\n", this->med);
			this->next = this->med;
			this->wentRight = 0;
			this->minReached = 0;
			this->maxReached = 0;
			break;
		default:
			Debug_assert(DEBUG, 0, "Iterator type %d unknown\n", this->type);
	}
	//Debug_out(DEBUG, "End rewind()\n");
}
void Range_Double_Iterator_free(Range_Double_Iterator** thisP) {
	Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
	Range_Double_Iterator* this = *thisP;
	if (this == NULL) return;
	free(this);
	*thisP = NULL;
}



