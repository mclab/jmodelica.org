/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>

#include <Properties.h>

#include <Debug.h>
#include <Timer.h>

#define DEBUG_THIS "Timer"

typedef struct {
	char name[TIMER_NAME_MAXLEN+1];
	unsigned long long start;
	unsigned long long end;
	unsigned long long start_0;
} Timer;

static Properties* timers = NULL;

Timer* _Timer_new(const char* const name) {
	Timer* new = calloc(1, sizeof(Timer));
	strncpy(new->name, name, TIMER_NAME_MAXLEN);
	new->start = 0;
	new->start_0 = 0;
	new->end = 0;
	Debug_out(DEBUG_THIS, "New timer '%s' created\n", new->name);
	return new;
}

void _Timer_free(Timer** thisP) {
	Debug_assert(DEBUG_ALL, thisP != NULL, "thisP == NULL\n");
	Timer* this = *thisP;
	if (this == NULL) return;
	free(this);
	*thisP = NULL;
}


void Timer_startFromValue(const char* const name, unsigned long long value) {
	Debug_assert(DEBUG_ALWAYS, name != NULL, "name == NULL\n");
	Debug_out(DEBUG_THIS, "Timer_startFromValue(%s, %lld): start\n", name, value);
	Debug_outNest(DEBUG_THIS);
	if (timers == NULL) {
		Debug_out(DEBUG_THIS, "There were no timers: Properties object created\n");
		timers = Properties_new(TIMER_NAME_MAXLEN);
		Debug_assert(DEBUG_ALWAYS, timers != NULL, "timers == NULL!\n");
	}
	Timer* timer = NULL;
	int err = Properties_getPointer(timers, name, (const void**)&timer);
	Debug_out(DEBUG_THIS, "Timer object retrieved from Properties: %p\n", (void*)timer);
	Debug_assert(DEBUG_THIS, (err == 0) == (timer != NULL), "err is %d but timer = %p\n", err, (void*)timer);
	if (timer == NULL) {
		// Create a new timer
		Debug_out(DEBUG_THIS, "timer == NULL -> User requested a new timer\n");
		timer = _Timer_new(name);
		Properties_setPointer(timers, timer->name, timer);
		Debug_out(DEBUG_THIS, "Timer '%s' put into Properties\n", timer->name);
	}
	timer->start = Debug_timestamp_millisec();
	timer->start_0 = value;
	timer->end = 0;
	Debug_out(DEBUG_THIS, "Timer_start(%s): timer started\n", timer->name);
}


unsigned long long Timer_getCurrentValue(const char* const name) {
	Debug_assert(DEBUG_ALWAYS, name != NULL, "name == NULL\n");
	Timer* timer = NULL;
	int err = Properties_getPointer(timers, name, (const void**)&timer);
	Debug_assert(DEBUG_THIS, (err == 0) == (timer != NULL), "err is %d but timer = %p\n", err, (void*)timer);
	Debug_assert(DEBUG_ALWAYS, timer != NULL, "Timer_end(%s): timer has not been created\n", name);
	timer->end = Debug_timestamp_millisec();
	unsigned long long timeDiff = timer->end - timer->start + timer->start_0;
	return timeDiff;
}


void Timer_start(const char* const name) {
	Debug_assert(DEBUG_ALWAYS, name != NULL, "name == NULL\n");
	Timer_startFromValue(name, (unsigned long long) 0);
}


unsigned long long Timer_end(const char* const name) {
	Debug_assert(DEBUG_ALWAYS, name != NULL, "name == NULL\n");
	Timer* timer = NULL;
	int err = Properties_getPointer(timers, name, (const void**)&timer);
	Debug_assert(DEBUG_THIS, (err == 0) == (timer != NULL), "err is %d but timer = %p\n", err, (void*)timer);
	Debug_assert(DEBUG_ALWAYS, timer != NULL, "Timer_end(%s): timer has not been created\n", name);
	timer->end = Debug_timestamp_millisec();
	unsigned long long timeDiff = timer->end - timer->start + timer->start_0;
	Properties_remove(timers, name);
	Debug_out(DEBUG_THIS, "Timer_end(%s): time elapsed = %llu milliseconds\n", timer->name, timeDiff);
	_Timer_free(&timer);

	if (Properties_size(timers) == 0) {
		Properties_free(&timers);
	}
	return timeDiff;
}

