/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define DEBUG "Numbers"

static double absoluteTolerance = 1E-6;
static double relativeTolerance = 1E-6;

void Numbers_setAbsoluteTolerance(double t) {
	absoluteTolerance = fabs(t);
}
double Numbers_getAbsoluteTolerance() {
	return absoluteTolerance;
}

void Numbers_setRelativeTolerance(double t) {
	relativeTolerance = fabs(t);
}
double Numbers_getRelativeTolerance() {
	return relativeTolerance;
}

int Numbers_approxEQ(double d1, double d2) {
	double absDiff = fabs(d1-d2);
	double max_abs = fabs(d1);
	double abs_d2 = fabs(d2);
	if (abs_d2 > max_abs) max_abs = abs_d2;

	return absDiff <= absoluteTolerance ||
		absDiff <= relativeTolerance * max_abs;
}

int Numbers_approxLE(double d1, double d2) {
	return d1 <= d2 || Numbers_approxEQ(d1, d2);
}

int Numbers_approxL(double d1, double d2) {
	return d1 <= d2 && !Numbers_approxEQ(d1, d2);
}


int Numbers_approxGE(double d1, double d2) {
	return Numbers_approxLE(d2, d1);
}
int Numbers_approxG(double d1, double d2) {
	return d1 >= d2 && !Numbers_approxEQ(d1, d2);
}



double Numbers_relativeError(double d1, double d2) {
	double denominator = d1;
	int sign = (denominator > 0 ? 1 : -1);
	if (sign < 0) {
		denominator = -denominator;
	}
	if (denominator < relativeTolerance) denominator = relativeTolerance;
	return sign*(d1-d2)/denominator;
}


int Numbers_abs_int(int v) {
	return (v >= 0 ? v : -v);
}
long Numbers_abs_long(long v) {
	return (v >= 0 ? v : -v);
}
double Numbers_abs_double(double v) {
	return (Numbers_approxGE(v, 0) ? v : -v);
}

