#include <stdlib.h>
/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>

#include <CSV.h>
#include <StringUtils.h>

#include <Debug.h>


#define DEBUG "CSV"



struct CSV {
	FILE *f;
	char separator;
	unsigned int rowMaxLen;
	Array* header;
	Array* next;
	int lineNb;
	int fieldsNb;
};

int _CSV_nextLine(CSV* this, Array* fields){

	Array* tokens = NULL;
	char buffer[this->rowMaxLen];
	for (int i = 0; i < this->rowMaxLen; i++) buffer[i] = '\0';
	if (fgets(buffer, this->rowMaxLen, this->f) != NULL) {
		Debug_out(DEBUG, "CSV line '%s'\n", buffer);
		StringUtils_trim(buffer);
		tokens = Array_newString(100,100);
		StringUtils_tokenize(buffer, &this->separator, tokens);
		for (int i = 0; i < Array_length(tokens); i++) {
			char *s = NULL;
			Array_get(tokens, i, &s);
			char *s_new = StringUtils_clone(s);
			Array_add(fields, &s_new);
		}
		Array_free(&tokens);
	} else {
		return -1;
	}
	return 0;
}


CSV* CSV_new(const char* fileName, int hasHeader, char separator, unsigned int rowMaxLen) {
	Debug_assert(DEBUG_ALWAYS, fileName != NULL, "fileName == NULL\n");

	CSV* new = calloc(1, sizeof(CSV));

	new->f = fopen(fileName, "r");
	new->separator = separator;
	new->rowMaxLen = rowMaxLen;
	new->lineNb = 0;
	new->fieldsNb = 0;

	if (hasHeader) {
		new->header = Array_newString(100,100);
		if (_CSV_nextLine(new, new->header) < 0) {
			Debug_assert(DEBUG_ALWAYS, 0, "An error occurs while reading header\n");
		}
		new->fieldsNb = Array_length(new->header);
		new->lineNb++;
	}

	new->next = Array_newString(100, 100);
	if (_CSV_nextLine(new, new->next) < 0) {
		Debug_assert(DEBUG_ALWAYS, 0, "An error occurs while reading line %d\n", new->lineNb);
	}
	if (new->fieldsNb > 0) {
		Debug_assert(DEBUG_ALWAYS, Array_length(new->next) == new->fieldsNb, "Array_length(new->next) %d != new->fieldsNb %d \n", Array_length(new->next), new->fieldsNb);
	}
 	new->lineNb++;
 	return new;
}

void CSV_free(CSV** thisP) {
	if (thisP == NULL) return;
	CSV *this = (CSV*)*thisP;
	fclose(this->f);
	char *s = NULL;
	if (this->header != NULL) {
		for(int i = 0; i < Array_length(this->header); i++) {
			Array_get(this->header, i, &s);
			free(s);
		}
		Array_free(&this->header);
	}
	if (this->next != NULL){
		for(int i = 0; i < Array_length(this->next); i++) {
			Array_get(this->next, i, &s);
			free(s);
		}
		Array_free(&this->next);
	}
	free(this);
	thisP = NULL;
	return;
}


int CSV_hasNextRow(CSV* this) {
	return this->next != NULL;
}

Array* CSV_nextRow(CSV* this) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	Array* ret = NULL;
	if (this->next != NULL) {
		char *s = NULL;
		ret = this->next;
		this->next = Array_newString(100,100);
		if (_CSV_nextLine(this, this->next) == 0) {
			if (this->fieldsNb > 0) {
				Debug_assert(DEBUG_ALWAYS, Array_length(this->next) == this->fieldsNb, "Array_length(this->next) %d != this->fieldsNb %d \n", Array_length(this->next), this->fieldsNb);
			}
			this->lineNb++;
		} else {
			Array_free(&this->next);
			this->next = NULL;
		}
	}
	return ret;
}

Array* CSV_header(CSV* this) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	return this->header;
}

