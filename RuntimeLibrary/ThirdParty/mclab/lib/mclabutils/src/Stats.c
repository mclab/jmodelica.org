/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <math.h>

#include <Stats.h>


#define AVG_BODY(type) {\
	if (n == 0) { \
		fprintf(stderr, "Stats_avg(): n must be > 0.\n");\
		return 0;\
	}\
	\
	long double result = 0;\
	unsigned long i=0;\
	for(i=0; i<n; i++) {\
		result = ((result*i) + data[i]) / (i+1);\
	}\
	return (type) result;\
}


/***** BAGOUS ************
#define STDDEV_GIVEN_AVG_BODY {\
	if (n < 1) {\
		fprintf(stderr, "Stats_stddev(): n must be > 0.\n");\
		return 0;\
	}\
	long double result = 0;\
	if (n < 2) return result;\
	unsigned long i=0;\
	result = (powl((data[0] - avg), 2) + powl((data[1] - avg), 2))  /  (2-1);   \
	for(i=2; i<n; i++) {\
		result = ((result*i) + powl((data[i] - avg), 2)) / (i)   ;\
	}\
	result = sqrtl(result);\
	return result;\
}\
************************/

/* 	stddev^2(data[0]) = (1/n) * (data[0] - avg)^2
		forall j>=1: stddev^2(data[0..j]) = stddev^2(data[0..j-1]) + (1/n) * (data[j]-avg)^2
*/
#define STDDEV_GIVEN_AVG_BODY(type) {\
	if (n < 1) {\
		fprintf(stderr, "Stats_stddev(): n must be > 0.\n");\
		return 0;\
	}\
	long double result = 0;\
	if (n < 2) return (type) result;\
	unsigned long j=0;\
  long double avgPromoted = (long double) avg;\
	result = (powl((data[0] - avgPromoted), 2)) / n;   \
	for(j=1; j<n; j++) {\
		result += (powl((data[j] - avgPromoted), 2)) / n; \
	}\
	result = sqrtl(result);\
	return (type) result;\
}\


double Stats_avg_d(const double* const data, unsigned long n) {
	AVG_BODY(double);
}
double Stats_stddev_d(const double* const data, unsigned long n) {
	return Stats_stddevGivenAvg_d(data, n, Stats_avg_d(data, n));
}
double Stats_stddevGivenAvg_d(const double* const data, unsigned long n, double avg) {
	STDDEV_GIVEN_AVG_BODY(double);
}


long double Stats_avg_ld(const long double* const data, unsigned long n) {
	AVG_BODY(long double);
}
long double Stats_stddev_ld(const long double* const data, unsigned long n) {
	return Stats_stddevGivenAvg_ld(data, n, Stats_avg_ld(data, n));
}
long double Stats_stddevGivenAvg_ld(const long double* const data, unsigned long n, long double avg) {
	STDDEV_GIVEN_AVG_BODY(long double);
}




long double Stats_avg_l(const long* const data, unsigned long n) {
	AVG_BODY(long double);
}
long double Stats_stddev_l(const long* const data, unsigned long n) {
	return Stats_stddevGivenAvg_l(data, n, Stats_avg_l(data, n));
}
long double Stats_stddevGivenAvg_l(const long* const data, unsigned long n, long double avg) {
	STDDEV_GIVEN_AVG_BODY(long double);
}





long double Stats_avg_ul(const unsigned long* const data, unsigned long n) {
	AVG_BODY(long double);
}
long double Stats_stddev_ul(const unsigned long* const data, unsigned long n) {
	return Stats_stddevGivenAvg_ul(data, n, Stats_avg_ul(data, n));
}
long double Stats_stddevGivenAvg_ul(const unsigned long* const data, unsigned long n, long double avg) {
	STDDEV_GIVEN_AVG_BODY(long double);
}


long double Stats_avg_time_t(const time_t* const data, unsigned long n) {
	AVG_BODY(long double);
}
long double Stats_stddev_time_t(const time_t* const data, unsigned long n) {
	return Stats_stddevGivenAvg_time_t(data, n, Stats_avg_time_t(data, n));
}
long double Stats_stddevGivenAvg_time_t(const time_t* const data, unsigned long n, long double avg) {
	STDDEV_GIVEN_AVG_BODY(long double);
}

