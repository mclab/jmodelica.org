/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#include <RndGenDistribution.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <Debug.h>
#include <Array.h>
#include <RndGen.h>

#define DEBUG "RndGenDistribution"

struct RndGenDistribution {
	RndGen* rnd;
	unsigned long x_min;
	unsigned long x_max;

	unsigned long distr_norm_factor;

	/* for each i:
		distribution[i] = prob(x_min+i)
	*/
	Array* distribution; // of double


	// Cache:
	int isCacheValid;
	/* for each i:
		cumulative[i] = sum_{0 <= j <= i} distribution[i]*distr_norm_factor
	*/
	Array* cache_cumulative; // of unsigned long
	unsigned long cache_integral;

};

unsigned long RndGenDistribution_domain_min(const RndGenDistribution* const this) {
	return this->x_min;
}
unsigned long RndGenDistribution_domain_max(const RndGenDistribution* const this) {
	return this->x_max;
}


void buildCache(RndGenDistribution* this);

	/* meaningful decimal places (precision: the higher the better)
	   Each value of probability() is approximated as a multiple of 1/(10^decimal_places).

	  Example: assume that x_min = 0, x_max = 3 and probability() = {1-->0.1, 2-->0.73, 3-->0.26}
	  If decimal_places = 1, we are scaling the probability distribution as follows:
	      x=1 --> (int) 0.1  * 10^1 = 1
	      x=2 --> (int) 0.73 * 10^1 = 7
	      x=3 --> (int) 0.26 * 10^1 = 2

	  With a better precision, e.g., decimal_places = 2, we are scaling the
	  probability distribution as follows:

		x=1 --> (int) 0.1  * 10^2 = 10
		x=2 --> (int) 0.73 * 10^2 = 73
		x=3 --> (int) 0.26 * 10^2 = 26
	*/


RndGenDistribution* RndGenDistribution_new(RndGen* rnd, unsigned long x_min, unsigned long x_max, int decimal_places) {
	if (rnd == NULL) {
		fprintf(stderr, "RndGenDistribution_new() rnd==NULL\n");
		return NULL;
	}
	if (x_min > x_max) {
		fprintf(stderr, "RndGenDistribution_new() x_min > x_max\n");
		return NULL;
	}
	if (decimal_places < 1) {
		// at least one decimal digit needed
		fprintf(stderr, "RndGenDistribution_new() decimal_places < 1\n");
		return NULL;
	}
	RndGenDistribution* new = (RndGenDistribution*)malloc(sizeof(RndGenDistribution));
	new->rnd = rnd;
	new->x_min = x_min;
	new->x_max = x_max;
	new->distr_norm_factor = pow(10,decimal_places);
	new->distribution = Array_newDouble(x_max - x_min + 1, x_max - x_min + 1);

	double zero = 0;
	for (unsigned long i = x_min; i <= x_max; i++) {
		Array_add(new->distribution, &zero);
	}

	new->cache_cumulative = Array_new(x_max - x_min + 1, sizeof(unsigned long), x_max - x_min + 1,
		NULL, NULL);
	new->cache_integral = 0;
	new->isCacheValid = 0;
	return new;
}


// Creates a new instance from a probability distribution function
RndGenDistribution* RndGenDistribution_new_fromFunction(RndGen* rnd, double (*probability)(unsigned long), unsigned long x_min, unsigned long x_max, int decimal_places) {

		RndGenDistribution* new = RndGenDistribution_new(rnd, x_min, x_max, decimal_places);
		if (new == NULL) {
			fprintf(stderr, "RndGenDistribution_new_fromFunction(): cannot create instance\n");
			return NULL;
		}

		if (probability == NULL) {
			fprintf(stderr, "RndGenDistribution_new_fromFunction() probability==NULL\n");
			RndGenDistribution_free(&new);
			return NULL;
		}

		for (unsigned long x=x_min; x<=x_max; x++) {
			double p = probability(x);
			RndGenDistribution_set(new, x, p);
		}
		return new;
}

void RndGenDistribution_set(RndGenDistribution* this, unsigned long x, double p) {
	Debug_assert(DEBUG_ALL, x >= this->x_min && x <= this->x_max,
		"x = %lu is not in [%lu, %lu]\n", x, this->x_min, this->x_max);
	Debug_assert(DEBUG_ALL, p >= 0, "p(x=%lu) = %lf cannot be < 0\n", x, p);

	this->isCacheValid = 0;
	Array_set(this->distribution, x - this->x_min, &p);
}

double RndGenDistribution_get(RndGenDistribution* this, unsigned long x) {
	Debug_assert(DEBUG_ALL, x >= this->x_min && x <= this->x_max,
		"x = %lu is not in [%lu, %lu]\n", x, this->x_min, this->x_max);

	double p = 0;
	Array_get(this->distribution, x - this->x_min, &p);
	if (!this->isCacheValid) buildCache(this);

	return p / (((double)this->cache_integral)/this->distr_norm_factor);
}


RndGen* RndGenDistribution_getRndGen(RndGenDistribution* this) {
	return this->rnd;
}

void buildCache(RndGenDistribution* this) {
	if (this->isCacheValid) return;
	this->cache_integral = 0;
	Array_clear(this->cache_cumulative);

	for (unsigned long x = this->x_min; x <= this->x_max; x++) {
		unsigned long i = x - this->x_min;
		double p = 0;
		Array_get(this->distribution, i, &p);
		this->cache_integral += (unsigned long)(p * this->distr_norm_factor);
		Array_set(this->cache_cumulative, i, &this->cache_integral);
	}
	this->isCacheValid = 1;
}


unsigned long RndGenDistribution_next(RndGenDistribution* this) {
	if (!this->isCacheValid) {
		buildCache(this);
	}

	unsigned long chosenIntegral = RndGen_nextUL(this->rnd, 1, this->cache_integral);
	unsigned long i=0;

	unsigned long length = Array_length(this->cache_cumulative);
	unsigned long cumulative_value = 0;
	do {
		Array_get(this->cache_cumulative, i, &cumulative_value);
		if (cumulative_value >= chosenIntegral) {
			return i + this->x_min;
		} else {
			i++;
		}
	} while (i < length);
	Debug_assert(DEBUG_ALL, 0, "This should never happen\n");
	return -1;
}


void RndGenDistribution_free(RndGenDistribution** thisP) {
	Debug_assert(DEBUG_ALL, thisP != NULL, "thisP == NULL\n");
	RndGenDistribution* this = *thisP;
	if (this == NULL) return;
	Array_free(&this->distribution);
	Array_free(&this->cache_cumulative);
	free(this);
	*thisP = NULL;
}

