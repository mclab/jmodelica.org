/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <ctype.h>

#include <StringUtils.h>
#include <Debug.h>
#include <Array.h>

#define DEBUG_S "StringUtils"

int StringUtils_lastIndexOf(const char* str, char c) {
	if (!str) return -1;
	int result = strlen(str)-1;
	while (result >= 0 && str[result] != c) result--;
	return result;
}

int StringUtils_lastIndexOf_atMost(const char* str, char c, int maxIndex) {
	if (!str) return -1;
	if (maxIndex < 0) return -1;
	int len = strlen(str);
	if (maxIndex >= len) {
		maxIndex = len-1;
	}
	int result = maxIndex;
	while (result >= 0 && str[result] != c) result--;
	return result;
}

char* StringUtils_clone(const char* str) {
	if (str == NULL) return NULL;
	char* result = calloc(strlen(str)+1, sizeof(char));
	strcpy(result, str);
	return result;
}

char* StringUtils_substring(const char* str, int from, int to) {
	Debug_out(DEBUG_S, "start substring(str='%s', from=%d, to=%d) -- strlen(str) = %lu\n", str, from, to, strlen(str));
	int str_l = strlen(str);
	if (from > str_l - 1) return NULL;

	while(from < 0) {
		// counts from end
		from = str_l + from;
	}
	while(to < 0) {
		// counts from end
		to = str_l + to;
	}
	if (to == 0) to = str_l - 1;
	Debug_out(DEBUG_S, "  --> substring(str='%s', from=%d, to=%d)\n", str, from, to);

	Debug_out(DEBUG_S, "Allocating %d chars for result\n", to-from+2);
	char* result = calloc(to-from+2, sizeof(char));
	Debug_out(DEBUG_S, "Copying %d chars from str[%d] = '%c' to result\n", to-from+1, from, str[from]);
	strncpy(result, &str[from], to-from+1);
	Debug_out(DEBUG_S, "  --> result = '%s'\n", result);
	return result;
}

#define DEBUG_TRIM "StringUtils_trim"
#define WHITE_SPACE " \t\n\r"
char* StringUtils_trim(char* str) {
	char* start = str;
	Debug_out(DEBUG_TRIM, "first char *str == ASCII %d\n", *str);
	while (*start != '\0' && strchr(WHITE_SPACE, *start) != NULL) {
		Debug_out(DEBUG_TRIM, "Trimming front char ASCII %d\n", *start);
		start++;
	}
	int len = strlen(start);
	if (len == 0) {
		str[0] = '\0';
		return str;
	}
	char* last = start + len - 1;
	while (last != start && strchr(WHITE_SPACE, *last) != NULL) {
		Debug_out(DEBUG_TRIM, "Trimming end char ASCII %d\n", *last);
		*last = '\0';
		last--;
	}
	// trimmed string is between 'start' and 'last'+1
	memmove((void*)str, start, last-start+2);
	return str;
}

int StringUtils_strpos(const char* s, char c) {
	if (s == NULL) return -1;
	int i=0;
	while (*s != '\0') {
		if (*s == c) return i;
		i++;
		s++;
	}
	return -1;
}

int StringUtils_replaceAll(char* s, char* findChars, char* replaceChars) {
	int i=0;
	int result = 0;
	while (*s != '\0') {
		i = StringUtils_strpos(findChars, *s);
		if (i >= 0) {
			*s = replaceChars[i];
			result++;
		}
		s++;
	}
	return result;
}

#define DEBUG_TOK "StringUtils_tokenize"
void StringUtils_tokenize(char* s, char* sep, Array* tokens) {
	Debug_out(DEBUG_TOK, "Tokenizing '%s':\n", s);
	if (s == NULL) return;

	while(*s != '\0' && StringUtils_strpos(sep, *s) >= 0) {
		Debug_out(DEBUG_TOK, " - Skipping char %d '%c'\n", *s, *s);
		s++;
	}
	int len = strlen(s);
	if (len == 0) {
		return;
	}
	char* last = s + len - 1;

	int withinWord = 0;
	for (s; s <= last; s++) {
		if ( StringUtils_strpos(sep, *s) >= 0 ) {
			Debug_out(DEBUG_TOK, " - Converting separator char %d '%c' into ZERO\n", *s, *s);
			// *s is a separator
			*s = '\0'; // convert it into '\0'
			withinWord = 0;
		} else {
			if (!withinWord) {
				// first letter in new word
				Debug_out(DEBUG_TOK, " - Char %d '%c' is the start of a new word\n", *s, *s);
				Array_push(tokens, &s);
				Debug_out(DEBUG_TOK, " - pushed '%s' in tokens\n", s);
				withinWord = 1;
			}
		}
	}
	Debug_out(DEBUG_TOK, "Tokenization terminated. Resulting array:\n");
	Debug_perform(DEBUG_TOK, Array_fprint(tokens, stderr));
	return;
}

char* StringUtils_rightPad(char* s, size_t length, char pad) {
	int l = strlen(s);
	while(l < length) {
		s[l++] = pad;
	}
	s[l] = '\0';
	return s;
}

char* StringUtils_leftPad(char* s, size_t length, char pad) {
	int l = strlen(s);
	int newStart = length-l;
	memmove(s+newStart, s, l);
	for(int i = 0; i<newStart; i++) {
		s[i] = pad;
	}
	return s;
}

char* StringUtils_toLowercase(char* s) {
	if (s == NULL) return s;
	while(*s != '\0') {
		*s = tolower(*s);
		s++;
	}
	return s;
}

char* StringUtils_toUppercase(char* s) {
	if (s == NULL) return s;
	while(*s != '\0') {
		*s = toupper(*s);
		s++;
	}
	return s;
}

int StringUtils_startsWith(const char* const s, const char* const needle) {
	int sl = strlen(s);
	int nl = strlen(needle);
	if (sl < nl) return 0;
	const char* ss = s;
	const char* nn = needle;
	for(int i=0; i<nl; i++) {
		if (*ss != *nn) return 0;
		ss++;
		nn++;
	}
	return 1;
}

int StringUtils_startsWithIgnoreCase(const char* const s, const char* const needle) {
	char* ss = StringUtils_clone(s);
	StringUtils_toLowercase(ss);
	char* nn = StringUtils_clone(needle);
	StringUtils_toLowercase(nn);
	int result = StringUtils_startsWith(ss, nn);
	free(ss);
	free(nn);
	return result;
}


int StringUtils_endsWith(const char* const s, const char* const needle) {
	int sl = strlen(s);
	int nl = strlen(needle);
	if (sl < nl) return 0;

	const char* ss = & s[sl - nl];
	const char* nn = needle;
	for(int i=0; i<nl; i++) {
		if (*ss != *nn) return 0;
		ss++;
		nn++;
	}
	return 1;
}

int StringUtils_endsWithIgnoreCase(const char* const s, const char* const needle) {
	char* ss = StringUtils_clone(s);
	StringUtils_toLowercase(ss);
	char* nn = StringUtils_clone(needle);
	StringUtils_toLowercase(nn);
	int result = StringUtils_endsWith(ss, nn);
	free(ss);
	free(nn);
	return result;
}

int StringUtils_toInt(const char* const s, int* result) {
	*result = 0;
	int nTokens = sscanf(s, "%d", result);
	if (nTokens == 1) return 0;
	else return -1;
}
int StringUtils_toLong(const char* const s, long* result) {
	*result = 0;
	int nTokens = sscanf(s, "%ld", result);
	if (nTokens == 1) return 0;
	else return -1;
}
int StringUtils_toUInt(const char* const s, unsigned int* result) {
	*result = 0;
	int nTokens = sscanf(s, "%u", result);
	if (nTokens == 1) return 0;
	else return -1;
}
int StringUtils_toULong(const char* const s, unsigned long* result) {
	*result = 0;
	int nTokens = sscanf(s, "%lu", result);
	if (nTokens == 1) return 0;
	else return -1;
}
int StringUtils_toDouble(const char* const s, double* result) {
	*result = 0;
	int nTokens = sscanf(s, "%lf", result);
	if (nTokens == 1) return 0;
	else return -1;
}
int StringUtils_toBoolean(const char* const s, char* result) {
	*result = -1;
	int error = 0;
	if (strcasecmp(s, "true") == 0) {
		*result = 1;
	} else if (strcasecmp(s, "false") == 0) {
		*result = 0;
	} else {
		error = -1;
	}
	return error;
}


int StringUtils_equals(const char* const s, const char* const s2) {
	return (strcmp(s, s2) == 0);
}

int StringUtils_equalsIgnoreCase(const char* const s, const char* const s2) {
	return (strcasecmp(s, s2) == 0);
}

char* StringUtils_extend(char* s, size_t length){
	Debug_assert(DEBUG_ALWAYS, s != NULL, "s == NULL\n");
	size_t oldLength = strlen(s);
	Debug_assert(DEBUG_ALWAYS, length > oldLength, "length <= oldLength\n");
	s = (char *)realloc(s, (length+1)*sizeof(char));
	Debug_assert(DEBUG_ALWAYS, s != NULL, "s == NULL\n");
	s = StringUtils_rightPad(s, length, '\0');
	return s;
}



