/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <Debug.h>
#include <HashTable.h>
#include <StringUtils.h>
#include <Properties.h>
#include <HashTable.h>
#include <HashSet.h>
#include <FileUtils.h>

#include <CommandLineInterface.h>

#define OPTION_MAX_SIZE 15
#define LINE_MAX_LEN 1000
#define OPTION_LEN_LIMIT 256
#define FULL_CMD_LINE_LEN_LIMIT 1024*10

#define DEBUG_C "CommandLineInterface"

#define HELP_OR_VALUE_MAX_LEN 1024

struct CommandLineInterface {
	char* programName;
	char* workingDir;
	const char* syntaxHeader;
	Properties* options;  // option --> (type, 0)
	Properties* help; 	  // option --> (string, "option explanation")
	Properties* defaults; // option --> (type, default value)  (no default <--> mandatory option)

	// Values of command line arguments as provided by user
	Properties* argValues; // option --> (type, value)  (flags are bool options with 'false' default)

	size_t optionLenLimit;
	size_t optionMaxLen;

	char* fullCmdLine;
};

void CommandLineInterface_free(CommandLineInterface** thisP) {
	Debug_assert(DEBUG_ALL, thisP != NULL, "thisP == NULL\n");
	CommandLineInterface* this = *thisP;

	Properties_free(&this->options);
	Properties_free(&this->help);
	Properties_free(&this->defaults);
	if (this->argValues != NULL) Properties_free(&this->argValues);
  free((char *)this->workingDir);
  free((char *)this->programName);
	free(this->fullCmdLine);
	free(this);
	*thisP = NULL;
}

void CommandLineInterface_fprint_args(const CommandLineInterface* const this, FILE* f) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	Properties_fprint(this->argValues, f);
}


const Properties* CommandLineInterface_args(const CommandLineInterface* const this) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	return this->argValues;
}

CommandLineInterface* CommandLineInterface_new(const char* const args0, const char* const syntaxHeader) {
	size_t optionLenLimit = OPTION_LEN_LIMIT;
	CommandLineInterface* new = calloc(1, sizeof(CommandLineInterface));
	Debug_assert(DEBUG_ALL, new != NULL, "new == NULL");

	int lastSlashIdx = StringUtils_lastIndexOf(args0, '/');
  if (lastSlashIdx < 0) {
		new->workingDir = StringUtils_clone("./");
		new->programName = StringUtils_clone(args0);
	}
	else {
		new->workingDir = StringUtils_substring(args0, 0, lastSlashIdx);
		new->programName = StringUtils_substring(args0, lastSlashIdx+1, (int)strlen(args0)-1);
	}
	new->syntaxHeader = syntaxHeader;
	Debug_assert(DEBUG_ALL, optionLenLimit > 0, "optionLenLimit = %lu <= 0\n", optionLenLimit);
	new->optionLenLimit = optionLenLimit + 2; // to handle '--' prefix
	new->optionMaxLen = 0;
	new->options = Properties_new(optionLenLimit);
	new->help = Properties_new(optionLenLimit);
	new->defaults = Properties_new(optionLenLimit);
	CommandLineInterface_addFlag(new, "help", "Prints out this help message");
	CommandLineInterface_addStringOption_withDefault(new, "config", NULL, "Reads additional options and configurations from the given Properties file. The config file contains 'option = value' pairs (where '--option' is a command-line option), which are processed when this option is given. The config file might also contain further properties not documented here and not allowed from the command-line.");

	new->argValues = NULL;
	new->fullCmdLine = calloc(FULL_CMD_LINE_LEN_LIMIT+1, sizeof(char));

	Debug_out(DEBUG_C, "Instance created\n");
	return new;
}

void _CommandLineInterface_error(const CommandLineInterface* const this, const char* const msg, const char* const arg) {
	Debug_out(DEBUG_C, "option max length: %lu\n", this->optionMaxLen);
	fprintf(stderr, "ERROR: ");
	fprintf(stderr, msg, arg);
	fprintf(stderr, "\n\n%s\n\nSyntax: %s [options] [flags]\n", this->syntaxHeader, this->programName);

	fprintf(stderr, "\nOptions:");
	const Properties_Entry* e = NULL;
	char* padded_string = calloc(this->optionLenLimit + 2, sizeof(char));
	const char* option = NULL;
	const char* help = NULL;
	const Properties_Content* default_content = NULL;
	Properties_Content_Type type = 0;
	int err = 0;

	// PRINT OUT OPTIONS
	Properties_Iterator* it = Properties_Iterator_new(this->help);
	while ( (e = Properties_Iterator_next(it)) != NULL) {
		option = Properties_Entry_property(e);
    err = Properties_type(this->options, option, &type);
    Debug_assert(DEBUG_C, !err, "err = %d\n", err);
		if (type == PROPERTIES_BOOLEAN) {
			// It is a flag, skip it
			continue;
		}
		err = Properties_getString(this->help, option, &help);
		Debug_assert(DEBUG_C, err == 0, "err = %d\n", err);
		default_content = Properties_get(this->defaults, option);
		Debug_assert(DEBUG_C, err == 0, "err = %d\n", err);

		sprintf(padded_string, "--%s", option);
		StringUtils_leftPad(padded_string, this->optionMaxLen + 2, ' ');
		fprintf(stderr, "\n\n%s : type: %s, default: ",
			padded_string,
			Properties_Content_Type_toString(type)
		);

		if (default_content != NULL) {
			Properties_Content_Value_fprint(default_content, stderr);
		} else {
      fprintf(stderr, "none (MANDATORY OPTION)");
		}

		strcpy(padded_string, " ");
		StringUtils_leftPad(padded_string, this->optionMaxLen + 2, ' ');
		fprintf(stderr, "\n%s   %s", padded_string, help);
	}
	Properties_Iterator_free(&it);


	fprintf(stderr, "\n\nFlags:");
	// PRINT OUT FLAGS
	it = Properties_Iterator_new(this->help);
	while ( (e = Properties_Iterator_next(it)) != NULL) {
		option = Properties_Entry_property(e);
    err = Properties_type(this->options, option, &type);
    Debug_assert(DEBUG_C, !err, "err = %d\n", err);
		if (type != PROPERTIES_BOOLEAN) {
			// It is an option, skip it
			continue;
		}
		err = Properties_getString(this->help, option, &help);
		Debug_assert(DEBUG_C, err == 0, "err = %d\n", err);

		sprintf(padded_string, "--%s", option);
		StringUtils_leftPad(padded_string, this->optionMaxLen + 2, ' ');
		fprintf(stderr, "\n\n%s : %s",
			padded_string,
			help
		);
	}
	fprintf(stderr, "\n");
	Properties_Iterator_free(&it);

	free(padded_string);
	exit(-1);
}

void _CommandLineInterface_addOption(CommandLineInterface* this, const char* const option, Properties_Content_Type type, const void* zero, const char* const help) {
	if (Properties_get(this->options, option) != NULL) {
		fprintf(stderr, "Error while defining option '%s'. Option has already been defined\n", option);
		return;
	}
	unsigned int option_len = (unsigned int)strlen(option);
  if (option_len > this->optionMaxLen) this->optionMaxLen = option_len;
	Properties_set(this->options, option, type, zero);
	Properties_setString(this->help, option, help);
}

void CommandLineInterface_addDoubleOption(CommandLineInterface* this, const char* const option, const char* const help) {
	double zero = 0;
	_CommandLineInterface_addOption(this, option, PROPERTIES_DOUBLE, &zero, help);
}
void CommandLineInterface_addUIntOption(CommandLineInterface* this, const char* const option, const char* const help) {
	int zero = 0;
	_CommandLineInterface_addOption(this, option, PROPERTIES_UINT, &zero, help);
}
void CommandLineInterface_addIntOption(CommandLineInterface* this, const char* const option, const char* const help) {
	int zero = 0;
	_CommandLineInterface_addOption(this, option, PROPERTIES_INT, &zero, help);
}
void CommandLineInterface_addULongOption(CommandLineInterface* this, const char* const option, const char* const help) {
	unsigned long zero = 0;
	_CommandLineInterface_addOption(this, option, PROPERTIES_ULONG, &zero, help);
}
void CommandLineInterface_addStringOption(CommandLineInterface* this, const char* const option, const char* const help) {
	char* zero = NULL;
	_CommandLineInterface_addOption(this, option, PROPERTIES_STRING, &zero, help);
}


void CommandLineInterface_addDoubleOption_withDefault(CommandLineInterface* this, const char* const option, double default_value, const char* const help) {
	CommandLineInterface_addDoubleOption(this, option, help);
	Properties_setDouble(this->defaults, option, default_value);
}

void CommandLineInterface_addUIntOption_withDefault(CommandLineInterface* this, const char* const option, unsigned int default_value, const char* const help) {
	CommandLineInterface_addUIntOption(this, option, help);
	Properties_setUInt(this->defaults, option, default_value);
}
void CommandLineInterface_addIntOption_withDefault(CommandLineInterface* this, const char* const option, int default_value, const char* const help) {
	CommandLineInterface_addIntOption(this, option, help);
	Properties_setInt(this->defaults, option, default_value);
}
void CommandLineInterface_addULongOption_withDefault(CommandLineInterface* this, const char* const option, unsigned long default_value, const char* const help) {
	CommandLineInterface_addULongOption(this, option, help);
	Properties_setULong(this->defaults, option, default_value);
}

void CommandLineInterface_addStringOption_withDefault(CommandLineInterface* this, const char* const option, const char* const default_value, const char* const help) {
	CommandLineInterface_addStringOption(this, option, help);
	Properties_setString(this->defaults, option, default_value);
}



void CommandLineInterface_addFlag(CommandLineInterface* this, const char* const flag, const char* const help) {
	if (Properties_get(this->options, flag) != NULL) {
		fprintf(stderr, "Error while defining flag '%s'. Flag/option has already been defined\n", flag);
		return;
	}
	int zero = 0;
	_CommandLineInterface_addOption(this, flag, PROPERTIES_BOOLEAN, &zero, help);
	Properties_setBoolean(this->defaults, flag, 0);
}

int CommandLineInterface_processArgs(CommandLineInterface* this, int argc, char** args) {
	Debug_out(DEBUG_C, "Start processArgs()\n");
	int error = 0;
	int i=0;
	if (this->argValues != NULL) {
		Properties_free(&this->argValues);
	}
	//this->programName = args[0];
	strcpy(this->fullCmdLine, this->programName);




	// Fill-in this->argValues with default values
	this->argValues = Properties_clone(this->defaults);

	// Store command line arguments (overriding defaults, if given)
	for(i=1; i < argc; i++) {
		char* arg = args[i];
		strcat(this->fullCmdLine, " ");
		strcat(this->fullCmdLine, args[i]);

		if (strlen(arg) < 3 || arg[0] != '-' || arg[1] != '-') {
			_CommandLineInterface_error(this, "Option/flag has illegal format: '%s'", arg);
			error = -1;
			break;
		}
		arg = &arg[2]; // skip '--'

		if (strcmp(arg, "help") == 0) {
			_CommandLineInterface_error(this, "Help on syntax requested (--%s)", arg);
			error = -2;
			break;
		}

		char* value_string = NULL;

		int ok = 0;
		const Properties_Content* option_content = Properties_get(this->options, arg);
		if (option_content == NULL) {
			_CommandLineInterface_error(this, "Option/flag unknown: '%s'", arg);
			error = -3;
			break;
		}
		Properties_Content_Type type = Properties_Content_type(option_content);
		if (type == PROPERTIES_BOOLEAN) {
			// It is a flag: set value_string to "true"
			value_string = "true";
		} else {
			value_string = args[++i];
			strcat(this->fullCmdLine, " ");
			strcat(this->fullCmdLine, args[i]);
		}
		Debug_out(DEBUG_C, "Read value '%s' for option/flag %s\n", value_string, arg);

		if (strcmp(arg, "config") == 0 && value_string != NULL) {
			// special option "config": override values in this->argValues with values from config file
			if (FileUtils_isFile(value_string)) {
				Debug_out(DEBUG_C, "Reading additional options and configurations from file '%s'\n", value_string);
				Properties* config = Properties_newFromFile(value_string);
				Properties_copyInto(config, this->argValues);
				Properties_free(&config);
			} else {
				_CommandLineInterface_error(this, "Config file '%s' does not exist.\n", value_string);
				error = -4;
				break;
			}
		}

		// for any option (also config)
		error = Properties_setFromString(this->argValues, arg, type, value_string);
		if (error) {
			_CommandLineInterface_error(this, "Cannot parse value given to option/flag '%s'",
				arg);
			break;
		}
	}

  Properties_fprint(this->defaults, stderr);
  Properties_fprint(this->argValues, stderr);

	if (error == 0) {
		// Check whether all mandatory options are given a value
		Properties_Iterator* it = Properties_Iterator_new(this->options);
		const Properties_Entry* e = NULL;
		while ((e = Properties_Iterator_next(it)) != NULL) {
			const char* const option = Properties_Entry_property(e);
			if (Properties_get(this->argValues, option) == NULL) {
				_CommandLineInterface_error(this, "Value not given for mandatory option '%s'\n", option);
				error = -5;
				break;
			}
		}
		Properties_Iterator_free(&it);
	}
	Debug_out(DEBUG_C, "processArgs() done.\n");
	return error;
}







const char* CommandLineInterface_getStringOption(const CommandLineInterface* const this, char* property) {
	const char* result = NULL;
	Properties_getString(this->argValues, property, &result);
	return result;
}
unsigned int CommandLineInterface_getUIntOption(const CommandLineInterface* const this, char* property) {
	unsigned int result = 0;
	Properties_getUInt(this->argValues, property, &result);
	return result;
}
int CommandLineInterface_getIntOption(const CommandLineInterface* const this, char* property) {
	int result = 0;
	Properties_getInt(this->argValues, property, &result);
	return result;
}
unsigned long CommandLineInterface_getULongOption(const CommandLineInterface* const this, char* property) {
	unsigned long result = 0;
	Properties_getULong(this->argValues, property, &result);
	return result;
}
double CommandLineInterface_getDoubleOption(const CommandLineInterface* const this, char* property) {
	double result = 0;
	Properties_getDouble(this->argValues, property, &result);
	return result;
}
int CommandLineInterface_getFlag(const CommandLineInterface* const this, char* property) {
	char result = 0;
	int err = Properties_getBoolean(this->argValues, property, &result);
	if (err == -1) {
		// flag not defined
		result = 0;
	}
	return (int) result;
}

const char* CommandLineInterface_getFullCmdline(const CommandLineInterface* const this) {
	return this->fullCmdLine;
}

const char* CommandLineInterface_getWorkingDir(const CommandLineInterface* const this) {
	return this->workingDir;
}

