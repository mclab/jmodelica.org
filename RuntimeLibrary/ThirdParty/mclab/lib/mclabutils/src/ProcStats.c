/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>

#if defined(__unix__) || defined(__unix) || defined(unix)
	#include <unistd.h>
	#include <sys/resource.h>

#elif defined(__APPLE__) && defined(__MACH__)
	#include <mach/mach.h>
	#include <unistd.h>
	#include <sys/resource.h>
#else
	#error "Cannot define ProcStats for an unknown OS."
#endif

#include <ProcStats.h>

#define BUFFER_READ 10000

long ProcStats_selfMaxResidentSetSize() {
	struct rusage rusage;
	getrusage(RUSAGE_SELF, &rusage);
	return rusage.ru_maxrss;
}

int ProcStats_selfCurrentMemoryUsage(long* vmrss_kb, long* vmsize_kb) {
	*vmsize_kb = 0;
	*vmrss_kb = 0;

	#if defined(__APPLE__) && defined(__MACH__)
		/* OSX ------------------------------------------------------ */
		struct mach_task_basic_info info;
		mach_msg_type_number_t infoCount = MACH_TASK_BASIC_INFO_COUNT;

		if ( task_info(mach_task_self(), MACH_TASK_BASIC_INFO, (task_info_t)&info, &infoCount) != KERN_SUCCESS)
			return -1;

		*vmrss_kb = (long) (((mach_vm_size_t)info.resident_size) / 1000.0);
		*vmsize_kb = (long) (((mach_vm_size_t)info.virtual_size) / 1000.0);

	#elif defined(__linux__) || defined(__linux) || defined(linux) || defined(__gnu_linux__)
		long vmsize_pages = 0;
		long vmrss_pages = 0;
		FILE* fp = NULL;

		if ( (fp = fopen( "/proc/self/statm", "r" )) == NULL ) return -1;

		if ( fscanf(fp, "%ld %ld%*s", &vmsize_pages, &vmrss_pages) != 2 ) {
			fclose(fp);
			return -2;
		}
		fclose(fp);

		*vmsize_kb = (long) (((size_t)vmsize_pages * (size_t)sysconf( _SC_PAGESIZE)) / 1000.0);
		*vmrss_kb = (long) (((size_t)vmrss_pages * (size_t)sysconf( _SC_PAGESIZE)) / 1000.0);

	#endif

	return 0;
}
