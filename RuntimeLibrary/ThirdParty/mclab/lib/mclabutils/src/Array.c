/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <Debug.h>
#include <Numbers.h>

#include <Array.h>

#define DEBUG_ARRAY "Array"

struct Array {
	size_t elemSize;
	void* array;
	unsigned int startPos;
	unsigned int firstEmptyPos;

	unsigned int capacity;
	unsigned int capIncrement;

	void (*fprintElem)(const void* const elemP, FILE*);
	int (*fEqualsElem)(const void* const e1, const void* const e2);

	void* elemPlaceHolder;
};

void Array_adjustSize(Array* this);


size_t Array_elemSize(const Array* const this) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	return this->elemSize;
}

#define DEBUG_EQUALS "Array_equals"
int Array_equals(const Array* const this, const Array* const other) {
	Debug_assert(DEBUG_ALL, this->fEqualsElem != NULL, "this->fEqualsElem == NULL");
	if (this == NULL) return other == NULL;
	if (other == NULL) return 0;

	unsigned int length = Array_length(this);
	if (length != Array_length(other)) return 0;
	if (this->elemSize != other->elemSize) return 0;

	void* elem_this = calloc(1, this->elemSize);
	void* elem_other = calloc(1, this->elemSize);
	int done = 0;
	int result = 1;
	for(unsigned int i=0; !done && i < length; i++) {
		//Debug_out(DEBUG_EQUALS, "\n\n\nChecking array component nb %d:\n", i);
		Array_get(this, i, elem_this);
		Array_get(other, i, elem_other);
		//if (this->fprintElem) Debug_perform(DEBUG_EQUALS, this->fprintElem(elem_this, stderr));
		//Debug_out(DEBUG_EQUALS, "\nversus:\n");
		//if (this->fprintElem) Debug_perform(DEBUG_EQUALS, this->fprintElem(elem_other, stderr));
		//Debug_out(DEBUG_EQUALS, "\n\n\n\n");
		if (!this->fEqualsElem(elem_this, elem_other)) {
			result = 0;
			done = 1;
		}
	}
	free(elem_this);
	free(elem_other);
	return result;
}

unsigned int Array_length(const Array* const this) {
	Debug_assert(DEBUG_ALL, this != NULL, "this == NULL\n");
	Debug_assert(DEBUG_ALL, this->firstEmptyPos >= this->startPos, "this->firstEmptyPos = %u, this->startPos = %u\n",
		this->firstEmptyPos, this->startPos);
	return this->firstEmptyPos - this->startPos;
}

const void* Array_as_C_array(const Array* const this) {
	return (void*)((char*)this->array + this->startPos*this->elemSize);
}

void Array_trunc(Array* const this, unsigned int len) {
	Debug_assert(DEBUG_ALL, this != NULL, "this == NULL\n");
	if (Array_length(this) > len) {
		this->firstEmptyPos = this->startPos + len;
	}
}

/**
Replaces the content of the memory block pointed by valueP with a copy of the
i-th element of array this.
The function returns valueP
*/
void* Array_get(const Array* const this, unsigned int i, void* const valueP) {
	Debug_assert(DEBUG_ALL, this != NULL, "this == NULL\n");
	//Debug_assert(DEBUG_ALL, i >= 0, "i = %d < 0\n", i);
	Debug_assert(DEBUG_ALL, i < Array_length(this), "i =%u >= array length = %u\n", i, Array_length(this));

	i = i + this->startPos; // the real position of array[i]
	/*Debug_out(DEBUG_ARRAY, "Array_get(): Copying %ld bytes from %p to memblock pointed by out param %p\n",
	 	this->elemSize,
		(void*)((char*)this->array + i*this->elemSize), valueP);*/

	memcpy(valueP, (void*)((char*)this->array + i*this->elemSize), this->elemSize);
	return valueP;
}
void* Array_getFirst(const Array* const this, void* const value) {
	return Array_get(this, 0, value);
}
void* Array_getLast(const Array* const this, void* const value) {
	return Array_get(this, Array_length(this)-1, value);
}

const void* Array_elemPtr(const Array* const this, unsigned int i) {
	i = i + this->startPos;
	return (void*)((char*)this->array + i*this->elemSize);
}


void Array_swap(Array* this, unsigned int i, unsigned int j) {
	Debug_assert(DEBUG_ALL, this != NULL, "this == NULL\n");
	//Debug_assert(DEBUG_ALL, i >= 0, "i = %d < 0\n", i);
	Debug_assert(DEBUG_ALL, i < Array_length(this), "i = %d >= array length = %d\n", i, Array_length(this));
	//Debug_assert(DEBUG_ALL, j >= 0, "j = %d < 0\n", j);
	Debug_assert(DEBUG_ALL, j < Array_length(this), "j = %d >= array length = %d\n", j, Array_length(this));

	i = i + this->startPos;
	j = j + this->startPos;
	memcpy( this->elemPlaceHolder, (void*)((char*)this->array + i*this->elemSize), this->elemSize );
	memcpy( (void*)((char*)this->array + i*this->elemSize),
			(void*)((char*)this->array + j*this->elemSize), this->elemSize );
	memcpy( (void*)((char*)this->array + j*this->elemSize),
			this->elemPlaceHolder, this->elemSize );
}

// Methods to handle this array as a stack
void* Array_pop(Array* const this, void* const value) {
	Array_getLast(this, value);
	this->firstEmptyPos--;
	return value;
}
void Array_push(Array* const this, const void* const valueP) {
	Array_add(this, valueP);
}
void* Array_top(Array* const this, void* const value) {
	return Array_getLast(this, value);
}

// Methods to handle this array as a queue
void Array_enqueue(Array* const this, const void* const valueP) {
	Array_add(this, valueP);
}
void* Array_dequeue(Array* const this, void* const value) {
	Array_getFirst(this, value);
	this->startPos++;
	return value;
}



void Array_clear(Array* const this) {
	this->startPos = 0;
	this->firstEmptyPos = 0;
}

/** Replaces the i-th element of array 'this' with a _copy_ of the value pointed by 'valueP'
*/
void Array_set(Array* const this, unsigned int i, const void* const valueP) {
	Debug_assert(DEBUG_ALL, this != NULL, "this == NULL\n");
	//Debug_assert(DEBUG_ALL, i >= 0, "i < 0\n");
	if (i == Array_length(this)) {
		Array_add(this, valueP);
		return;
	}
	Debug_assert(DEBUG_ALL, i < Array_length(this), "i = %u > array length = %u\n", i, Array_length(this));
	i = i + this->startPos;
	void* destP = (void*)((char*)this->array + i*this->elemSize);
	Debug_out(DEBUG_ARRAY, "Array_set(): Copying value pointed by %p to array (memblock %p)\n", valueP, destP);
	memcpy(destP, valueP, this->elemSize);
}


/** Appends to array 'this' a _copy_ of the value pointed by 'valueP'
*/
void Array_add(Array* const this, const void* const valueP) {
	Debug_assert(DEBUG_ARRAY, this != NULL, "this == NULL\n");
	Debug_out(DEBUG_ARRAY, "Array_add(%p, %p)\n", (void*)this, valueP);
	Debug_out(DEBUG_ARRAY, " - firstEmptyPos = %d\n - capacity = %d\n - elemSize = %ld\n - array start = %p\n", this->firstEmptyPos, this->capacity, this->elemSize, this->array);
	if (this->firstEmptyPos >= this->capacity) {
		Debug_out(DEBUG_ARRAY, "   --> adjusting size\n");
		Array_adjustSize(this);
	}
	void* destP = (void*)((char*)this->array + this->firstEmptyPos * this->elemSize);
	Debug_out(DEBUG_ARRAY, " - destP = %p\n", destP);
	memcpy(destP, valueP, this->elemSize);
	this->firstEmptyPos++;
	Debug_out(DEBUG_ARRAY, "Array_add() done\n");
}


void Array_add_n_times(Array* this, const void* const valueP, int n) {
	for (int i=0; i<n; i++) {
		Array_add(this, valueP);
	}
}

Array* Array_new(unsigned int capacity, size_t elemSize, unsigned int capIncrement,
		void (*fprintElem)(const void* const elemP, FILE*),
		int (*fEqualsElem)(const void* const e1, const void* const e2)   ) {
	Debug_assert(DEBUG_ALL, elemSize > 0, "elemSize <= 0");
	Debug_assert(DEBUG_ALL, capacity > 0, "capacity <= 0");
	Debug_assert(DEBUG_ALL, capIncrement > 0, "capIncrement <= 0");

	Array* new = calloc(1, sizeof(Array));
	new->elemSize = elemSize;
	new->capacity = capacity;
	new->capIncrement = capIncrement;
	new->startPos = 0;
	new->firstEmptyPos = 0;
	new->array = calloc(capacity, elemSize);
	new->fprintElem = fprintElem;
	new->fEqualsElem = fEqualsElem;
	new->elemPlaceHolder = calloc(1, new->elemSize);
	Debug_out(DEBUG_ARRAY, "Array_new() = %p: elemSize = %ld\n", (void*)new, new->elemSize);
	return new;
}

Array* Array_new_wrap(unsigned int length, size_t elemSize,
		void* buffer,
		unsigned int capIncrement,
		void (*fprintElem)(const void* const elem_p, FILE*),
		int (*fEqualsElem)(const void* const e1_p, const void* const e2_p)   ) {
	Debug_assert(DEBUG_ALL, length > 0, "length < 0\n");
	Debug_assert(DEBUG_ALL, elemSize > 0, "elemSize <= 0\n");
	Debug_assert(DEBUG_ALL, capIncrement > 0, "capIncrement <= 0");
	Debug_assert(DEBUG_ALL, buffer != NULL, "buffer == NULL\n");

	Array* new = calloc(1, sizeof(Array));
	new->elemSize = elemSize;
	new->capacity = length;
	new->capIncrement = capIncrement;
	new->startPos = 0;
	new->firstEmptyPos = length;
	new->array = buffer;
	new->fprintElem = fprintElem;
	new->fEqualsElem = fEqualsElem;
	new->elemPlaceHolder = calloc(1, new->elemSize);
	Debug_out(DEBUG_ARRAY, "Array_new_wrap() = %p: elemSize = %ld\n", (void*)new, new->elemSize);
	return new;
}







#define DEBUG_ARRAY_ADJ_SIZE "Array_adjustSize"
void Array_adjustSize(Array* this) {
	Debug_assert(DEBUG_ALL, this != NULL, "this == NULL");
	Debug_assert(DEBUG_ALL, this->capIncrement > 0, "Array_adjustSize() error: "
		"this->capIncrement == 0\n");

	if (this->startPos > this->capIncrement) {
		Debug_out(DEBUG_ARRAY_ADJ_SIZE, "Reallocating array as to cut-off unused prefix\n");
		// There is too much room at the left of the array (too many dequeue() operations)
		void* newArray = calloc(this->capacity, this->elemSize);
		void* startP = (void*)((char*)this->array + this->startPos*this->elemSize);
		memcpy(newArray, startP, Array_length(this) * this->elemSize);
		free(this->array);
		this->array = newArray;
		this->firstEmptyPos -= this->startPos;
		this->startPos = 0;
	} else {
		Debug_out(DEBUG_ARRAY_ADJ_SIZE, "Increasing array capacity from %d to %d\nArray is:\n",
			this->capacity, this->capacity + this->capIncrement);
		Debug_perform(DEBUG_ARRAY_ADJ_SIZE, Array_fprint(this, stderr));
		// Increase capacity
		this->capacity += this->capIncrement;
		this->array = realloc(this->array, this->capacity*this->elemSize);
		Debug_assert(DEBUG_ALL, this->array != NULL, "Error while increasing array capacity to %d\n",
		 	this->capacity);
	}
}

void Array_free(Array** thisP) {
	Debug_assert(DEBUG_ALL, thisP != NULL, "thisP == NULL\n");
	Array* this = *thisP;
	if (this == NULL) return;
	free(this->array);
	free(this->elemPlaceHolder);
	free(this);
	*thisP = NULL;
}


void (*Array_fprintElem(const Array* const this))(const void* const, FILE*) {
	return this->fprintElem;
}
int (*Array_fEqualsElem(const Array* const this))(const void* const, const void* const) {
	return this->fEqualsElem;
}


void Array_fprint(const void* const this_v, FILE* f) {
	if (this_v == NULL) {
		fprintf(f, "[Array NULL]\n");
		return;
	}
	Debug_assert(DEBUG_ALL, this_v != NULL, "this_v == NULL\n");
	Debug_assert(DEBUG_ALL, f != NULL, "f == NULL\n");
	Array* this = (Array*)this_v;
	unsigned int len = Array_length(this);
	fprintf(f, "Array (length: %d, elemSize: %ld):", len, this->elemSize);
	void* vP = calloc(1, this->elemSize); // vP will point to a copy of an element of the array, e.g., a pointer to struct
	Debug_assert(DEBUG_ALL, vP != NULL, "vP == NULL: could not allocate %ld bytes\n", this->elemSize);
	for(unsigned int i=0; i < len; i++) {
		Array_get(this, i, vP);
		fprintf(f, "\n [%d]: ", i);
		if (this->fprintElem != NULL) {
			this->fprintElem(vP, f);
		} else {
			fprintf(f, " <no fprintElem() function given>");
		}
	}
	free(vP);
	fprintf(f, "\nend\n");
}





// Simplified methods for arrays of base types

void fprintIntElem(const void* const elemP, FILE* f) {
	fprintf(f, "%d", *((int*)elemP));
}
void fprintLongElem(const void* const elemP, FILE* f) {
	fprintf(f, "%ld", *((long*)elemP));
}
void fprintULongElem(const void* const elemP, FILE* f) {
	fprintf(f, "%lu", *((unsigned long*)elemP));
}
void fprintFloatElem(const void* const elemP, FILE* f) {
	fprintf(f, "%f", *((float*)elemP));
}
void fprintDoubleElem(const void* const elemP, FILE* f) {
	fprintf(f, "%lf", *((double*)elemP));
}
void fprintStringElem(const void* const elemP, FILE* f) {
	fprintf(f, "\"%s\"", *(char**)elemP);
}
void fprintCharElem(const void* const elemP, FILE* f) {
	fprintf(f, "\"%c\"", *(char*)elemP);
}

int fEqualsIntElem(const void* const e1, const void* const e2) {
	if (e1 == NULL) return e2 == NULL;
	if (e2 == NULL) return 0;
	return *((int*)e1) == *((int*)e2);
}
int fEqualsLongElem(const void* const e1, const void* const e2) {
	if (e1 == NULL) return e2 == NULL;
	if (e2 == NULL) return 0;
	return *((long*)e1) == *((long*)e2);
}
int fEqualsULongElem(const void* const e1, const void* const e2) {
	if (e1 == NULL) return e2 == NULL;
	if (e2 == NULL) return 0;
	return *((unsigned long*)e1) == *((unsigned long*)e2);
}
int fEqualsFloatElem(const void* const e1, const void* const e2) {
	if (e1 == NULL) return e2 == NULL;
	if (e2 == NULL) return 0;
	return Numbers_approxEQ(*((float*)e1), *((float*)e2));
}
int fEqualsDoubleElem(const void* const e1, const void* const e2) {
	if (e1 == NULL) return e2 == NULL;
	if (e2 == NULL) return 0;
	return Numbers_approxEQ(*((double*)e1), *((double*)e2));
}
int fEqualsStringElem(const void* const e1, const void* const e2) {
	if (e1 == NULL) return e2 == NULL;
	if (e2 == NULL) return 0;
	return strcmp( (char*)e1, (char*)e2 ) == 0;
}
int fEqualsCharElem(const void* const e1, const void* const e2) {
	if (e1 == NULL) return e2 == NULL;
	if (e2 == NULL) return 0;
	return *((char*)e1) == *((char*)e2);
}





Array* Array_newInt(unsigned int capacity, unsigned int capIncrement) {
	return Array_new(capacity, sizeof(int), capIncrement, fprintIntElem, fEqualsIntElem);
}
Array* Array_newFloat(unsigned int capacity, unsigned int capIncrement) {
	return Array_new(capacity, sizeof(float), capIncrement, fprintFloatElem, fEqualsFloatElem);
}
Array* Array_newDouble(unsigned int capacity, unsigned int capIncrement) {
	return Array_new(capacity, sizeof(double), capIncrement, fprintDoubleElem, fEqualsDoubleElem);
}
Array* Array_newString(unsigned int capacity, unsigned int capIncrement) {
	return Array_new(capacity, sizeof(char*), capIncrement, fprintStringElem, fEqualsStringElem);
}
Array* Array_newLong(unsigned int capacity, unsigned int capIncrement) {
	return Array_new(capacity, sizeof(long), capIncrement, fprintLongElem, fEqualsLongElem);
}
Array* Array_newULong(unsigned int capacity, unsigned int capIncrement) {
	return Array_new(capacity, sizeof(unsigned long), capIncrement, fprintULongElem, fEqualsULongElem);
}
Array* Array_newChar(unsigned int capacity, unsigned int capIncrement) {
	return Array_new(capacity, sizeof(char), capIncrement, fprintCharElem, fEqualsCharElem);
}


Array* Array_clone(const Array* const this) {
	Array* new = Array_new(this->capacity, this->elemSize,
					this->capIncrement, this->fprintElem, this->fEqualsElem);
	new->startPos = 0;
	new->firstEmptyPos = Array_length(this);

	void* startP = (void*)((char*)this->array + this->startPos*this->elemSize);
	memcpy(new->array, startP, new->firstEmptyPos * new->elemSize);

	//Debug_assert(DEBUG_ARRAY, Array_equals(this, new), "Array_clone(): this != new\n");

	return new;
}

void Array_copy(const Array* const this, Array* dest) {
	Debug_assert(DEBUG_ALL, this != NULL, "this == NULL\n");
	Debug_assert(DEBUG_ALL, dest != NULL, "dest == NULL\n");
	Debug_assert(DEBUG_ALL, this->elemSize == dest->elemSize, "this->elemSize != dest->elemSize\n");
	Debug_assert(DEBUG_ALL, Array_length(this) <= dest->capacity, "this->length = %d <= dest->capacity = %d\n", Array_length(this), dest->capacity);

	void* startP = (void*)((char*)this->array + this->startPos*this->elemSize);
	dest->startPos = 0;
	dest->firstEmptyPos = Array_length(this);
	memcpy(dest->array, startP, dest->firstEmptyPos * dest->elemSize);
	Debug_assert(DEBUG_ARRAY, Array_equals(this, dest), "Array_copy(): this != dest\n");
}

void Array_sort(Array* this, int (*comparator)(const void *, const void*)) {
	qsort(
	    // starting position
		(void*)((char*)this->array + this->startPos*this->elemSize),
		Array_length(this),
		this->elemSize,
		comparator
	);
}


int Array_double_comparator_asc(const void* a, const void* b) {
	if (Numbers_approxEQ(*(double*)a, *(double*)b)) return 0;
	if ( *(double*)a < *(double*)b) return -1;
	else return +1;
}
int Array_double_comparator_desc(const void* a, const void* b) {
	if (Numbers_approxEQ(*(double*)a, *(double*)b)) return 0;
	if ( *(double*)a > *(double*)b) return -1;
	else return +1;
}

