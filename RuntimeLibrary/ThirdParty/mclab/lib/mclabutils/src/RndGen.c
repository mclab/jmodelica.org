/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#include <RndGen.h>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include <math.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#include <Debug.h>

#define DEBUG "RndGen"

// Implemented using GMP
struct RndGen {
	gsl_rng *state;
	unsigned long seed;
};


// Create a new random generator (with seed = time(NULL))
RndGen* RndGen_new() {
	RndGen* new = (RndGen*)malloc(sizeof(RndGen));
  new->state = gsl_rng_alloc(gsl_rng_taus);
	RndGen_setSeed(new, (unsigned long) time(NULL) );
	return new;
}
void RndGen_free(RndGen** rndPtr) {
	RndGen* rnd = *rndPtr;
  gsl_rng_free(rnd->state);
	free(rnd);
	*rndPtr = NULL;
}

// (Re-)set seed
void RndGen_setSeed(RndGen* rnd, unsigned long int seed) {
	rnd->seed = seed;
  Debug_out(DEBUG, "Set seed %lu\n", seed);
  gsl_rng_set(rnd->state, seed);
}
unsigned long RndGen_getSeed(RndGen* rnd) {
	return rnd->seed;
}

unsigned long RndGen_getSeedUpcomingSequence(RndGen* rnd) {
	unsigned long new_seed = RndGen_nextUL(rnd, 1, UINT_MAX - 1);
	RndGen_setSeed(rnd, new_seed);
	return new_seed;
}

// Get next number as unsigned long between from and to (inclusive)
unsigned long RndGen_nextUL(RndGen* rnd, unsigned long from, unsigned long to) {
  unsigned long ret = gsl_rng_uniform_int(rnd->state, to - from +1 ) + from;
  Debug_out(DEBUG, "Generated nextUL: %lu\n", ret);
  return ret;
}

// Get next number as double between from and to (inclusive)
double RndGen_nextD(RndGen* rnd, double from, double to) {
  double ret = gsl_ran_flat(rnd->state, from, to);
  Debug_out(DEBUG, "Generated nextD: %g\n", ret);
  return ret;
}

double RndGen_nextDQuantized_with_size(RndGen *rnd, double from, double to, double q_size) {
  unsigned long x = RndGen_nextUL(rnd, 0, (unsigned long) ((from < to ? to - from : from - to) / q_size));
  double ret = from < to ? from + x * q_size : from - x * q_size;
  Debug_out(DEBUG, "Generated nextDQuantized_with_size: %g\n", ret);
  return ret;
}

double RndGen_nextDQuantized(RndGen *rnd, double from, double to, int parts) {
	if (parts <= 1) {
		fprintf(stderr, "RndGen_nextDQuantized(): parts must be > 1. Now = %d\n", parts);
		exit(-1);
	}
	int max_p = parts - 1;
	unsigned long  x = RndGen_nextUL(rnd, 0, (unsigned long) max_p);
  double ret = from + x * (to - from)/max_p;
  Debug_out(DEBUG, "Generated nextDQuantized_with_size: %g\n", ret);
	return ret;
}

void RndGen_shuffle(RndGen *rnd, Array *array) {
  void *a = (void *) Array_as_C_array(array);
  size_t len = Array_length(array);
  size_t elem_size = Array_elemSize(array);
  gsl_ran_shuffle(rnd->state, a, len, elem_size);
}

