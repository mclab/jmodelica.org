/*
#    This file is part of MCLabUtils
#    Copyright (C) 2020  MCLab, http://mclab.di.uniroma1.it
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#include "HashTable.h"
#include <stdio.h>
#include <stdlib.h>
#include "uthash/src/uthash.h"

#include <Debug.h>
#include <BasicTypeValue.h>

#define HASHTABLE_DEBUG "HashTable"

/**
	A generic hash-table: both keys and values are generic pointers (void*), thus
	they can point to instances of arbitrary structures.
**/

struct HashTable_Entry {
	void* key;
  size_t keySize;
	void* value;

	UT_hash_handle hh; /* makes this structure hashable */
};

const void* HashTable_Entry_key(const HashTable_Entry* this) {
	assert(this != NULL);
	return this->key;
}
size_t HashTable_Entry_keySize(HashTable_Entry const *_this) {
  return _this->keySize;
}
void* HashTable_Entry_value(const HashTable_Entry* this) {
	assert(this != NULL);
	return this->value;
}



void HashTable_Entry_free(HashTable_Entry** thisP) {
	HashTable_Entry* this = *thisP;
	if (this == NULL) return;
	free(this->key);
	free(this);
	*thisP = NULL;
}

struct HashTable {
	HashTable_Entry* entries;
	void (*fprint_key)(const void* const key, FILE* f);
	void (*fprint_value)(const void* const valueP, FILE* f);
};

size_t HashTable_Entry_sizeof(const HashTable* this) {
  this;
	return sizeof(HashTable_Entry);
}
size_t HashTable_sizeof() {
	return sizeof(HashTable);
}

void fprint_key_default(const void* key, FILE* f) {
	key; f;
	fprintf(f, "[Error: before using HashTable_fprint(), you must use HashTable_set_fprint_key() "
		"to define a function to fprint keys]\n");
}
void fprint_value_default(const void* valueP, FILE* f) {
	valueP; f;
	fprintf(f, "[Error: before using HashTable_fprint(), you must use HashTable_set_fprint_value() "
		"to define a function to fprint values]\n");
}


void HashTable_set_fprint_key(HashTable* this, void (*fprint_key)(const void* const key, FILE* f) ) {
	this->fprint_key = fprint_key;
}
void HashTable_set_fprint_value(HashTable* this, void (*fprint_value)(const void* const key, FILE* f) ) {
	this->fprint_value = fprint_value;
}


/**
	Create a new empty hashtable, whose key elements are keySize bytes long.
 */
HashTable* HashTable_new() {
	HashTable* new = (HashTable*)calloc(1, sizeof(HashTable));
	new->entries = NULL;
	HashTable_set_fprint_key(new, fprint_key_default);
	HashTable_set_fprint_value(new, fprint_value_default);
	return new;
}

size_t HashTable_keySize(const HashTable* this) {
  this;
	return 0;
}


HashTable_Entry* HashTable_entry(const HashTable* this, const void* keyP, size_t keySize) {
	assert(this != NULL);
	assert(keyP != NULL);

	HashTable_Entry* result = NULL;

  HASH_FIND(hh, this->entries, keyP, keySize, result);

	return result;
}

/**
	Returns the value associated to the key value pointed by key in HashTable this, or NULL if
	no such value exists.

	Arguments:
	 - keyP: a pointer to the key. The function will search for
		an entry in HashTable this whose key is an identical copy of value pointed by keyP as key
		(i.e., not the value of the pointer itself).

	 - valueP: the address of a pointer to the value. The function will store
		the returned value into *valueP (NULL if no pair exists).

	The function returns *valueP.
 */
void* HashTable_get(const HashTable* this, const void* keyP, size_t keySize, void** valueP) {
	assert(valueP != NULL);
	HashTable_Entry* entry = HashTable_entry(this, keyP, keySize);
	if (entry == NULL) {
		*valueP = NULL;
	} else {
		*valueP = entry->value;
	}
	return *valueP;
}


/**
	Add pair (*key, value) to HashTable this, removing any other
	(*key, value') pair.

	Arguments:
	 - keyP: a pointer to the key. The HastTable will store
		a copy of the value pointed by keyP as key (i.e., not the pointer).
		This means that a call to free(key) will have no destroying effects
		on the HashTable content.

	 - value: a pointer to the value. The HashTable will store
		the pointer itself as value (i.e., it will not duplicate the value
		in another memory location).

	Returns a pointer to the old value if exists, otherwise NULL
 */
void* HashTable_put(HashTable* this, const void* keyP, size_t keySize, void* value) {
	assert(this != NULL);
	assert(keyP != NULL);
	HashTable_Entry* entry = HashTable_entry(this, keyP, keySize);

	if (entry == NULL) {
		entry = (HashTable_Entry*)calloc(1, sizeof(HashTable_Entry));
		entry->value = value;
    entry->key = calloc(1, keySize);
    entry->keySize = keySize;
    memcpy(entry->key, keyP, keySize);
    HASH_ADD_KEYPTR(hh, this->entries, entry->key, keySize, entry);
		return NULL;
	}
	else {
		void* oldValue = entry->value;
		entry->value = value;
		//fprintf(stderr, "Changing %p --> %p\n", entry->key, entry->value);
		return oldValue;
	}
}

/**
	Delete the pair having key *key from HashTable this, if one exists.

	The function looks for a key value which is an identical copy
	of the value pointed by key (i.e., *key, not the pointer itself).

	Returns a pointer to the old value of 'key' if exists (otherwise NULL)
 */
void* HashTable_remove(HashTable* this, const void* key, size_t keySize) {
	assert(this != NULL);
	assert(key != NULL);
	HashTable_Entry* result = NULL;
	void* oldValue = NULL;
  HASH_FIND(hh, this->entries, key, keySize, result);
	if (result != NULL) {
		oldValue = result->value;
		HASH_DEL(this->entries, result);
		HashTable_Entry_free(&result);
	}
	return oldValue;
}


/** 	Return the number of entries in this HashTable */
unsigned long HashTable_size(const HashTable* this) {
	return HASH_COUNT(this->entries);
}

/**
	Free the memory allocates for the HashTable this and sets *this to NULL.
	In particular:
	 - the memory storing the key values is freed (as key values are copied)
	 - the memory storing pointers to values is freed
	 - the memory storing value is **not** freed (as the HashTable stores pointers
	   to values, not the values themselves).
 */
void HashTable_free(HashTable** thisP) {
	Debug_assert(HASHTABLE_DEBUG, thisP != NULL, "thisP == NULL");
	HashTable* this = *thisP;
	if (this == NULL) return;

	//if (HashTable_size(*this) > 0) HASH_CLEAR(hh, (*this)->entries);
	HashTableIterator* it = HashTableIterator_new(this);
	HashTable_Entry* entry = NULL;
	while ((entry = HashTableIterator_next(it)) != NULL) {
		HASH_DEL(this->entries, entry);
		HashTable_Entry_free(&entry);
	}
	HashTableIterator_free(&it);
	Debug_assert(HASHTABLE_DEBUG, HashTable_size(this) == 0,
		"hTable still has %lu entries\n", HashTable_size(this));

	free(this);
	*thisP = NULL;
}


/*
void HashTable_print(FILE* stream, const HashTable* const this, char* (*keyToString)(const void* keyP, char* str), char* (*valueToString)(const void* valueP, char* str), unsigned int keyMaxStrLen, unsigned int valueMaxStrLen) {
	assert(stream != NULL);
	assert(this != NULL);
	assert(keyToString != NULL);
	assert(valueToString != NULL);

	fprintf(stream, "BEGIN (%lu elements)\n", HashTable_size(this));
	HashTableIterator* it = HashTableIterator_new(this);
	HashTable_Entry* e = NULL;
	char* strK = calloc(keyMaxStrLen+1, sizeof(char));
	char* strV = calloc(valueMaxStrLen+1, sizeof(char));
	while ( (e = HashTableIterator_next(it)) != NULL) {
		fprintf(stream, "  '%s' (%p) --> '%s' (%p)\n", (*keyToString)(e->key, strK), e->key, (*valueToString)(e->value, strV), e->value);
	}
	fprintf(stream, "END\n");
	free(strK);
	free(strV);
	HashTableIterator_free(&it);
}

char* HashTable_dumpToString(char* string, const HashTable* const this, char* (*keyToString)(const void* keyP, char* str), char* (*valueToString)(const void* valueP, char* str)) {
	assert(string != NULL);
	assert(this != NULL);
	assert(keyToString != NULL);
	assert(valueToString != NULL);

	sprintf(&string[0], "BEGIN (%lu elements)\n", HashTable_size(this));
	HashTableIterator* it = HashTableIterator_new(this);
	HashTable_Entry* e = NULL;
	char* strK = calloc(10000, sizeof(char));
	char* strV = calloc(10000, sizeof(char));
	while ( (e = HashTableIterator_next(it)) != NULL) {
		sprintf(&string[strlen(string)], "  '%s' (%p) --> '%s' (%p)\n", (*keyToString)(e->key, strK), e->key, (*valueToString)(e->value, strV), e->value);
	}
	sprintf(&string[strlen(string)], "END\n");
	free(strK);
	free(strV);
	HashTableIterator_free(&it);
	return string;
}
*/

void HashTable_fprint(const HashTable* this, FILE* f) {
	assert(f != NULL);
	assert(this != NULL);

	fprintf(f, "HashTable (%lu elements)\n", HashTable_size(this));
	HashTableIterator* it = HashTableIterator_new(this);
	HashTable_Entry* e = NULL;
	while ( (e = HashTableIterator_next(it)) != NULL) {
		fprintf(f, "  ");
		this->fprint_key(HashTable_Entry_key(e), f);
		fprintf(f, " --> ");
		void* value = HashTable_Entry_value(e);
		if (value == NULL) {
			fprintf(f, "NULL");
		} else {
			this->fprint_value(value, f);
		}
		fprintf(f, "\n");
	}
	fprintf(f, "end.\n");
	HashTableIterator_free(&it);
}


HashTable* HashTable_clone(const HashTable* this) {
	assert(this != NULL);
	HashTable* new = HashTable_new();
	new->fprint_key = this->fprint_key;
	new->fprint_value = this->fprint_value;

	HashTableIterator* it = HashTableIterator_new(this);
	HashTable_Entry* e = NULL;
	while ( (e = HashTableIterator_next(it)) != NULL) {
		HashTable_put(new, e->key, e->keySize, e->value);
	}
	HashTableIterator_free(&it);
	return new;
}


// HashTable Iterator
struct HashTableIterator {
	const HashTable* hTable;
	HashTable_Entry* nextEntry;
};
size_t HashTableIterator_sizeof() {
	return sizeof(HashTableIterator);
}


/** Create an Iterator for this HashTable */
HashTableIterator* HashTableIterator_new(const HashTable* this) {
	assert(this != NULL);
	HashTableIterator* it = (HashTableIterator*)calloc(1, sizeof(HashTableIterator));
	it->hTable = this;
	it->nextEntry = this->entries;
	return it;
}

void HashTableIterator_rewind(HashTableIterator* this) {
	this->nextEntry = this->hTable->entries;
}

/** Return 1 if a call to HashTableIterator_next(this) will return an entry, 0 otherwise */
int HashTableIterator_hasNext(const HashTableIterator* this) {
	assert(this != NULL);
	return this->nextEntry != NULL;
}

/** Return the next entry of the HashTable which 'this' is iterating on,
	NULL if no more entries exist */
HashTable_Entry* HashTableIterator_next(HashTableIterator* this) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	HashTable_Entry* result = this->nextEntry;
	if (this->nextEntry != NULL) {
		this->nextEntry = (this->nextEntry->hh).next;
	}
	return result;
}

/** Free memory used by this iterator. Also sets *this to NULL. */
void HashTableIterator_free(HashTableIterator** thisP) {
	Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
	HashTableIterator* this = *thisP;
	free(this);
	*thisP = NULL;
}

