FROM debian:stretch as intermediate

COPY . /jmodelica

RUN apt-get update && apt-get install -y gcc make g++ gfortran python ipython \
                                         swig ant default-jre-headless  \
                                         python-dev python-numpy python-scipy \
                                         python-lxml python-nose python-jpype \
                                         zlib1g-dev libboost-dev cython jcc   \
                                         git patch pkg-config                 \
                                         liblapack-dev libblas-dev libgsl-dev

RUN git clone -b v3.13.0 --single-branch --depth 1 https://github.com/Kitware/CMake /cmake
WORKDIR /cmake
RUN ./bootstrap && make install

RUN mkdir /opt/jmodelica && mkdir /jmodelica/build
WORKDIR /jmodelica/build
RUN ../configure --prefix=/opt/jmodelica --without-ipopt && make install

FROM debian:stretch
MAINTAINER alimguzhin@di.uniroma1.it

COPY --from=intermediate /opt/jmodelica /opt/jmodelica

RUN apt-get update && apt-get install -y gcc make gfortran python ipython     \
                                         default-jre-headless python-numpy    \
                                         python-scipy python-lxml python-nose \
                                         python-jpype zlib1g cython           \
                                         python-matplotlib liblapack3         \
                                         libblas3 libgsl2

RUN rm -rf /var/lib/apt/lists/*

ENV USER root

RUN mkdir /root/work
WORKDIR /root/work

CMD ["/opt/jmodelica/bin/jm_ipython.sh"]
