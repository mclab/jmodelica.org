

=============
Contact
=============

If you have any questions or comments, contact us preferably through the `forum <http://www.jmodelica.org/forum>`_ or by sending an email to Christian Andersson: chria.at.maths.lth.se
